# Cisco Meraki

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Meraki
Product Page: https://meraki.cisco.com/

## Introduction
We classify Cisco Meraki into the SD-WAN/SASE domain as Cisco Meraki devices provide a Software Defined Wide Area Network (SD-WAN) solution. We also classify it into the Inventory domain because it contains an inventory of Meraki devices.

"Cisco Meraki improves connectivity for remote and hybrid workers." 
"Cisco Meraki makes the workspace safer both digitally and physically." 

The Cisco Meraki adapter can be integrated to the Itential Device Broker which will allow your Cisco Meraki devices to be managed within the Itential Configuration Manager Application.

## Why Integrate
The Cisco Meraki adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Meraki. With this adapter you have the ability to perform operations on items such as:

- Organizations
- Networks
- Devices
- Claims

## Additional Product Documentation
The [API documents for Cisco Meraki](https://developer.cisco.com/meraki/api-v1/)
[Cisco Meraki Products](https://www.cisco.com/c/m/en_hk/products/meraki.html)
