## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Meraki. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Meraki.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Meraki. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationActionBatch(organizationId, createOrganizationActionBatch, callback)</td>
    <td style="padding:15px">Create an action batch</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/actionBatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationActionBatches(organizationId, callback)</td>
    <td style="padding:15px">Return the list of action batches in the organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/actionBatches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationActionBatch(organizationId, id, callback)</td>
    <td style="padding:15px">Return an action batch</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/actionBatches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationActionBatch(organizationId, id, callback)</td>
    <td style="padding:15px">Delete an action batch</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/actionBatches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationActionBatch(organizationId, id, updateOrganizationActionBatch, callback)</td>
    <td style="padding:15px">Update an action batch</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/actionBatches/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationAdmins(organizationId, callback)</td>
    <td style="padding:15px">List the dashboard administrators in this organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationAdmin(organizationId, createOrganizationAdmin, callback)</td>
    <td style="padding:15px">Create a new dashboard administrator</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/admins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationAdmin(organizationId, id, updateOrganizationAdmin, callback)</td>
    <td style="padding:15px">Update an administrator</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationAdmin(organizationId, id, callback)</td>
    <td style="padding:15px">Revoke all access for a dashboard administrator within this organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/admins/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAlertSettings(networkId, callback)</td>
    <td style="padding:15px">Return the alert configuration for this network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/alertSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/alerts/Settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkAlertSettings(networkId, updateNetworkAlertSettings, callback)</td>
    <td style="padding:15px">Update the alert configuration for this network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/alertSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/alerts/Settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsOverview(serial, t0, t1, timespan, callback)</td>
    <td style="padding:15px">Returns an overview of aggregate analytics data for a timespan</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsOverviewV2(serial, queryVars, callback)</td>
    <td style="padding:15px">Returns an overview of aggregate analytics data for a timespan</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsZones(serial, callback)</td>
    <td style="padding:15px">Returns all configured analytic zones for this camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsZoneHistory(serial, zoneId, t0, t1, timespan, resolution, callback)</td>
    <td style="padding:15px">Return historical records for analytic zones</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/zones/{pathv2}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsZoneHistoryV2(serial, zoneId, queryVars, callback)</td>
    <td style="padding:15px">Return historical records for analytic zones</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/zones/{pathv2}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsRecent(serial, callback)</td>
    <td style="padding:15px">Returns most recent record for analytics zones</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/recent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsRecentV2(serial, queryVars, callback)</td>
    <td style="padding:15px">Returns most recent record for analytics zones</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/recent?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCameraAnalyticsLive(serial, callback)</td>
    <td style="padding:15px">Returns live state from camera of analytics zones</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/analytics/live?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationApiRequests(organizationId, t0, t1, timespan, perPage, startingAfter, endingBefore, adminId, pathParam, method, callback)</td>
    <td style="padding:15px">List the API requests made by an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/apiRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationApiRequestsV2(organizationId, queryVars, callback)</td>
    <td style="padding:15px">List the API requests made by an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/apiRequests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkBluetoothClient(networkId, bluetoothClientId, includeConnectivityHistory, connectivityHistoryTimespan, callback)</td>
    <td style="padding:15px">Return a Bluetooth client. Bluetooth clients can be identified by their ID or their MAC.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bluetoothClients/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkBluetoothClients(networkId, perPage, startingAfter, endingBefore, timespan, includeConnectivityHistory, callback)</td>
    <td style="padding:15px">List the Bluetooth clients seen by APs in this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bluetoothClients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkBluetoothClientsV2(networkId, queryVars, callback)</td>
    <td style="padding:15px">List the Bluetooth clients seen by APs in this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bluetoothClients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkBluetoothSettings(networkId, callback)</td>
    <td style="padding:15px">Return the Bluetooth settings for a network.  Bluetooth settings  must be enabled on the network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bluetoothSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkBluetoothSettings(networkId, updateNetworkBluetoothSettings, callback)</td>
    <td style="padding:15px">Update the Bluetooth settings for a network. See the docs page for  Bluetooth settings .</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bluetoothSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationNetworks(organizationId, configTemplateId, callback)</td>
    <td style="padding:15px">List the networks in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationNetwork(organizationId, createOrganizationNetwork, callback)</td>
    <td style="padding:15px">Create a network</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetwork(networkId, callback)</td>
    <td style="padding:15px">Return a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetwork(networkId, updateNetwork, callback)</td>
    <td style="padding:15px">Update a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetwork(networkId, callback)</td>
    <td style="padding:15px">Delete a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bindNetwork(networkId, bindNetwork, callback)</td>
    <td style="padding:15px">Bind a network to a template.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/bind?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unbindNetwork(networkId, callback)</td>
    <td style="padding:15px">Unbind a network from a template.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/unbind?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTraffic(networkId, timespan, deviceType, callback)</td>
    <td style="padding:15px">The traffic analysis data for this network.
 Traffic Analysis with Hostname Visibility  must be enabled on the network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTrafficV2(networkId, queryVars, callback)</td>
    <td style="padding:15px">The traffic analysis data for this network.
 Traffic Analysis with Hostname Visibility  must be enabled on the network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAccessPolicies(networkId, callback)</td>
    <td style="padding:15px">List the access policies for this network. Only valid for MS networks.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/accessPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSSIDStatusesOfAnAccessPoint(networkId, serial, callback)</td>
    <td style="padding:15px">Return the SSID statuses of an access point</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/wireless/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSSIDStatusesOfAnAccessPointV1(serial, callback)</td>
    <td style="padding:15px">Return the SSID statuses of an access point</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAirMarshal(networkId, t0, timespan, callback)</td>
    <td style="padding:15px">List Air Marshal scan results from a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/airMarshal?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/airMarshal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">combineOrganizationNetworks(organizationId, combineOrganizationNetworks, callback)</td>
    <td style="padding:15px">Combine multiple networks into a single network</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/networks/combine?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitNetwork(networkId, callback)</td>
    <td style="padding:15px">Split a combined network into individual networks for each type of device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSiteToSiteVpn(networkId, callback)</td>
    <td style="padding:15px">Return the site-to-site VPN settings of a network. Only valid for MX networks.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/siteToSiteVpn?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/aplpiance/vpn/siteToSiteVpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSiteToSiteVpn(networkId, updateNetworkSiteToSiteVpn, callback)</td>
    <td style="padding:15px">Update the site-to-site VPN settings of a network. Only valid for MX networks in NAT mode.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/siteToSiteVpn?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/aplpiance/vpn/siteToSiteVpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCameraVideoLink(networkId, serial, timestamp, callback)</td>
    <td style="padding:15px">Returns video link for the specified camera. If a timestamp supplied, it links to that time.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/cameras/{pathv2}/videoLink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateNetworkCameraSnapshot(networkId, serial, generateNetworkCameraSnapshot, callback)</td>
    <td style="padding:15px">Generate a snapshot of what the camera sees at the specified time and return a link to that image.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/cameras/{pathv2}/snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClient(networkId, clientId, callback)</td>
    <td style="padding:15px">Return the client associated with the given identifier. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionNetworkClients(networkId, provisionNetworkClients, callback)</td>
    <td style="padding:15px">Provisions a client with a name and policy. Clients can be provisioned before they associate to the network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/provision?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientUsageHistory(networkId, clientId, callback)</td>
    <td style="padding:15px">Return the client's daily usage history. Usage data is in kilobytes. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/usageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientPolicy(networkId, clientId, callback)</td>
    <td style="padding:15px">Return the policy assigned to a client on the network. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkClientPolicy(networkId, clientId, updateNetworkClientPolicy, callback)</td>
    <td style="padding:15px">Update the policy assigned to a client on the network. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientSplashAuthorizationStatus(networkId, clientId, callback)</td>
    <td style="padding:15px">Return the splash authorization for a client, for each SSID they've associated with through splash. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/splashAuthorizationStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkClientSplashAuthorizationStatus(networkId, clientId, updateNetworkClientSplashAuthorizationStatus, callback)</td>
    <td style="padding:15px">Update a client's splash authorization. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/splashAuthorizationStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClients(networkId, t0, timespan, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">List the clients that have used this network in the timespan</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClients(serial, t0, timespan, callback)</td>
    <td style="padding:15px">List the clients of a device, up to a maximum of a month ago. The usage of each client is returned in kilobytes. If the device is a switch, the switchport is returned; otherwise the switchport field is null.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientTrafficHistory(networkId, clientId, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Return the client's network traffic data over time. Usage data is in kilobytes. This endpoint requires detailed traffic analysis to be enabled on the Network-wide > General page. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/trafficHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientEvents(networkId, clientId, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Return the events associated with this client. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientLatencyHistory(networkId, clientId, t0, t1, timespan, resolution, callback)</td>
    <td style="padding:15px">Return the latency history for a client. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP. The latency data is from a sample of 2% of packets and is grouped into 4 traffic categories: background, best effort, video, voice. Within these categories the sampled packet counters are bucketed by latency in milliseconds.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/latencyHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationConfigTemplates(organizationId, callback)</td>
    <td style="padding:15px">List the configuration templates for this organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationConfigTemplate(organizationId, id, callback)</td>
    <td style="padding:15px">Remove a configuration template</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLldpCdp(networkId, serial, timespan, callback)</td>
    <td style="padding:15px">List LLDP and CDP information for a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/lldp_cdp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLldpCdpV1(serial, timespan, callback)</td>
    <td style="padding:15px">List LLDP and CDP information for a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/lldp_cdp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationDevices(organizationId, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">List the devices in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevices(networkId, callback)</td>
    <td style="padding:15px">List the devices in a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevice(networkId, serial, callback)</td>
    <td style="padding:15px">Return a single device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceV1(serial, callback)</td>
    <td style="padding:15px">Return a single device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDevice(networkId, serial, updateNetworkDevice, callback)</td>
    <td style="padding:15px">Update the attributes of a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevicePerformance(networkId, serial, callback)</td>
    <td style="padding:15px">Return the performance score for a single device. Only primary MX devices supported. If no data is available, a 204 error code is returned.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevicePerformanceV1(serial, callback)</td>
    <td style="padding:15px">Return the performance score for a single device. Only primary MX devices supported. If no data is available, a 204 error code is returned.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/appliance/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceUplink(networkId, serial, callback)</td>
    <td style="padding:15px">Return the uplink information for a device.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/uplink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimNetworkDevices(networkId, claimNetworkDevices, callback)</td>
    <td style="padding:15px">Claim a device into a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeNetworkDevice(networkId, serial, callback)</td>
    <td style="padding:15px">Remove a single device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/remove?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/devices/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLossAndLatencyHistory(networkId, serial, t0, t1, timespan, resolution, uplink, ip, callback)</td>
    <td style="padding:15px">Get the uplink loss percentage and latency in milliseconds for a wired network device.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/lossAndLatencyHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLossAndLatencyHistoryV1(serial, t0, t1, timespan, resolution, uplink, ip, callback)</td>
    <td style="padding:15px">Get the uplink loss percentage and latency in milliseconds for a wired network device.</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/lossAndLatencyHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootNetworkDevice(networkId, serial, callback)</td>
    <td style="padding:15px">Reboot a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootNetworkDeviceV1(serial, callback)</td>
    <td style="padding:15px">Reboot a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">blinkNetworkDeviceLeds(networkId, serial, blinkNetworkDeviceLeds, callback)</td>
    <td style="padding:15px">Blink the LEDs on a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/blinkLeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">blinkNetworkDeviceLedsV1(serial, blinkNetworkDeviceLeds, callback)</td>
    <td style="padding:15px">Blink the LEDs on a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/blinkLeds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCellularFirewallRules(networkId, callback)</td>
    <td style="padding:15px">Return the cellular firewall rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularFirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/cellularFirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCellularFirewallRules(networkId, updateNetworkCellularFirewallRules, callback)</td>
    <td style="padding:15px">Update the cellular firewall rules of an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularFirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/cellularFirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL3FirewallRules(networkId, callback)</td>
    <td style="padding:15px">Return the L3 firewall rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/l3FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/l3FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL3FirewallRules(networkId, updateNetworkL3FirewallRules, callback)</td>
    <td style="padding:15px">Update the L3 firewall rules of an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/l3FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/l3FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL7FirewallRulesApplicationCategories(networkId, callback)</td>
    <td style="padding:15px">Return the L7 firewall application categories and their associated applications for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/l7FirewallRules/applicationCategories?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/l7FirewallRules/applicationCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkL7FirewallRules(networkId, callback)</td>
    <td style="padding:15px">List the MX L7 firewall rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/l7FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/l7FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkL7FirewallRules(networkId, updateNetworkL7FirewallRules, callback)</td>
    <td style="padding:15px">Update the MX L7 firewall rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/l7FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/l7FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationVpnFirewallRules(organizationId, callback)</td>
    <td style="padding:15px">Return the firewall rules for an organization's site-to-site VPN</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/vpnFirewallRules?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/vpn/vpnFirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationVpnFirewallRules(organizationId, updateOrganizationVpnFirewallRules, callback)</td>
    <td style="padding:15px">Update the firewall rules of an organization's site-to-site VPN</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/vpnFirewallRules?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/vpn/vpnFirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsidL3FirewallRules(networkId, number, callback)</td>
    <td style="padding:15px">Return the L3 firewall rules for an SSID on an MR network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/l3FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/firewall/l3FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSsidL3FirewallRules(networkId, number, updateNetworkSsidL3FirewallRules, callback)</td>
    <td style="padding:15px">Update the L3 firewall rules of an SSID on an MR network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/l3FirewallRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/firewall/l3FirewallRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroupPolicies(networkId, callback)</td>
    <td style="padding:15px">List the group policies in a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/groupPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkGroupPolicy(networkId, createNetworkGroupPolicy, callback)</td>
    <td style="padding:15px">Create a group policy</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/groupPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkGroupPolicy(networkId, groupPolicyId, callback)</td>
    <td style="padding:15px">Display a group policy</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/groupPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkGroupPolicy(networkId, groupPolicyId, updateNetworkGroupPolicy, callback)</td>
    <td style="padding:15px">Update a group policy</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/groupPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkGroupPolicy(networkId, groupPolicyId, callback)</td>
    <td style="padding:15px">Delete a group policy</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/groupPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsidHotspot20(networkId, number, callback)</td>
    <td style="padding:15px">Return the Hotspot 2.0 settings for an SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/hotspot20?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/hotspot20?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSsidHotspot20(networkId, number, updateNetworkSsidHotspot20, callback)</td>
    <td style="padding:15px">Update the Hotspot 2.0 settings of an SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/hotspot20?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/hotspot20?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkHttpServers(networkId, callback)</td>
    <td style="padding:15px">List the HTTP servers for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkHttpServer(networkId, createNetworkHttpServer, callback)</td>
    <td style="padding:15px">Add an HTTP server</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkHttpServer(networkId, id, callback)</td>
    <td style="padding:15px">Return an HTTP server</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkHttpServer(networkId, id, updateNetworkHttpServer, callback)</td>
    <td style="padding:15px">Update an HTTP server</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkHttpServer(networkId, id, callback)</td>
    <td style="padding:15px">Delete an HTTP server</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkHttpServersWebhookTest(networkId, createNetworkHttpServersWebhookTest, callback)</td>
    <td style="padding:15px">Send a test webhook</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers/webhookTests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkHttpServersWebhookTest(networkId, id, callback)</td>
    <td style="padding:15px">Return the status of a webhook test</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/httpServers/webhookTests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceManagementInterfaceSettings(networkId, serial, callback)</td>
    <td style="padding:15px">Return the management interface settings for a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/managementInterfaceSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceManagementInterfaceSettingsV1(serial, callback)</td>
    <td style="padding:15px">Return the management interface settings for a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/managementInterfaceSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDeviceManagementInterfaceSettings(networkId, serial, updateNetworkDeviceManagementInterfaceSettings, callback)</td>
    <td style="padding:15px">Update the management interface settings for a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/managementInterfaceSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDeviceManagementInterfaceSettingsV1(serial, updateNetworkDeviceManagementInterfaceSettings, callback)</td>
    <td style="padding:15px">Update the management interface settings for a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/managementInterfaceSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkMerakiAuthUsers(networkId, callback)</td>
    <td style="padding:15px">List the splash or RADIUS users configured under Meraki Authentication for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/merakiAuthUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkMerakiAuthUser(networkId, id, callback)</td>
    <td style="padding:15px">Return the Meraki Auth splash or RADIUS user</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/merakiAuthUsers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationOpenapiSpec(organizationId, callback)</td>
    <td style="padding:15px">Return the OpenAPI 2.0 Specification of the organization's API documentation in JSON</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/openapiSpec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizations(callback)</td>
    <td style="padding:15px">List the organizations that the user has privileges on</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganization(createOrganization, callback)</td>
    <td style="padding:15px">Create a new organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganization(id, callback)</td>
    <td style="padding:15px">Return an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganization(id, updateOrganization, callback)</td>
    <td style="padding:15px">Update an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneOrganization(id, cloneOrganization, callback)</td>
    <td style="padding:15px">Create a new organization by cloning the addressed organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationLicenseState(id, callback)</td>
    <td style="padding:15px">Return the license state for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenseState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationInventory(id, callback)</td>
    <td style="padding:15px">Return the inventory for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/inventory?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationInventoryV2(id, queryVars, callback)</td>
    <td style="padding:15px">Return the inventory for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/inventory?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationDeviceStatuses(id, callback)</td>
    <td style="padding:15px">List the status of every Meraki device in the organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/deviceStatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationSnmp(id, callback)</td>
    <td style="padding:15px">Return the SNMP settings for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationSnmp(id, updateOrganizationSnmp, callback)</td>
    <td style="padding:15px">Update the SNMP settings for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimOrganization(organizationId, claimOrganization, callback)</td>
    <td style="padding:15px">Claim a device, license key, or order into an organization. When claiming by order, all devices and licenses in the order will be claimed; licenses will be added to the organization and devices will be placed in the organization's inventory. These three types of claims are mutually exclusive and cannot be performed in one request.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationUplinksLossAndLatency(organizationId, uplink, ip, callback)</td>
    <td style="padding:15px">Return the uplink loss and latency for every MX in the organization from 2 - 7 minutes ago</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/uplinksLossAndLatency?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/devices/uplinksLossAndLatency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationUplinksLossAndLatencyV2(organizationId, queryVars, callback)</td>
    <td style="padding:15px">Return the uplink loss and latency for every MX in the organization from 2 - 7 minutes ago</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/uplinksLossAndLatency?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/devices/uplinksLossAndLatency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationThirdPartyVPNPeers(organizationId, callback)</td>
    <td style="padding:15px">Return the third party VPN peers for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/thirdPartyVPNPeers?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/vpn/thirdPartyVPNPeers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationThirdPartyVPNPeers(organizationId, updateOrganizationThirdPartyVPNPeers, callback)</td>
    <td style="padding:15px">Update the third party VPN peers for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/thirdPartyVPNPeers?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/vpn/thirdPartyVPNPeers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPiiPiiKeys(networkId, username, email, mac, serial, imei, bluetoothMac, callback)</td>
    <td style="padding:15px">List the keys required to access Personally Identifiable Information (PII) for a given identifier. Exactly one identifier will be accepted. If the organization contains org-wide Systems Manager users matching the key provided then there will be an entry with the key "0" containing the applicable keys.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/piiKeys?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPiiSmDevicesForKey(networkId, username, email, mac, serial, imei, bluetoothMac, callback)</td>
    <td style="padding:15px">Given a piece of Personally Identifiable Information (PII), return the Systems Manager device ID(s) associated with that identifier. These device IDs can be used with the Systems Manager API endpoints to retrieve device details. Exactly one identifier will be accepted.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/smDevicesForKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPiiSmOwnersForKey(networkId, username, email, mac, serial, imei, bluetoothMac, callback)</td>
    <td style="padding:15px">Given a piece of Personally Identifiable Information (PII), return the Systems Manager owner ID(s) associated with that identifier. These owner IDs can be used with the Systems Manager API endpoints to retrieve owner details. Exactly one identifier will be accepted.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/smOwnersForKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPiiRequests(networkId, callback)</td>
    <td style="padding:15px">List the PII requests for this network or organization</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkPiiRequest(networkId, createNetworkPiiRequest, callback)</td>
    <td style="padding:15px">Submit a new delete or restrict processing PII request</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/requests?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPiiRequest(networkId, requestId, callback)</td>
    <td style="padding:15px">Return a PII request</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkPiiRequest(networkId, requestId, callback)</td>
    <td style="padding:15px">Delete a restrict processing PII request</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/pii/requests/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSNMPSettingsForANetwork(networkId, callback)</td>
    <td style="padding:15px">Return The SNMP Settings For A Network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/snmpSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheSNMPSettingsForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The SNMP Settings For A Network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/snmpSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSNMPSettingsForAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">Return The SNMP Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheSNMPSettingsForAnOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Update The SNMP Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceWirelessRadioSettings(networkId, serial, callback)</td>
    <td style="padding:15px">Return the radio settings of a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/wireless/radioSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceWirelessRadioSettingsV1(serial, callback)</td>
    <td style="padding:15px">Return the radio settings of a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/radio/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDeviceWirelessRadioSettings(networkId, serial, updateNetworkDeviceWirelessRadioSettings, callback)</td>
    <td style="padding:15px">Update the radio settings of a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/wireless/radioSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkDeviceWirelessRadioSettingsV1(serial, updateNetworkDeviceWirelessRadioSettings, callback)</td>
    <td style="padding:15px">Update the radio settings of a device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/radio/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkWirelessRfProfiles(networkId, includeTemplateProfiles, callback)</td>
    <td style="padding:15px">List the non-basic RF profiles for this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/rfProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationSamlRoles(organizationId, callback)</td>
    <td style="padding:15px">List the SAML roles for this organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/samlRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationSamlRole(organizationId, createOrganizationSamlRole, callback)</td>
    <td style="padding:15px">Create a SAML role</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/samlRoles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationSamlRole(organizationId, id, callback)</td>
    <td style="padding:15px">Return a SAML role</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/samlRoles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationSamlRole(organizationId, id, updateOrganizationSamlRole, callback)</td>
    <td style="padding:15px">Update a SAML role</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/samlRoles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationSamlRole(organizationId, id, callback)</td>
    <td style="padding:15px">Remove a SAML role</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/samlRoles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientSecurityEvents(networkId, clientId, t0, t1, timespan, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">List the security events for a client. Clients can be identified by a client key or either the MAC or IP depending on whether the network uses Track-by-IP.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/securityEvents?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/clients/{pathv2}/security/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSecurityEvents(networkId, t0, t1, timespan, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">List the security events for a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/securityEvents?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/security/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationSecurityEvents(organizationId, t0, t1, timespan, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">List the security events for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/securityEvents?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/security/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSecurityIntrusionSettings(networkId, callback)</td>
    <td style="padding:15px">Returns all supported intrusion settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/security/intrusionSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/security/intrusionSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSecurityIntrusionSettings(networkId, updateNetworkSecurityIntrusionSettings, callback)</td>
    <td style="padding:15px">Set the supported instrusion settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/security/intrusionSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/security/intrusionSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationSecurityIntrusionSettings(organizationId, callback)</td>
    <td style="padding:15px">Returns all supported intrusion settings for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/security/intrusionSettings?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/security/intrusionSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationSecurityIntrusionSettings(organizationId, updateOrganizationSecurityIntrusionSettings, callback)</td>
    <td style="padding:15px">Sets supported intrusion settings for an organization</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/security/intrusionSettings?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/appliance/security/intrusion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSecurityMalwareSettings(networkId, callback)</td>
    <td style="padding:15px">Returns all supported malware settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/security/malwareSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/security/malware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSecurityMalwareSettings(networkId, updateNetworkSecurityMalwareSettings, callback)</td>
    <td style="padding:15px">Set the supported malware settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/security/malwareSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/security/malware?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmTargetGroups(networkId, withDetails, callback)</td>
    <td style="padding:15px">List the target groups in this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/targetGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSmTargetGroup(networkId, createNetworkSmTargetGroup, callback)</td>
    <td style="padding:15px">Add a target group</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/targetGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmTargetGroup(networkId, targetGroupId, withDetails, callback)</td>
    <td style="padding:15px">Return a target group</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/targetGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmTargetGroup(networkId, targetGroupId, updateNetworkSmTargetGroup, callback)</td>
    <td style="padding:15px">Update a target group</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/targetGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSmTargetGroup(networkId, targetGroupId, callback)</td>
    <td style="padding:15px">Delete a target group from a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/targetGroups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSmProfileClarity(networkId, createNetworkSmProfileClarity, callback)</td>
    <td style="padding:15px">Create a new profile containing a Cisco Clarity payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/clarity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmProfileClarity(networkId, profileId, updateNetworkSmProfileClarity, callback)</td>
    <td style="padding:15px">Update an existing profile containing a Cisco Clarity payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/clarity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkSmProfileClarity(networkId, profileId, addNetworkSmProfileClarity, callback)</td>
    <td style="padding:15px">Add a Cisco Clarity payload to an existing profile</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/clarity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmProfileClarity(networkId, profileId, callback)</td>
    <td style="padding:15px">Get details for a Cisco Clarity payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/clarity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSmProfileClarity(networkId, profileId, callback)</td>
    <td style="padding:15px">Delete a Cisco Clarity payload. Deletes the entire profile if it's empty after removing the payload.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/clarity/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSmProfileUmbrella(networkId, createNetworkSmProfileUmbrella, callback)</td>
    <td style="padding:15px">Create a new profile containing a Cisco Umbrella payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/umbrella?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmProfileUmbrella(networkId, profileId, updateNetworkSmProfileUmbrella, callback)</td>
    <td style="padding:15px">Update an existing profile containing a Cisco Umbrella payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/umbrella/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkSmProfileUmbrella(networkId, profileId, addNetworkSmProfileUmbrella, callback)</td>
    <td style="padding:15px">Add a Cisco Umbrella payload to an existing profile</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/umbrella/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmProfileUmbrella(networkId, profileId, callback)</td>
    <td style="padding:15px">Get details for a Cisco Umbrella payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/umbrella/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSmProfileUmbrella(networkId, profileId, callback)</td>
    <td style="padding:15px">Delete a Cisco Umbrella payload. Deletes the entire profile if it's empty after removing the payload</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/umbrella/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSmAppPolaris(networkId, createNetworkSmAppPolaris, callback)</td>
    <td style="padding:15px">Create a new Polaris app</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/app/polaris?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmAppPolaris(networkId, bundleId, callback)</td>
    <td style="padding:15px">Get details for a Cisco Polaris app if it exists</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/app/polaris?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmAppPolaris(networkId, appId, updateNetworkSmAppPolaris, callback)</td>
    <td style="padding:15px">Update an existing Polaris app</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/app/polaris/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSmAppPolaris(networkId, appId, callback)</td>
    <td style="padding:15px">Delete a Cisco Polaris app</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/app/polaris/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmDevices(networkId, fields, wifiMacs, serials, ids, scope, batchToken, callback)</td>
    <td style="padding:15px">List the devices enrolled in an SM network with various specified fields and filters</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmDevicesV2(networkId, queryVars, callback)</td>
    <td style="padding:15px">List the devices enrolled in an SM network with various specified fields and filters</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmUsers(networkId, usernames, emails, ids, scope, callback)</td>
    <td style="padding:15px">List the owners in an SM network with various specified fields and filters</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmUserDeviceProfiles(networkId, id, callback)</td>
    <td style="padding:15px">Get the profiles associated with a user</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/user/{pathv2}/deviceProfiles?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/users/{pathv2}/deviceProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmDeviceProfiles(networkId, id, callback)</td>
    <td style="padding:15px">Get the profiles associated with a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/deviceProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmUserSoftwares(networkId, id, callback)</td>
    <td style="padding:15px">Get a list of softwares associated with a user</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/user/{pathv2}/softwares?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/users/{pathv2}/softwares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmSoftwares(networkId, id, callback)</td>
    <td style="padding:15px">Get a list of softwares associated with a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/softwares?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/softwares?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmNetworkAdapters(networkId, id, callback)</td>
    <td style="padding:15px">List the network adapters of a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/networkAdapters?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/networkAdapters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmWlanLists(networkId, id, callback)</td>
    <td style="padding:15px">List the saved SSID names on a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/wlanLists?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/wlanLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmSecurityCenters(networkId, id, callback)</td>
    <td style="padding:15px">List the security centers on a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/securityCenters?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/securityCenters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmRestrictions(networkId, id, callback)</td>
    <td style="padding:15px">List the restrictions on a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/restrictions?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/restrictions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmCerts(networkId, id, callback)</td>
    <td style="padding:15px">List the certs on a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/certs?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/certs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmDevicesTags(networkId, updateNetworkSmDevicesTags, callback)</td>
    <td style="padding:15px">Add, delete, or update the tags of a set of devices</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmDevicesTagsV1(networkId, updateNetworkSmDevicesTags, callback)</td>
    <td style="padding:15px">Add, delete, or update the tags of a set of devices</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/modifyTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmDeviceFields(networkId, updateNetworkSmDeviceFields, callback)</td>
    <td style="padding:15px">Modify the fields of a device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/device/fields?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/fields?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockNetworkSmDevices(networkId, lockNetworkSmDevices, callback)</td>
    <td style="padding:15px">Lock a set of devices</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wipeNetworkSmDevice(networkId, wipeNetworkSmDevice, callback)</td>
    <td style="padding:15px">Wipe a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/device/wipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wipeNetworkSmDeviceV1(networkId, wipeNetworkSmDevice, callback)</td>
    <td style="padding:15px">Wipe a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/wipe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkinNetworkSmDevices(networkId, checkinNetworkSmDevices, callback)</td>
    <td style="padding:15px">Force check-in a set of devices</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/checkin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkinNetworkSmDevicesV1(networkId, checkinNetworkSmDevices, callback)</td>
    <td style="padding:15px">Force check-in a set of devices</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/checkin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveNetworkSmDevices(networkId, moveNetworkSmDevices, callback)</td>
    <td style="padding:15px">Move a set of devices to a new network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveNetworkSmDevicesV1(networkId, moveNetworkSmDevices, callback)</td>
    <td style="padding:15px">Move a set of devices to a new network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unenrollNetworkSmDevice(networkId, deviceId, callback)</td>
    <td style="padding:15px">Unenroll a device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/unenroll?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSmProfileReinstall(networkId, updateNetworkSmProfileReinstall, callback)</td>
    <td style="padding:15px">Force reinstall a profile on a given device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/reinstall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmProfiles(networkId, callback)</td>
    <td style="padding:15px">List all the profiles in the network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSmProfile(networkId, deleteNetworkSmProfile, callback)</td>
    <td style="padding:15px">Delete a specified profile in the network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSmProfileCreate(networkId, createNetworkSmProfileCreate, callback)</td>
    <td style="padding:15px">Create a profile in the given network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profile/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmCellularUsageHistory(networkId, id, callback)</td>
    <td style="padding:15px">Return the client's daily cellular data usage history. Usage data is in kilobytes.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/cellularUsageHistory?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/cellularUsageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmPerformanceHistory(networkId, id, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Return historical records of various Systems Manager client metrics for desktop devices.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/performanceHistory?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/performanceHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmDesktopLogs(networkId, id, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Return historical records of various Systems Manager network connection details for desktop devices.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/desktopLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmDeviceCommandLogs(networkId, id, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Return historical records of commands sent to Systems Manager devices.
     Note that this will include the name of the Dashboard user who initiated the command if it was generated
    by a Dashboard admin rather than the automatic behavior of the system; you may wish to filter this out
    of any reports.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/deviceCommandLogs?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/deviceCommandLogs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSmConnectivity(networkId, id, perPage, startingAfter, endingBefore, callback)</td>
    <td style="padding:15px">Returns historical connectivity data (whether a device is regularly checking in to Dashboard).</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/{pathv2}/connectivity?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/connectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSplashLoginAttempts(id, ssidNumber, loginIdentifier, timespan, callback)</td>
    <td style="padding:15px">List the splash login attempts for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/splashLoginAttempts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsidSplashSettings(networkId, number, callback)</td>
    <td style="padding:15px">Display the splash page settings for the given SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/splashSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/splash/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSsidSplashSettings(networkId, number, updateNetworkSsidSplashSettings, callback)</td>
    <td style="padding:15px">Modify the splash page settings for the given SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/splashSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/splash/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsids(networkId, callback)</td>
    <td style="padding:15px">List the SSIDs in a network. Supports networks with access points or wireless-enabled security appliances and teleworker gateways.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsid(networkId, number, callback)</td>
    <td style="padding:15px">Return a single SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSsid(networkId, number, updateNetworkSsid, callback)</td>
    <td style="padding:15px">Update the attributes of an SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettings(networkId, callback)</td>
    <td style="padding:15px">Returns the switch network settings</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettings(networkId, updateNetworkSwitchSettings, callback)</td>
    <td style="padding:15px">Update switch network settings</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSwitchPorts(serial, callback)</td>
    <td style="padding:15px">List the switch ports for a switch</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/switchPorts?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/switch/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceSwitchPort(serial, number, callback)</td>
    <td style="padding:15px">Return a switch port</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/switchPorts/{pathv2}?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/switch/ports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceSwitchPort(serial, number, updateDeviceSwitchPort, callback)</td>
    <td style="padding:15px">Update a switch port</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/switchPorts/{pathv2}?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/switch/ports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationConfigTemplateSwitchProfiles(organizationId, configTemplateId, callback)</td>
    <td style="padding:15px">List the switch profiles for your switch template configuration</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}/switchProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchStacks(networkId, callback)</td>
    <td style="padding:15px">List the switch stacks in a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSwitchStack(networkId, createNetworkSwitchStack, callback)</td>
    <td style="padding:15px">Create a stack</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchStack(networkId, switchStackId, callback)</td>
    <td style="padding:15px">Show a switch stack</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSwitchStack(networkId, switchStackId, callback)</td>
    <td style="padding:15px">Delete a stack</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNetworkSwitchStack(networkId, switchStackId, addNetworkSwitchStack, callback)</td>
    <td style="padding:15px">Add a switch to a stack</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks/{pathv2}/add?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeNetworkSwitchStack(networkID, switchStackId, removeNetworkSwitchStack, callback)</td>
    <td style="padding:15px">Remove a switch from a stack</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switchStacks/{pathv2}/remove?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSyslogServers(networkId, callback)</td>
    <td style="padding:15px">List the syslog servers for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/syslogServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSyslogServers(networkId, updateNetworkSyslogServers, callback)</td>
    <td style="padding:15px">Update the syslog servers for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/syslogServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkTrafficShaping(networkId, updateNetworkTrafficShaping, callback)</td>
    <td style="padding:15px">Update the traffic shaping settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/trafficShaping?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTrafficShaping(networkId, callback)</td>
    <td style="padding:15px">Display the traffic shaping settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/trafficShaping?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSsidTrafficShaping(networkId, number, updateNetworkSsidTrafficShaping, callback)</td>
    <td style="padding:15px">Update the traffic shaping settings for an SSID on an MR network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/trafficShaping?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/trafficShaping/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSsidTrafficShaping(networkId, number, callback)</td>
    <td style="padding:15px">Display the traffic shaping settings for a SSID on an MR network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/trafficShaping?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/trafficShaping/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTrafficShapingDscpTaggingOptions(networkId, callback)</td>
    <td style="padding:15px">Returns the available DSCP tagging options for your traffic shaping rules.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/trafficShaping/dscpTaggingOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTrafficShapingApplicationCategories(networkId, callback)</td>
    <td style="padding:15px">Returns the application categories for traffic shaping rules.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/trafficShaping/applicationCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkContentFilteringCategories(networkId, callback)</td>
    <td style="padding:15px">List all available content filtering categories for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/contentFiltering/categories?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/contentFiltering/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkContentFiltering(networkId, callback)</td>
    <td style="padding:15px">Return the content filtering settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/contentFiltering?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/contentFiltering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkContentFiltering(networkId, updateNetworkContentFiltering, callback)</td>
    <td style="padding:15px">Update the content filtering settings for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/contentFiltering?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/contentFiltering?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFirewalledServices(networkId, callback)</td>
    <td style="padding:15px">List the appliance services and their accessibility rules</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/firewalledServices?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/firewalledServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFirewalledService(networkId, service, callback)</td>
    <td style="padding:15px">Return the accessibility settings of the given service ('ICMP', 'web', or 'SNMP')</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/firewalledServices/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/firewalledServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkFirewalledService(networkId, service, updateNetworkFirewalledService, callback)</td>
    <td style="padding:15px">Updates the accessibility settings for the given service ('ICMP', 'web', or 'SNMP')</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/firewalledServices/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/firewalledServices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkOneToManyNatRules(networkId, callback)</td>
    <td style="padding:15px">Return the 1:Many NAT mapping rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/oneToManyNatRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/oneToManyNatRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkOneToManyNatRules(networkId, updateNetworkOneToManyNatRules, callback)</td>
    <td style="padding:15px">Set the 1:Many NAT mapping rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/oneToManyNatRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/oneToManyNatRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkOneToOneNatRules(networkId, callback)</td>
    <td style="padding:15px">Return the 1:1 NAT mapping rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/oneToOneNatRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/oneToOneNatRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkOneToOneNatRules(networkId, updateNetworkOneToOneNatRules, callback)</td>
    <td style="padding:15px">Set the 1:1 NAT mapping rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/oneToOneNatRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/oneToOneNatRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkPortForwardingRules(networkId, callback)</td>
    <td style="padding:15px">Return the port forwarding rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/portForwardingRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/portForwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkPortForwardingRules(networkId, updateNetworkPortForwardingRules, callback)</td>
    <td style="padding:15px">Update the port forwarding rules for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/portForwardingRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/firewall/portForwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkStaticRoutes(networkId, callback)</td>
    <td style="padding:15px">List the static routes for this network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/staticRoutes?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkStaticRoute(networkId, createNetworkStaticRoute, callback)</td>
    <td style="padding:15px">Add a static route</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/staticRoutes?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkStaticRoute(networkId, srId, callback)</td>
    <td style="padding:15px">Return a static route</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/staticRoutes/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkStaticRoute(networkId, srId, updateNetworkStaticRoute, callback)</td>
    <td style="padding:15px">Update a static route</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/staticRoutes/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkStaticRoute(networkId, srId, callback)</td>
    <td style="padding:15px">Delete a static route from a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/staticRoutes/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkUplinkSettings(networkId, callback)</td>
    <td style="padding:15px">Returns the uplink settings for your MX network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/uplinkSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkUplinkSettings(networkId, updateNetworkUplinkSettings, callback)</td>
    <td style="padding:15px">Updates the uplink settings for your MX network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/uplinkSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVlans(networkId, callback)</td>
    <td style="padding:15px">List the VLANs for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/vlans?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkVlan(networkId, createNetworkVlan, callback)</td>
    <td style="padding:15px">Add a VLAN</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/vlans?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVlan(networkId, vlanId, callback)</td>
    <td style="padding:15px">Return a VLAN</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/vlans/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/vlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVlan(networkId, vlanId, updateNetworkVlan, callback)</td>
    <td style="padding:15px">Update a VLAN</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/vlans/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/vlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkVlan(networkId, vlanId, callback)</td>
    <td style="padding:15px">Delete a VLAN from a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/vlans/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/vlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkVlansEnabledState(networkId, callback)</td>
    <td style="padding:15px">Returns the enabled status of VLANs for the network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/vlansEnabledState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkVlansEnabledState(networkId, updateNetworkVlansEnabledState, callback)</td>
    <td style="padding:15px">Enable/Disable VLANs for the given network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/vlansEnabledState?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConnectionStats(networkId, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for this network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/connectionStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevicesConnectionStats(networkId, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for this network, grouped by node</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/devices/connectionStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/devices/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceConnectionStats(networkId, serial, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for a given AP on this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceConnectionStatsV1(serial, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for a given AP on this network</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientsConnectionStats(networkId, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for this network, grouped by clients</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/clients/connectionStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/clients/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientConnectionStats(networkId, clientId, t0, t1, timespan, ssid, vlan, apTag, callback)</td>
    <td style="padding:15px">Aggregated connectivity info for a given client on this network. Clients are identified by their MAC.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/connectionStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/clients/{pathv2}/connectionStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkLatencyStats(networkId, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for this network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/latencyStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDevicesLatencyStats(networkId, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for this network, grouped by node</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/devices/latencyStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/devices/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLatencyStats(networkId, serial, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for a given AP on this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/{pathv2}/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkDeviceLatencyStatsV1(serial, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for a given AP on this network</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientsLatencyStats(networkId, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for this network, grouped by clients</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/clients/latencyStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/clients/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkClientLatencyStats(networkId, clientId, t0, t1, timespan, ssid, vlan, apTag, fields, callback)</td>
    <td style="padding:15px">Aggregated latency info for a given client on this network. Clients are identified by their MAC.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/clients/{pathv2}/latencyStats?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/clients/{pathv2}/latencyStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFailedConnections(networkId, t0, t1, timespan, ssid, vlan, apTag, serial, clientId, callback)</td>
    <td style="padding:15px">List of all failed client connection events on this network in a given time range</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/failedConnections?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/failedConnections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCameraQualityRetentionProfiles(networkId, callback)</td>
    <td style="padding:15px">List the quality retention profiles for this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/qualityRetentionProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkCameraQualityRetentionProfile(body, networkId, callback)</td>
    <td style="padding:15px">Creates new quality retention profile for this network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/qualityRetentionProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCameraQualityRetentionProfile(networkId, qualityRetentionProfileId, callback)</td>
    <td style="padding:15px">Retrieve a single quality retention profile</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/qualityRetentionProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCameraQualityRetentionProfile(body, networkId, qualityRetentionProfileId, callback)</td>
    <td style="padding:15px">Update an existing quality retention profile for this network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/qualityRetentionProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkCameraQualityRetentionProfile(networkId, qualityRetentionProfileId, callback)</td>
    <td style="padding:15px">Delete an existing quality retention profile for this network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/qualityRetentionProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(networkId, callback)</td>
    <td style="padding:15px">Return the connectivity testing destinations for an MG network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/connectivityMonitoringDestinations?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/connectivityMonitoringDestinations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(networkId, body, callback)</td>
    <td style="padding:15px">Update the connectivity testing destinations for an MG network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/connectivityMonitoringDestinations?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/connectivityMonitoringDestinations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCellularGatewaySettingsDhcp(networkId, callback)</td>
    <td style="padding:15px">List common DHCP settings of MGs</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/dhcp?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCellularGatewaySettingsDhcp(networkId, body, callback)</td>
    <td style="padding:15px">Update common DHCP settings of MGs</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/dhcp?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCellularGatewaySettings(serial, callback)</td>
    <td style="padding:15px">Show the LAN Settings of a MG</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/cellularGateway/settings?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/cellularGateway/lan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheEAPOverriddenParametersForAnSSID(networkId, number, callback)</td>
    <td style="padding:15px">Return The EAP Overridden Parameters For An SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/eap_override?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/eapOverride?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheEAPOverriddenParametersForAnSSID(networkId, number, body, callback)</td>
    <td style="padding:15px">Update The EAP Overridden Parameters For An SSID</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/ssids/{pathv2}/eap_override?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/eapOverride?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceCellularGatewaySettings(serial, body, callback)</td>
    <td style="padding:15px">Update the LAN Settings for a single MG.</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/cellularGateway/settings?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/cellularGateway/lan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCellularGatewaySettingsPortForwardingRules(serial, callback)</td>
    <td style="padding:15px">Returns the port forwarding rules for a single MG.</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/cellularGateway/settings/portForwardingRules?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/cellularGateway/portForwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDeviceCellularGatewaySettingsPortForwardingRules(serial, body, callback)</td>
    <td style="padding:15px">Updates the port forwarding rules for a single MG.</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/cellularGateway/settings/portForwardingRules?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/cellularGateway/portForwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCellularGatewaySettingsSubnetPool(networkId, callback)</td>
    <td style="padding:15px">Return the subnet pool and mask configured for MGs in the network.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/subnetPool?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/subnetPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCellularGatewaySettingsSubnetPool(networkId, body, callback)</td>
    <td style="padding:15px">Update the subnet pool and mask configuration for MGs in the network.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/subnetPool?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/subnetPool?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkCellularGatewaySettingsUplink(networkId, callback)</td>
    <td style="padding:15px">Returns the uplink settings for your MG network.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/uplink?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/uplink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkCellularGatewaySettingsUplink(networkId, body, callback)</td>
    <td style="padding:15px">Updates the uplink settings for your MG network.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/cellularGateway/settings/uplink?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/cellularGateway/uplink?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchAccessControlLists(networkId, callback)</td>
    <td style="padding:15px">Return the access control lists for a MS network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessControlLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchAccessControlLists(body, networkId, callback)</td>
    <td style="padding:15px">Update the access control lists for a MS network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessControlLists?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheAccessPoliciesForASwitchNetwork(networkId, callback)</td>
    <td style="padding:15px">List The Access Policies For A Switch Network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/accessPolicies?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/accessPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnAccessPolicyForASwitchNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Create An Access Policy For A Switch Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnASpecificAccessPolicyForASwitchNetwork(networkId, accessPolicyNumber, callback)</td>
    <td style="padding:15px">Return A Specific Access Policy For A Switch Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnAccessPolicyForASwitchNetwork(networkId, accessPolicyNumber, body, callback)</td>
    <td style="padding:15px">Update An Access Policy For A Switch Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAccessPolicyForASwitchNetwork(networkId, accessPolicyNumber, callback)</td>
    <td style="padding:15px">Delete An Access Policy For A Switch Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/accessPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheStatusForAllThePortsOfASwitch(serial, callback)</td>
    <td style="padding:15px">Return The Status For All The Ports Of A Switch</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/switchPortStatuses?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/switch/ports/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnThePacketCountersForAllThePortsOfASwitch(serial, callback)</td>
    <td style="padding:15px">Return the packet counters for all the ports of a switch</td>
    <td style="padding:15px">v0:{base_path}/{version}/devices/{pathv1}/switchPortStatuses/packets?{query} <br /> v1:{base_path}/{version}/devices/{pathv1}/switch/ports/statuses/packets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheUplinkBandwidthSettingsForYourMXNetwork(networkId, callback)</td>
    <td style="padding:15px">Returns The Uplink Bandwidth Settings For Your MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/uplinkBandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatesTheUplinkBandwidthSettingsForYourMXNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Updates The Uplink Bandwidth Settings For Your MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/uplinkBandwidth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshTheDetailsOfADevice(networkId, deviceId, callback)</td>
    <td style="padding:15px">Refresh The Details Of A Device</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/sm/device/{pathv2}/refreshDetails?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/sm/devices/{pathv2}/refreshDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchPortSchedules(networkId, callback)</td>
    <td style="padding:15px">List switch port schedules</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/portSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSwitchPortSchedule(networkId, body, callback)</td>
    <td style="padding:15px">Add a switch port schedule</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/portSchedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchPortSchedule(networkId, portScheduleId, body, callback)</td>
    <td style="padding:15px">Update the access control lists for a MS network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/portSchedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSwitchPortSchedule(networkId, portScheduleId, callback)</td>
    <td style="padding:15px">Delete a switch port schedule</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/portSchedules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAppliancePorts(networkId, callback)</td>
    <td style="padding:15px">List per-port VLAN settings for all ports of a MX.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/appliancePorts?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAppliancePort(networkId, appliancePortId, callback)</td>
    <td style="padding:15px">Return per-port VLAN settings for a single MX port.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/appliancePorts/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/ports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkAppliancePort(body, networkId, appliancePortId, callback)</td>
    <td style="padding:15px">Update the per-port VLAN settings for a single MX port.</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/appliancePorts/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/ports/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">swapNetworkWarmspare(networkId, body, callback)</td>
    <td style="padding:15px">Swap MX primary and warm spare appliances</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/swapWarmSpare?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/WarmSpare/swap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkWarmSpareSettings(networkId, callback)</td>
    <td style="padding:15px">Return MX warm spare settings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/warmSpareSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/warmSpare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkWarmSpareSettings(networkId, body, callback)</td>
    <td style="padding:15px">Update MX warm spare settings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/warmSpareSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/warmSpare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationLicenses(perPage, startingAfter, endingBefore, deviceSerial, networkId, state, organizationId, callback)</td>
    <td style="padding:15px">List the licenses for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationLicense(organizationId, licenseId, callback)</td>
    <td style="padding:15px">Display a license</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationLicense(body, organizationId, licenseId, callback)</td>
    <td style="padding:15px">Update a license</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignOrganizationLicensesSeats(organizationId, body, callback)</td>
    <td style="padding:15px">Assign SM seats to a network.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/assignSeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveOrganizationLicenses(organizationId, body, callback)</td>
    <td style="padding:15px">Move licenses to another organization.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/move?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveOrganizationLicensesSeats(body, organizationId, callback)</td>
    <td style="padding:15px">Move SM seats to another organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/moveSeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renewOrganizationLicensesSeats(body, organizationId, callback)</td>
    <td style="padding:15px">Renew SM seats of a license.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/renewSeats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsDhcpServerPolicy(networkId, callback)</td>
    <td style="padding:15px">Return the DHCP server policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/dhcpServerPolicy?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsDhcpServerPolicy(body, networkId, callback)</td>
    <td style="padding:15px">Update the DHCP server policy</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/dhcpServerPolicy?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsDscpToCosMappings(networkId, callback)</td>
    <td style="padding:15px">Return the DSCP to CoS mappings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/dscpToCosMappings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/dscpToCosMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsDscpToCosMappings(body, networkId, callback)</td>
    <td style="padding:15px">Update the DSCP to CoS mappings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/dscpToCosMappings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/dscpToCosMappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsMtu(networkId, callback)</td>
    <td style="padding:15px">Return the MTU configuration</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/mtu?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/mtu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsMtu(body, networkId, callback)</td>
    <td style="padding:15px">Update the MTU configuration</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/mtu?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/mtu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsQosRules(networkId, callback)</td>
    <td style="padding:15px">List quality of service rules</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSwitchSettingsQosRule(body, networkId, callback)</td>
    <td style="padding:15px">Add a quality of service rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsQosRulesOrder(networkId, callback)</td>
    <td style="padding:15px">Return the quality of service rule IDs by order in which they will be processed by the switch</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules/order?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules/order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsQosRulesOrder(body, networkId, callback)</td>
    <td style="padding:15px">Update the order in which the rules should be processed by the switch</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules/order?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules/order?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsQosRule(networkId, qosRuleId, callback)</td>
    <td style="padding:15px">Return a quality of service rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSwitchSettingsQosRule(networkId, qosRuleId, callback)</td>
    <td style="padding:15px">Delete a quality of service rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsQosRule(networkId, qosRuleId, body, callback)</td>
    <td style="padding:15px">Update a quality of service rule</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/qosRules/{pathv2}?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/qosRules/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsStormControl(networkId, callback)</td>
    <td style="padding:15px">Return the storm control configuration for a switch network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/stormControl?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stormControl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsStormControl(networkId, body, callback)</td>
    <td style="padding:15px">Update the storm control configuration for a switch network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/stormControl?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stormControl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchSettingsStp(networkId, callback)</td>
    <td style="padding:15px">Returns STP settings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/stp?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchSettingsStp(body, networkId, callback)</td>
    <td style="padding:15px">Updates STP settings</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/switch/settings/stp?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/switch/stp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkConnectivityMonitoringDestinations(networkId, callback)</td>
    <td style="padding:15px">Return the connectivity testing destinations for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/connectivityMonitoringDestinations?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/connectivityMonitoringDestinations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkConnectivityMonitoringDestinations(networkId, body, callback)</td>
    <td style="padding:15px">Update the connectivity testing destinations for an MX network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/connectivityMonitoringDestinations?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/appliance/connectivityMonitoringDestinations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationBrandingPolicies(organizationId, callback)</td>
    <td style="padding:15px">List the branding policies of an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrganizationBrandingPolicy(organizationId, body, callback)</td>
    <td style="padding:15px">Add a new branding policy to an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationBrandingPoliciesPriorities(organizationId, callback)</td>
    <td style="padding:15px">Return the branding policy IDs of an organization in priority order.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies/priorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationBrandingPoliciesPriorities(organizationId, body, callback)</td>
    <td style="padding:15px">Update the priority ordering of an organization's branding policies.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies/priorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationBrandingPolicy(organizationId, callback)</td>
    <td style="padding:15px">Return a branding policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies/:brandingPolicyId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateOrganizationBrandingPolicy(organizationId, brandingPolicyId, body, callback)</td>
    <td style="padding:15px">Update a branding policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationBrandingPolicy(organizationId, brandingPolicyId, callback)</td>
    <td style="padding:15px">Delete a branding policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/brandingPolicies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEvents(productType, includedEventTypes, excludedEventTypes, deviceMac, deviceSerial, deviceName, clientIp, clientMac, clientName, smDeviceMac, smDeviceName, perPage, startingAfter, endingBefore, networkId, callback)</td>
    <td style="padding:15px">List the events for the network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkEventsEventTypes(networkId, callback)</td>
    <td style="padding:15px">List the event type to human-readable description</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/events/eventTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFloorPlans(networkId, callback)</td>
    <td style="padding:15px">List the floor plans that belong to your network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/floorPlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkFloorPlan(networkId, body, callback)</td>
    <td style="padding:15px">Upload a floor plan</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/floorPlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkFloorPlan(networkId, floorPlanId, callback)</td>
    <td style="padding:15px">Find a floor plan by ID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/floorPlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkFloorPlan(networkId, floorPlanId, body, callback)</td>
    <td style="padding:15px">Update a floor plan's geolocation and other meta data</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/floorPlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkFloorPlan(networkId, floorPlanId, callback)</td>
    <td style="padding:15px">Destroy a floor plan</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/floorPlans/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkSwitchLinkAggregations(networkId, callback)</td>
    <td style="padding:15px">List link aggregation groups</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/linkAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkSwitchLinkAggregation(networkId, body, callback)</td>
    <td style="padding:15px">Create a link aggregation group</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/linkAggregations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkSwitchLinkAggregation(networkId, linkAggregationId, body, callback)</td>
    <td style="padding:15px">Update a link aggregation group</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/linkAggregations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkSwitchLinkAggregation(networkId, linkAggregationId, callback)</td>
    <td style="padding:15px">Split a link aggregation group into separate ports</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/linkAggregations/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkNetflowSettings(networkId, callback)</td>
    <td style="padding:15px">Return the NetFlow traffic reporting settings for a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/netflowSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/netflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkNetflowSettings(networkId, body, callback)</td>
    <td style="padding:15px">Update the NetFlow traffic reporting settings for a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/netflowSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/netflow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkTrafficAnalysisSettings(networkId, callback)</td>
    <td style="padding:15px">Return the traffic analysis settings for a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/trafficAnalysisSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/trafficAnalysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkTrafficAnalysisSettings(networkId, callback)</td>
    <td style="padding:15px">Update the traffic analysis settings for a network</td>
    <td style="padding:15px">v0:{base_path}/{version}/networks/{pathv1}/trafficAnalysisSettings?{query} <br /> v1:{base_path}/{version}/networks/{pathv1}/trafficAnalysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationWebhookLogs(t0, t1, timespan, perPage, startingAfter, endingBefore, url, organizationId, callback)</td>
    <td style="padding:15px">Return the log of webhook POSTs sent</td>
    <td style="padding:15px">v0:{base_path}/{version}/organizations/{pathv1}/webhookLogs?{query} <br /> v1:{base_path}/{version}/organizations/{pathv1}/webhooks/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkWirelessSettings(networkId, callback)</td>
    <td style="padding:15px">Return the wireless settings for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkWirelessSettings(networkId, body, callback)</td>
    <td style="padding:15px">Update the wireless settings for a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnstheidentityofthecurrentuser(callback)</td>
    <td style="padding:15px">Returns the identity of the current user.</td>
    <td style="padding:15px">{base_path}/{version}/administered/identities/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returntheDHCPsubnetinformationforanappliance(serial, callback)</td>
    <td style="padding:15px">Return the DHCP subnet information for an appliance</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/appliance/dhcp/subnets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getthesentandreceivedbytesforeachuplinkofanetwork(networkId, callback)</td>
    <td style="padding:15px">Get the sent and received bytes for each uplink of a network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/uplinks/usageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAPusageovertimeforadeviceornetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return AP usage over time for a device or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/usageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization(organizationId, callback)</td>
    <td style="padding:15px">List the uplink status of every Meraki MX and Z series appliances in the organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/appliance/uplink/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchOnboardingStatusOfCameras(organizationId, callback)</td>
    <td style="padding:15px">Fetch Onboarding Status Of Cameras</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/onboarding/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">notifyThatCredentialHandoffToCameraHasCompleted(organizationId, body, callback)</td>
    <td style="padding:15px">Notify That Credential Handoff To Camera Has Completed</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/onboarding/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization(organizationId, callback)</td>
    <td style="padding:15px">List the uplink status of every Meraki MG cellular gateway in the organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/cellularGateway/uplink/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization(organizationId, callback)</td>
    <td style="padding:15px">List the uplink status of every Meraki MX, MG and Z series devices in the organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/uplinks/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPNstatusfornetworksinanorganization(organizationId, callback)</td>
    <td style="padding:15px">Show VPN status for networks in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/appliance/vpn/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheStatusOfEveryMerakiDeviceInTheOrganization(organizationId, callback)</td>
    <td style="padding:15px">List The Status Of Every Meraki Device In The Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices/statuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showVPNhistorystatfornetworksinanorganization(organizationId, callback)</td>
    <td style="padding:15px">Show VPN history stat for networks in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/appliance/vpn/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheFirewallSettingsForThisNetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Firewall Settings For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/firewall/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheFirewallSettingsForThisNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Firewall Settings For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/firewall/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheApplianceSettingsForANetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Appliance Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheApplianceSettingsForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Appliance Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheEnabledStatusOfVLANsForTheNetwork(networkId, callback)</td>
    <td style="padding:15px">Returns The Enabled Status Of VLA Ns For The Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/vlans/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableDisableVLANsForTheGivenNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Enable Disable VLA Ns For The Given Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/vlans/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSettingsForANetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheSettingsForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsGlobalAdaptivePolicySettingsInAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">Returns Global Adaptive Policy Settings In An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalAdaptivePolicySettings(organizationId, body, callback)</td>
    <td style="padding:15px">Update Global Adaptive Policy Settings</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheBluetoothSettingsForAWirelessDevice(serial, callback)</td>
    <td style="padding:15px">Return The Bluetooth Settings For A Wireless Device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/bluetooth/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheBluetoothSettingsForAWirelessDevice(serial, body, callback)</td>
    <td style="padding:15px">Update The Bluetooth Settings For A Wireless Device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/wireless/bluetooth/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Bluetooth Settings For A Network A Href Https Documentation Meraki Com MR Bluetooth Blue</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/bluetooth/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheBluetoothSettingsForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Bluetooth Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/bluetooth/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAllThePortsOfASwitchProfile(organizationId, configTemplateId, profileId, callback)</td>
    <td style="padding:15px">Return All The Ports Of A Switch Profile</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}/switch/profiles/{pathv3}/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnASwitchProfilePort(organizationId, configTemplateId, profileId, portId, callback)</td>
    <td style="padding:15px">Return A Switch Profile Port</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}/switch/profiles/{pathv3}/ports/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateASwitchProfilePort(organizationId, configTemplateId, profileId, portId, body, callback)</td>
    <td style="padding:15px">Update A Switch Profile Port</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}/switch/profiles/{pathv3}/ports/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnSingleLANConfiguration(networkId, callback)</td>
    <td style="padding:15px">Return Single LAN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/singleLan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSingleLANConfiguration(networkId, body, callback)</td>
    <td style="padding:15px">Update Single LAN Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/singleLan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLayer3StaticRoutesForASwitch(serial, callback)</td>
    <td style="padding:15px">List Layer 3 Static Routes For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createALayer3StaticRouteForASwitch(serial, body, callback)</td>
    <td style="padding:15px">Create A Layer 3 Static Route For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3StaticRouteForASwitch(serial, staticRouteId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Static Route For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3StaticRouteForASwitch(serial, staticRouteId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Static Route For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteALayer3StaticRouteForASwitch(serial, staticRouteId, callback)</td>
    <td style="padding:15px">Delete A Layer 3 Static Route For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/staticRoutes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLayer3StaticRoutesForASwitchStack(networkId, switchStackId, callback)</td>
    <td style="padding:15px">List Layer 3 Static Routes For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createALayer3StaticRouteForASwitchStack(networkId, switchStackId, body, callback)</td>
    <td style="padding:15px">Create A Layer 3 Static Route For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/staticRoutes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3StaticRouteForASwitchStack(networkId, switchStackId, staticRouteId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Static Route For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/staticRoutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3StaticRouteForASwitchStack(networkId, switchStackId, staticRouteId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Static Route For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/staticRoutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteALayer3StaticRouteForASwitchStack(networkId, switchStackId, staticRouteId, callback)</td>
    <td style="padding:15px">Delete A Layer 3 Static Route For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/staticRoutes/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllCustomPerformanceClassesForAnMXNetwork(networkId, callback)</td>
    <td style="padding:15px">List All Custom Performance Classes For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/customPerformanceClasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addACustomPerformanceClassForAnMXNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Add A Custom Performance Class For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/customPerformanceClasses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnACustomPerformanceClassForAnMXNetwork(networkId, customPerformanceClassId, callback)</td>
    <td style="padding:15px">Return A Custom Performance Class For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/customPerformanceClasses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateACustomPerformanceClassForAnMXNetwork(networkId, customPerformanceClassId, body, callback)</td>
    <td style="padding:15px">Update A Custom Performance Class For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/customPerformanceClasses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteACustomPerformanceClassFromAnMXNetwork(networkId, customPerformanceClassId, callback)</td>
    <td style="padding:15px">Delete A Custom Performance Class From An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/customPerformanceClasses/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheTrafficShapingSettingsRulesForAnMXNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Traffic Shaping Settings Rules For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">displayTheTrafficShapingSettingsRulesForAnMXNetwork(networkId, callback)</td>
    <td style="padding:15px">Display The Traffic Shaping Settings Rules For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showUplinkSelectionSettingsForAnMXNetwork(networkId, callback)</td>
    <td style="padding:15px">Show Uplink Selection Settings For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/uplinkSelection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateUplinkSelectionSettingsForAnMXNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update Uplink Selection Settings For An MX Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/trafficShaping/uplinkSelection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAHubBGPConfiguration(networkId, callback)</td>
    <td style="padding:15px">Return A Hub BGP Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/vpn/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAHubBGPConfiguration(networkId, body, callback)</td>
    <td style="padding:15px">Update A Hub BGP Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/appliance/vpn/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnWarmSpareConfigurationForASwitch(serial, callback)</td>
    <td style="padding:15px">Return Warm Spare Configuration For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/warmSpare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateWarmSpareConfigurationForASwitch(serial, body, callback)</td>
    <td style="padding:15px">Update Warm Spare Configuration For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/warmSpare?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnoverviewstatisticsfornetworkclients(networkId, callback)</td>
    <td style="padding:15px">Return overview statistics for network clients</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization(organizationId, callback)</td>
    <td style="padding:15px">Return summary information around client data usage (in mb) across the given organization.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/clients/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnanoverviewofcurrentdevicestatuses(organizationId, callback)</td>
    <td style="padding:15px">Return an overview of current device statuses</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices/statuses/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsadaptivepolicyaggregatestatisticsforanorganization(organizationId, callback)</td>
    <td style="padding:15px">Returns adaptive policy aggregate statistics for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnanoverviewofthelicensestateforanorganization(organizationId, callback)</td>
    <td style="padding:15px">Return an overview of the license state for an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/licenses/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp(organizationId, callback)</td>
    <td style="padding:15px">Return all reported readings from sensors in a given timespan, sorted by timestamp</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/sensor/readings/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listCustomAnalyticsArtifacts(organizationId, callback)</td>
    <td style="padding:15px">List Custom Analytics Artifacts</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/customAnalytics/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomAnalyticsArtifact(organizationId, body, callback)</td>
    <td style="padding:15px">Create Custom Analytics Artifact</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/customAnalytics/artifacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAnalyticsArtifact(organizationId, artifactId, callback)</td>
    <td style="padding:15px">Get Custom Analytics Artifact</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/customAnalytics/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAnalyticsArtifact(organizationId, artifactId, callback)</td>
    <td style="padding:15px">Delete Custom Analytics Artifact</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/camera/customAnalytics/artifacts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnCustomAnalyticsSettingsForACamera(serial, callback)</td>
    <td style="padding:15px">Return Custom Analytics Settings For A Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/customAnalytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomAnalyticsSettingsForACamera(serial, body, callback)</td>
    <td style="padding:15px">Update Custom Analytics Settings For A Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/customAnalytics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnstheMVSenseobjectdetectionmodellistforthegivencamera(serial, callback)</td>
    <td style="padding:15px">Returns the MV Sense object detection model list for the given camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/sense/objectDetectionModels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsSenseSettingsForAGivenCamera(serial, callback)</td>
    <td style="padding:15px">Returns Sense Settings For A Given Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/sense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSenseSettingsForTheGivenCamera(serial, body, callback)</td>
    <td style="padding:15px">Update Sense Settings For The Given Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/sense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsWirelessProfileAssignedToTheGivenCamera(serial, callback)</td>
    <td style="padding:15px">Returns Wireless Profile Assigned To The Given Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/wirelessProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignWirelessProfilesToTheGivenCamera(serial, body, callback)</td>
    <td style="padding:15px">Assign Wireless Profiles To The Given Camera</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/camera/wirelessProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsANewCameraWirelessProfileForThisNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Creates A New Camera Wireless Profile For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/wirelessProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheCameraWirelessProfilesForThisNetwork(networkId, callback)</td>
    <td style="padding:15px">List The Camera Wireless Profiles For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/wirelessProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveASingleCameraWirelessProfile(networkId, wirelessProfileId, callback)</td>
    <td style="padding:15px">Retrieve A Single Camera Wireless Profile</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/wirelessProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnExistingCameraWirelessProfileInThisNetwork(networkId, wirelessProfileId, body, callback)</td>
    <td style="padding:15px">Update An Existing Camera Wireless Profile In This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/wirelessProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnExistingCameraWirelessProfileForThisNetwork(networkId, wirelessProfileId, callback)</td>
    <td style="padding:15px">Delete An Existing Camera Wireless Profile For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/camera/wirelessProfiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheOutageScheduleForTheSSID(networkId, number, callback)</td>
    <td style="padding:15px">List The Outage Schedule For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheOutageScheduleForTheSSID(networkId, number, body, callback)</td>
    <td style="padding:15px">Update The Outage Schedule For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/schedules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3InterfaceDHCPConfigurationForASwitch(serial, interfaceId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Interface DHCP Configuration For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces/{pathv2}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3InterfaceDHCPConfigurationForASwitch(serial, interfaceId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Interface DHCP Configuration For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces/{pathv2}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3InterfaceDHCPConfigurationForASwitchStack(networkId, switchStackId, interfaceId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Interface DHCP Configuration For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces/{pathv3}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3InterfaceDHCPConfigurationForASwitchStack(networkId, switchStackId, interfaceId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Interface DHCP Configuration For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces/{pathv3}/dhcp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheSwitchPortProfilesinanetwork(networkId, callback)</td>
    <td style="padding:15px">List the Switch Port Profiles in a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/ports/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallprofilesinanetwork(networkId, callback)</td>
    <td style="padding:15px">List all profiles in a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnOrganizationWideAlertConfiguration(organizationId, body, callback)</td>
    <td style="padding:15px">Create An Organization Wide Alert Configuration</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/alerts/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllOrganizationWideAlertConfigurations(organizationId, callback)</td>
    <td style="padding:15px">List All Organization Wide Alert Configurations</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/alerts/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removesAnOrganizationWideAlertConfig(organizationId, alertConfigId, callback)</td>
    <td style="padding:15px">Removes An Organization Wide Alert Config</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/alerts/profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnOrganizationWideAlertConfig(organizationId, alertConfigId, body, callback)</td>
    <td style="padding:15px">Update An Organization Wide Alert Config</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/alerts/profiles/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheswitchportsinanorganizationbyswitch(organizationId, callback)</td>
    <td style="padding:15px">List the switchports in an organization by switch</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/switch/ports/bySwitch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLayer3InterfacesForASwitch(serial, callback)</td>
    <td style="padding:15px">List Layer 3 Interfaces For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createALayer3InterfaceForASwitch(serial, body, callback)</td>
    <td style="padding:15px">Create A Layer 3 Interface For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3InterfaceForASwitch(serial, interfaceId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Interface For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3InterfaceForASwitch(serial, interfaceId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Interface For A Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteALayer3InterfaceFromTheSwitch(serial, interfaceId, callback)</td>
    <td style="padding:15px">Delete A Layer 3 Interface From The Switch</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/switch/routing/interfaces/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listLayer3InterfacesForASwitchStack(networkId, switchStackId, callback)</td>
    <td style="padding:15px">List Layer 3 Interfaces For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createALayer3InterfaceForASwitchStack(networkId, switchStackId, body, callback)</td>
    <td style="padding:15px">Create A Layer 3 Interface For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnALayer3InterfaceFromASwitchStack(networkId, switchStackId, interfaceId, callback)</td>
    <td style="padding:15px">Return A Layer 3 Interface From A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateALayer3InterfaceForASwitchStack(networkId, switchStackId, interfaceId, body, callback)</td>
    <td style="padding:15px">Update A Layer 3 Interface For A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteALayer3InterfaceFromASwitchStack(networkId, switchStackId, interfaceId, callback)</td>
    <td style="padding:15px">Delete A Layer 3 Interface From A Switch Stack</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/stacks/{pathv2}/routing/interfaces/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listMulticastRendezvousPoints(networkId, callback)</td>
    <td style="padding:15px">List Multicast Rendezvous Points</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast/rendezvousPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAMulticastRendezvousPoint(networkId, body, callback)</td>
    <td style="padding:15px">Create A Multicast Rendezvous Point</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast/rendezvousPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAMulticastRendezvousPoint(networkId, rendezvousPointId, callback)</td>
    <td style="padding:15px">Return A Multicast Rendezvous Point</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast/rendezvousPoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAMulticastRendezvousPoint(networkId, rendezvousPointId, callback)</td>
    <td style="padding:15px">Delete A Multicast Rendezvous Point</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast/rendezvousPoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAMulticastRendezvousPoint(networkId, rendezvousPointId, body, callback)</td>
    <td style="padding:15px">Update A Multicast Rendezvous Point</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast/rendezvousPoints/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnMulticastSettingsForANetwork(networkId, callback)</td>
    <td style="padding:15px">Return Multicast Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMulticastSettingsForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update Multicast Settings For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnLayer3OSPFRoutingConfiguration(networkId, callback)</td>
    <td style="padding:15px">Return Layer 3 OSPF Routing Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateLayer3OSPFRoutingConfiguration(networkId, body, callback)</td>
    <td style="padding:15px">Update Layer 3 OSPF Routing Configuration</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/routing/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheSwitchAlternateManagementInterfaceForTheNetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Switch Alternate Management Interface For The Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/alternateManagementInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheSwitchAlternateManagementInterfaceForTheNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update The Switch Alternate Management Interface For The Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/alternateManagementInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAlternateManagementInterfaceAndDevicesWithIPAssigned(networkId, callback)</td>
    <td style="padding:15px">Return Alternate Management Interface And Devices With IP Assigned</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/alternateManagementInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAlternateManagementInterfaceAndDeviceStaticIP(networkId, body, callback)</td>
    <td style="padding:15px">Update Alternate Management Interface And Device Static IP</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/alternateManagementInterface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day(networkId, callback)</td>
    <td style="padding:15px">Return the network's DHCPv4 servers seen within the selected timeframe (default 1 day)</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcp/v4/servers/seen?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork(networkId, callback)</td>
    <td style="padding:15px">Return The List Of Servers Trusted By Dynamic ARP Inspection On This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy/arpInspection/trustedServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork(networkId, body, callback)</td>
    <td style="padding:15px">Add A Server To Be Trusted By Dynamic ARP Inspection On This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy/arpInspection/trustedServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork(networkId, trustedServerId, body, callback)</td>
    <td style="padding:15px">Update A Server That Is Trusted By Dynamic ARP Inspection On This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy/arpInspection/trustedServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork(networkId, trustedServerId, callback)</td>
    <td style="padding:15px">Remove A Server From Being Trusted By Dynamic ARP Inspection On This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy/arpInspection/trustedServers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings(networkId, callback)</td>
    <td style="padding:15px">Return the devices that have a Dynamic ARP Inspection warning and their warnings</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/switch/dhcpServerPolicy/arpInspection/warnings/byDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthepowerstatusinformationfordevicesinanorganization(organizationId, callback)</td>
    <td style="padding:15px">List the power status information for devices in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices/powerModules/statuses/byDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthecurrentuplinkaddressesfordevicesinanorganization(organizationId, callback)</td>
    <td style="padding:15px">List the current uplink addresses for devices in an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices/uplinks/addresses/byDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches(organizationId, body, callback)</td>
    <td style="padding:15px">Clone port-level and some switch-level configuration settings from a source switch to one or more t</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/switch/devices/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnASingleDeviceFromTheInventoryOfAnOrganization(organizationId, serial, callback)</td>
    <td style="padding:15px">Return A Single Device From The Inventory Of An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/inventory/devices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnTheBillingSettingsOfThisNetwork(networkId, callback)</td>
    <td style="padding:15px">Return The Billing Settings Of This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/billing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheBillingSettings(networkId, body, callback)</td>
    <td style="padding:15px">Update The Billing Settings</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/billing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheBonjourForwardingSettingAndRulesForTheSSID(networkId, number, callback)</td>
    <td style="padding:15px">List The Bonjour Forwarding Setting And Rules For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/bonjourForwarding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheBonjourForwardingSettingAndRulesForTheSSID(networkId, number, body, callback)</td>
    <td style="padding:15px">Update The Bonjour Forwarding Setting And Rules For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/bonjourForwarding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAllIdentityPSKsInAWirelessNetwork(networkId, number, callback)</td>
    <td style="padding:15px">List All Identity PS Ks In A Wireless Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/identityPsks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAnIdentityPSK(networkId, number, body, callback)</td>
    <td style="padding:15px">Create An Identity PSK</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/identityPsks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAnIdentityPSK(networkId, number, identityPskId, callback)</td>
    <td style="padding:15px">Return An Identity PSK</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/identityPsks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnIdentityPSK(networkId, number, identityPskId, body, callback)</td>
    <td style="padding:15px">Update An Identity PSK</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/identityPsks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnIdentityPSK(networkId, number, identityPskId, callback)</td>
    <td style="padding:15px">Delete An Identity PSK</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/identityPsks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheVPNSettingsForTheSSID(networkId, number, callback)</td>
    <td style="padding:15px">List The VPN Settings For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheVPNSettingsForTheSSID(networkId, number, body, callback)</td>
    <td style="padding:15px">Update The VPN Settings For The SSID</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/ssids/{pathv2}/vpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAPchannelutilizationovertimeforadeviceornetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return AP channel utilization over time for a device or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/channelUtilizationHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return wireless client counts over time for a network, device, or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/clientCountHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan(networkId, clientId, callback)</td>
    <td style="padding:15px">List the wireless connectivity events for a client within a network in the timespan.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/clients/{pathv2}/connectivityEvents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return average wireless latency over time for a network, device, or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/latencyHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return PHY data rates over time for a network, device, or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/dataRateHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listwirelessmeshstatusesforrepeaters(networkId, callback)</td>
    <td style="padding:15px">List wireless mesh statuses for repeaters</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/meshStatuses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient(networkId, callback)</td>
    <td style="padding:15px">Return signal quality (SNR/RSSI) over time for a device or network client</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/wireless/signalQualityHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getapplicationhealthbytime(networkId, applicationId, callback)</td>
    <td style="padding:15px">Get application health by time</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/insight/applications/{pathv2}/healthByTime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listallInsighttrackedapplications(organizationId, callback)</td>
    <td style="padding:15px">List all Insight tracked applications</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/insight/applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial(organizationId, callback)</td>
    <td style="padding:15px">Return the latest available reading for each metric from each sensor, sorted by sensor serial</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/sensor/readings/latest?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTrustedAccessConfigs(networkId, callback)</td>
    <td style="padding:15px">List Trusted Access Configs</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/trustedAccessConfigs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUserAccessDevicesAndItsTrustedAccessConnections(networkId, callback)</td>
    <td style="padding:15px">List User Access Devices And Its Trusted Access Connections</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/userAccessDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAUserAccessDevice(networkId, userAccessDeviceId, callback)</td>
    <td style="padding:15px">Delete A User Access Device</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/sm/userAccessDevices/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettheorganizationSAPNScertificate(organizationId, callback)</td>
    <td style="padding:15px">Get the organization's APNS certificate</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/sm/apnsCert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheVPPAccountsInTheOrganization(organizationId, callback)</td>
    <td style="padding:15px">List The VPP Accounts In The Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/sm/vppAccounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID(organizationId, vppAccountId, callback)</td>
    <td style="padding:15px">Get A Hash Containing The Unparsed Token Of The VPP Account With The Given ID</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/sm/vppAccounts/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enqueueAJobToPingATargetHostFromTheDevice(serial, body, callback)</td>
    <td style="padding:15px">Enqueue A Job To Ping A Target Host From The Device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/liveTools/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAPingJob(serial, id, callback)</td>
    <td style="padding:15px">Return A Ping Job</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/liveTools/ping/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enqueueAJobToCheckConnectivityStatusToTheDevice(serial, body, callback)</td>
    <td style="padding:15px">Enqueue A Job To Check Connectivity Status To The Device</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/liveTools/pingDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAPingDeviceJob(serial, id, callback)</td>
    <td style="padding:15px">Return A Ping Device Job</td>
    <td style="padding:15px">{base_path}/{version}/devices/{pathv1}/liveTools/pingDevice/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimavMXintoanetwork(networkId, body, callback)</td>
    <td style="padding:15px">Claim a vMX into a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/devices/claim/vmx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rollbackaFirmwareUpgradeForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Rollback a Firmware Upgrade For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/firmwareUpgrades/rollbacks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirmwareUpgradeInformationForANetwork(networkId, callback)</td>
    <td style="padding:15px">Get Firmware Upgrade Information For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/firmwareUpgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirmwareUpgradeInformationForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Update Firmware Upgrade Information For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/firmwareUpgrades?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnallglobalalertsonthisnetwork(networkId, callback)</td>
    <td style="padding:15px">Return all global alerts on this network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/health/alerts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap(networkId, body, callback)</td>
    <td style="padding:15px">Authorize A User Configured With Meraki Authentication For A Network Currently Supports 802 1 X Spl</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/merakiAuthUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deauthorizeAUser(networkId, merakiAuthUserId, callback)</td>
    <td style="padding:15px">Deauthorize A User</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/merakiAuthUsers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated(networkId, merakiAuthUserId, body, callback)</td>
    <td style="padding:15px">Update A User Configured With Meraki Authentication Currently 802 1 X RADIUS Splash Guest And Clien</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/merakiAuthUsers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAnMQTTBroker(networkId, body, callback)</td>
    <td style="padding:15px">Add An MQTT Broker</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/mqttBrokers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheMQTTBrokersForThisNetwork(networkId, callback)</td>
    <td style="padding:15px">List The MQTT Brokers For This Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/mqttBrokers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnMQTTBroker(networkId, mqttBrokerId, callback)</td>
    <td style="padding:15px">Delete An MQTT Broker</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/mqttBrokers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAnMQTTBroker(networkId, mqttBrokerId, callback)</td>
    <td style="padding:15px">Return An MQTT Broker</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/mqttBrokers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnMQTTBroker(networkId, mqttBrokerId, body, callback)</td>
    <td style="padding:15px">Update An MQTT Broker</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/mqttBrokers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getpoliciesforallclientswithpolicies(networkId, callback)</td>
    <td style="padding:15px">Get policies for all clients with policies</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/policies/byClient?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listofdevicesandconnectionsamongthemwithinthenetwork(networkId, callback)</td>
    <td style="padding:15px">List of devices and connections among them within the network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/topology/linkLayer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createAWebhookPayloadTemplateForANetwork(networkId, body, callback)</td>
    <td style="padding:15px">Create A Webhook Payload Template For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/webhooks/payloadTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheWebhookPayloadTemplatesForANetwork(networkId, callback)</td>
    <td style="padding:15px">List The Webhook Payload Templates For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/webhooks/payloadTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">destroyAWebhookPayloadTemplateForANetwork(networkId, payloadTemplateId, callback)</td>
    <td style="padding:15px">Destroy A Webhook Payload Template For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/webhooks/payloadTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTheWebhookPayloadTemplateForANetwork(networkId, payloadTemplateId, callback)</td>
    <td style="padding:15px">Get The Webhook Payload Template For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/webhooks/payloadTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAWebhookPayloadTemplateForANetwork(networkId, payloadTemplateId, body, callback)</td>
    <td style="padding:15px">Update A Webhook Payload Template For A Network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/webhooks/payloadTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returntheapplicationusagedataforclients(networkId, callback)</td>
    <td style="padding:15px">Return the application usage data for clients</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/applicationUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond(networkId, callback)</td>
    <td style="padding:15px">Returns a timeseries of total traffic consumption rates for all clients on a network within a given</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/bandwidthUsageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return data usage (in megabits per second) over time for all clients in the given organization with</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/clients/bandwidthUsageHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returntheusagehistoriesforclients(networkId, callback)</td>
    <td style="padding:15px">Return the usage histories for clients</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/clients/usageHistories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getthechannelutilizationovereachradioforallAPsinanetwork(networkId, callback)</td>
    <td style="padding:15px">Get the channel utilization over each radio for all APs in a network.</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/networkHealth/channelUtilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsNewAdaptivePolicyACL(organizationId, body, callback)</td>
    <td style="padding:15px">Creates New Adaptive Policy ACL</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/acls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdaptivePolicyACLsInAOrganization(organizationId, callback)</td>
    <td style="padding:15px">List Adaptive Policy AC Ls In A Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/acls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesTheSpecifiedAdaptivePolicyACL(organizationId, aclId, callback)</td>
    <td style="padding:15px">Deletes The Specified Adaptive Policy ACL</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/acls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheAdaptivePolicyACLInformation(organizationId, aclId, callback)</td>
    <td style="padding:15px">Returns The Adaptive Policy ACL Information</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/acls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatesAnAdaptivePolicyACL(organizationId, aclId, body, callback)</td>
    <td style="padding:15px">Updates An Adaptive Policy ACL</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/acls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createsANewAdaptivePolicyGroup(organizationId, body, callback)</td>
    <td style="padding:15px">Creates A New Adaptive Policy Group</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdaptivePolicyGroupsInAOrganization(organizationId, callback)</td>
    <td style="padding:15px">List Adaptive Policy Groups In A Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences(organizationId, id, callback)</td>
    <td style="padding:15px">Deletes The Specified Adaptive Policy Group And Any Associated Policies And References</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsAnAdaptivePolicyGroup(organizationId, id, callback)</td>
    <td style="padding:15px">Returns An Adaptive Policy Group</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatesAnAdaptivePolicyGroup(organizationId, id, body, callback)</td>
    <td style="padding:15px">Updates An Adaptive Policy Group</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/groups/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAnAdaptivePolicy(organizationId, body, callback)</td>
    <td style="padding:15px">Add An Adaptive Policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAdaptivePoliciesInAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">List Adaptive Policies In An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/policies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnAdaptivePolicy(organizationId, id, callback)</td>
    <td style="padding:15px">Delete An Adaptive Policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnAnAdaptivePolicy(organizationId, id, callback)</td>
    <td style="padding:15px">Return An Adaptive Policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnAdaptivePolicy(organizationId, id, body, callback)</td>
    <td style="padding:15px">Update An Adaptive Policy</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/adaptivePolicy/policies/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returntheclientdetailsinanorganization(organizationId, callback)</td>
    <td style="padding:15px">Return the client details in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/clients/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createANewConfigurationTemplate(organizationId, body, callback)</td>
    <td style="padding:15px">Create A New Configuration Template</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnASingleConfigurationTemplate(organizationId, configTemplateId, callback)</td>
    <td style="padding:15px">Return A Single Configuration Template</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAConfigurationTemplate(organizationId, configTemplateId, body, callback)</td>
    <td style="padding:15px">Update A Configuration Template</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/configTemplates/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createANewEarlyAccessFeatureOptInForAnOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Create A New Early Access Feature Opt In For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features/optIns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheEarlyAccessFeatureOptInsForAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">List The Early Access Feature Opt Ins For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features/optIns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAnEarlyAccessFeatureOptIn(organizationId, optInId, callback)</td>
    <td style="padding:15px">Delete An Early Access Feature Opt In</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features/optIns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showAnEarlyAccessFeatureOptInForAnOrganization(organizationId, optInId, callback)</td>
    <td style="padding:15px">Show An Early Access Feature Opt In For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features/optIns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAnEarlyAccessFeatureOptInForAnOrganization(organizationId, optInId, body, callback)</td>
    <td style="padding:15px">Update An Early Access Feature Opt In For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features/optIns/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheAvailableEarlyAccessFeaturesForOrganization(organizationId, callback)</td>
    <td style="padding:15px">List The Available Early Access Features For Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/earlyAccess/features?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory(organizationId, body, callback)</td>
    <td style="padding:15px">Claim A List Of Devices Licenses And Or Orders Into An Organization Inventory</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/inventory/claim?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseAListOfClaimedDevicesFromAnOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Release A List Of Claimed Devices From An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/inventory/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheLoginSecuritySettingsForAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">Returns The Login Security Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/loginSecurity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTheLoginSecuritySettingsForAnOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Update The Login Security Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/loginSecurity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createASAMLIdPForYourOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Create A SAML Id P For Your Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml/idps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listTheSAMLIdPsInYourOrganization(organizationId, callback)</td>
    <td style="padding:15px">List The SAML Id Ps In Your Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml/idps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getASAMLIdPFromYourOrganization(organizationId, idpId, callback)</td>
    <td style="padding:15px">Get A SAML Id P From Your Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml/idps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeASAMLIdPInYourOrganization(organizationId, idpId, callback)</td>
    <td style="padding:15px">Remove A SAML Id P In Your Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml/idps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateASAMLIdPInYourOrganization(organizationId, idpId, body, callback)</td>
    <td style="padding:15px">Update A SAML Id P In Your Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml/idps/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnsTheSAMLSSOEnabledSettingsForAnOrganization(organizationId, callback)</td>
    <td style="padding:15px">Returns The SAML SSO Enabled Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatesTheSAMLSSOEnabledSettingsForAnOrganization(organizationId, body, callback)</td>
    <td style="padding:15px">Updates The SAML SSO Enabled Settings For An Organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/saml?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listtheavailabilityinformationfordevicesinanorganization(organizationId, callback)</td>
    <td style="padding:15px">List the availability information for devices in an organization</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/devices/availabilities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnthetop10appliancessortedbyutilizationovergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return the top 10 appliances sorted by utilization over given time range.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/appliances/byUtilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top 10 clients by data usage (in mb) over given time range.</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/clients/byUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top 10 devices sorted by data usage over given time range</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/devices/byUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top clients by data usage (in mb) over given time range, grouped</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/clients/manufacturers/byUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top 10 device models sorted by data usage over given time range</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/devices/models/byUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top 10 ssids by data usage over given time range</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/ssids/byUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange(organizationId, callback)</td>
    <td style="padding:15px">Return metrics for organization's top 10 switches by energy usage over given time range</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/summary/top/switches/byEnergyUsage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">returnalistofalerttypestobeusedwithmanagingwebhookalerts(organizationId, callback)</td>
    <td style="padding:15px">Return a list of alert types to be used with managing webhook alerts</td>
    <td style="padding:15px">{base_path}/{version}/organizations/{pathv1}/webhooks/alertTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
