/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-meraki',
      type: 'Meraki',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Meraki = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Meraki Adapter Test', () => {
  describe('Meraki Class Tests', () => {
    const a = new Meraki(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createOrganizationActionBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganizationActionBatch('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('123', data.response.id);
                assert.equal('in progress', data.response.status);
                assert.equal(true, data.response.confirmed);
                assert.equal(false, data.response.synchronous);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createOrganizationActionBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationActionBatches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationActionBatches('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationActionBatches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationActionBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationActionBatch('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('123', data.response.id);
                assert.equal('in progress', data.response.status);
                assert.equal(true, data.response.confirmed);
                assert.equal(false, data.response.synchronous);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationActionBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationActionBatch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganizationActionBatch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteOrganizationActionBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationActionBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationActionBatch('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('123', data.response.id);
                assert.equal('in progress', data.response.status);
                assert.equal(true, data.response.confirmed);
                assert.equal(false, data.response.synchronous);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationActionBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationAdmins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationAdmins('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationAdmins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationAdmin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganizationAdmin('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('212406', data.response.id);
                assert.equal('Miles Meraki', data.response.name);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('none', data.response.orgAccess);
                assert.equal('ok', data.response.accountStatus);
                assert.equal(false, data.response.twoFactorAuthEnabled);
                assert.equal(true, data.response.hasApiKey);
                assert.equal('2019-01-28 14:58:56 -0800', data.response.lastActive);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(true, Array.isArray(data.response.networks));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createOrganizationAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationAdmin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationAdmin('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('212406', data.response.id);
                assert.equal('Miles Meraki', data.response.name);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('none', data.response.orgAccess);
                assert.equal('ok', data.response.accountStatus);
                assert.equal(false, data.response.twoFactorAuthEnabled);
                assert.equal(true, data.response.hasApiKey);
                assert.equal('2019-01-28 14:58:56 -0800', data.response.lastActive);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(true, Array.isArray(data.response.networks));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationAdmin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganizationAdmin('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteOrganizationAdmin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAlertSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkAlertSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.defaultDestinations);
                assert.equal(true, Array.isArray(data.response.alerts));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkAlertSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkAlertSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkAlertSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.defaultDestinations);
                assert.equal(true, Array.isArray(data.response.alerts));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkAlertSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsOverview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsOverview('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceCameraAnalyticsOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsOverviewV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsOverviewV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MV Sense', 'getDeviceCameraAnalyticsOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsZones('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MV Sense', 'getDeviceCameraAnalyticsZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZoneHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceCameraAnalyticsZoneHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZoneHistoryV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistoryV2('fakedata', 'fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MV Sense', 'getDeviceCameraAnalyticsZoneHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsRecent - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsRecent('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MV Sense', 'getDeviceCameraAnalyticsRecent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsRecentV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsRecentV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('MV Sense', 'getDeviceCameraAnalyticsRecent', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsLive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceCameraAnalyticsLive('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('2018-08-15T18:32:38.123Z', data.response.ts);
                assert.equal('object', typeof data.response.zones);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceCameraAnalyticsLive', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationApiRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationApiRequests('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('API usage', 'getOrganizationApiRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationApiRequestsV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationApiRequestsV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationApiRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkBluetoothClient('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('N_24329156', data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkBluetoothClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkBluetoothClients('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkBluetoothClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClientsV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkBluetoothClientsV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkBluetoothClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkBluetoothSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.scanningEnabled);
                assert.equal(true, data.response.advertisingEnabled);
                assert.equal('00000000-0000-0000-000-000000000000', data.response.uuid);
                assert.equal('Non-unique', data.response.majorMinorAssignmentMode);
                assert.equal(1, data.response.major);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkBluetoothSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkBluetoothSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkBluetoothSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.scanningEnabled);
                assert.equal(true, data.response.advertisingEnabled);
                assert.equal('00000000-0000-0000-000-000000000000', data.response.uuid);
                assert.equal('Non-unique', data.response.majorMinorAssignmentMode);
                assert.equal(1, data.response.major);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkBluetoothSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationNetworks('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganizationNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('L_123456', data.response.id);
                assert.equal(2930418, data.response.organizationId);
                assert.equal('Long Island Office', data.response.name);
                assert.equal('America/Los_Angeles', data.response.timeZone);
                assert.equal(' tag1 tag2 ', data.response.tags);
                assert.equal('combined', data.response.type);
                assert.equal(false, data.response.disableMyMerakiCom);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createOrganizationNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetwork('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('L_123456', data.response.id);
                assert.equal(2930418, data.response.organizationId);
                assert.equal('Long Island Office', data.response.name);
                assert.equal('America/Los_Angeles', data.response.timeZone);
                assert.equal(' tag1 tag2 ', data.response.tags);
                assert.equal('combined', data.response.type);
                assert.equal(false, data.response.disableMyMerakiCom);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('L_123456', data.response.id);
                assert.equal(2930418, data.response.organizationId);
                assert.equal('Long Island Office', data.response.name);
                assert.equal('America/Los_Angeles', data.response.timeZone);
                assert.equal(' tag1 tag2 ', data.response.tags);
                assert.equal('combined', data.response.type);
                assert.equal(false, data.response.disableMyMerakiCom);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bindNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bindNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'bindNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unbindNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unbindNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'unbindNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTraffic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTraffic('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkTraffic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTrafficV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkTraffic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAccessPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkAccessPolicies('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkAccessPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAirMarshal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkAirMarshal('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkAirMarshal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#combineOrganizationNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.combineOrganizationNetworks('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.resultingNetwork);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'combineOrganizationNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#splitNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.splitNetwork('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.resultingNetworks));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'splitNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSiteToSiteVpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSiteToSiteVpn('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('spoke', data.response.mode);
                assert.equal(true, Array.isArray(data.response.hubs));
                assert.equal(true, Array.isArray(data.response.subnets));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSiteToSiteVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSiteToSiteVpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSiteToSiteVpn('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('spoke', data.response.mode);
                assert.equal(true, Array.isArray(data.response.hubs));
                assert.equal(true, Array.isArray(data.response.subnets));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSiteToSiteVpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraVideoLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkCameraVideoLink('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('https://nxx.meraki.com/office-cameras/n/bs0a1k/manage/nodes/new_list/29048243992402?timestamp=1535732570077', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkCameraVideoLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateNetworkCameraSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.generateNetworkCameraSnapshot('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('https://spn4.meraki.com/stream/jpeg/snapshot/b2d123asdf423qd22d2', data.response.url);
                assert.equal('Access to the image will expire at 2018-12-11T03:12:39Z.', data.response.expiry);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'generateNetworkCameraSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClient('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('k74272e', data.response.id);
                assert.equal('Miles\'s phone', data.response.description);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('1.2.3.4', data.response.ip);
                assert.equal('null', data.response.user);
                assert.equal('255', data.response.vlan);
                assert.equal('object', typeof data.response.switchport);
                assert.equal('', data.response.ip6);
                assert.equal(1518365681, data.response.firstSeen);
                assert.equal(1526087474, data.response.lastSeen);
                assert.equal('Apple', data.response.manufacturer);
                assert.equal('iOS', data.response.os);
                assert.equal('My SSID', data.response.ssid);
                assert.equal('802.11ac - 2.4 and 5 GHz', data.response.wirelessCapabilities);
                assert.equal(true, data.response.smInstalled);
                assert.equal('00:11:22:33:44:55', data.response.recentDeviceMac);
                assert.equal(true, Array.isArray(data.response.clientVpnConnections));
                assert.equal(true, Array.isArray(data.response.lldp));
                assert.equal('object', typeof data.response.cdp);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionNetworkClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.provisionNetworkClients('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('k74272e', data.response.clientId);
                assert.equal('Miles\'s phone', data.response.name);
                assert.equal('Group policy', data.response.devicePolicy);
                assert.equal('101', data.response.groupPolicyId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'provisionNetworkClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientUsageHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientUsageHistory('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientUsageHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientPolicy('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('Group policy', data.response.type);
                assert.equal('101', data.response.groupPolicyId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkClientPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkClientPolicy('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('Group policy', data.response.type);
                assert.equal('101', data.response.groupPolicyId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkClientPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientSplashAuthorizationStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientSplashAuthorizationStatus('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.ssids);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientSplashAuthorizationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkClientSplashAuthorizationStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkClientSplashAuthorizationStatus('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.ssids);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkClientSplashAuthorizationStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClients('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.usage);
                assert.equal('k74272e', data.response.id);
                assert.equal('Miles\'s phone', data.response.description);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('1.2.3.4', data.response.ip);
                assert.equal('milesmeraki', data.response.user);
                assert.equal(255, data.response.vlan);
                assert.equal('object', typeof data.response.switchport);
                assert.equal('', data.response.ip6);
                assert.equal(1518365681, data.response.firstSeen);
                assert.equal(1526087474, data.response.lastSeen);
                assert.equal('Apple', data.response.manufacturer);
                assert.equal('iOS', data.response.os);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceClients('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.usage);
                assert.equal('k74272e', data.response.id);
                assert.equal('Miles\'s phone', data.response.description);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('1.2.3.4', data.response.ip);
                assert.equal('milesmeraki', data.response.user);
                assert.equal(255, data.response.vlan);
                assert.equal('object', typeof data.response.switchport);
                assert.equal('Miles\'s phone', data.response.mdnsName);
                assert.equal('MilesPhone', data.response.dhcpHostname);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientTrafficHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientTrafficHistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientTrafficHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientEvents('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientLatencyHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientLatencyHistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientLatencyHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationConfigTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationConfigTemplates('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationConfigTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationConfigTemplate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganizationConfigTemplate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteOrganizationConfigTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLldpCdp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLldpCdp('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.sourceMac);
                assert.equal('object', typeof data.response.ports);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLldpCdp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLldpCdpV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLldpCdpV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:11:22:33:44:55', data.response.sourceMac);
                assert.equal('object', typeof data.response.ports);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLldpCdpV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationDevices('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My AP', data.response.name);
                assert.equal(37.4180951010362, data.response.lat);
                assert.equal(-122.098531723022, data.response.lng);
                assert.equal('1600 Pennsylvania Ave', data.response.address);
                assert.equal('My AP\'s note', data.response.notes);
                assert.equal(' recently-added ', data.response.tags);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('MR34', data.response.model);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('1.2.3.4', data.response.lanIp);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevices('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevice('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My AP', data.response.name);
                assert.equal(37.4180951010362, data.response.lat);
                assert.equal(-122.098531723022, data.response.lng);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('MR34', data.response.model);
                assert.equal('1600 Pennsylvania Ave', data.response.address);
                assert.equal('My AP\'s note', data.response.notes);
                assert.equal('1.2.3.4', data.response.lanIp);
                assert.equal(' recently-added ', data.response.tags);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('object', typeof data.response.beaconIdParams);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceV1('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My AP', data.response.name);
                assert.equal(37.4180951010362, data.response.lat);
                assert.equal(-122.098531723022, data.response.lng);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('MR34', data.response.model);
                assert.equal('1600 Pennsylvania Ave', data.response.address);
                assert.equal('My AP\'s note', data.response.notes);
                assert.equal('1.2.3.4', data.response.lanIp);
                assert.equal(' recently-added ', data.response.tags);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('object', typeof data.response.beaconIdParams);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkDevice('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My AP', data.response.name);
                assert.equal(37.4180951010362, data.response.lat);
                assert.equal(-122.098531723022, data.response.lng);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('MR34', data.response.model);
                assert.equal('1600 Pennsylvania Ave', data.response.address);
                assert.equal('My AP\'s note', data.response.notes);
                assert.equal('1.2.3.4', data.response.lanIp);
                assert.equal(' recently-added ', data.response.tags);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('object', typeof data.response.beaconIdParams);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicePerformance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevicePerformance('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.perfScore);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevicePerformance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicePerformanceV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevicePerformanceV1('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.perfScore);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevicePerformanceV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceUplink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceUplink('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceUplink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimNetworkDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.claimNetworkDevices('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'claimNetworkDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNetworkDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeNetworkDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'removeNetworkDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLossAndLatencyHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLossAndLatencyHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLossAndLatencyHistoryV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistoryV1('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLossAndLatencyHistoryV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootNetworkDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rebootNetworkDevice('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'rebootNetworkDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootNetworkDeviceV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rebootNetworkDeviceV1('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'rebootNetworkDeviceV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blinkNetworkDeviceLeds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.blinkNetworkDeviceLeds('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.sentToDevice);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'blinkNetworkDeviceLeds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blinkNetworkDeviceLedsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.blinkNetworkDeviceLedsV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.sentToDevice);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'blinkNetworkDeviceLedsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularFirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkCellularFirewallRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkCellularFirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularFirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkCellularFirewallRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkCellularFirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL3FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkL3FirewallRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkL3FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkL3FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkL3FirewallRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkL3FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL7FirewallRulesApplicationCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkL7FirewallRulesApplicationCategories('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkL7FirewallRulesApplicationCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL7FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkL7FirewallRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkL7FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkL7FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkL7FirewallRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkL7FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationVpnFirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationVpnFirewallRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationVpnFirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationVpnFirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationVpnFirewallRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationVpnFirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidL3FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsidL3FirewallRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsidL3FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidL3FirewallRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSsidL3FirewallRules('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSsidL3FirewallRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkGroupPolicies('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkGroupPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkGroupPolicy('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('No video streaming', data.response.name);
                assert.equal('101', data.response.groupPolicyId);
                assert.equal('object', typeof data.response.scheduling);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.firewallAndTrafficShaping);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkGroupPolicy('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('No video streaming', data.response.name);
                assert.equal('101', data.response.groupPolicyId);
                assert.equal('object', typeof data.response.scheduling);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.firewallAndTrafficShaping);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkGroupPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkGroupPolicy('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('No video streaming', data.response.name);
                assert.equal('101', data.response.groupPolicyId);
                assert.equal('object', typeof data.response.scheduling);
                assert.equal('object', typeof data.response.bandwidth);
                assert.equal('object', typeof data.response.firewallAndTrafficShaping);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkGroupPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkGroupPolicy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkGroupPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidHotspot20 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsidHotspot20('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.enabled);
                assert.equal('Meraki Product Management', data.response.operatorName);
                assert.equal('SF Branch', data.response.venueName);
                assert.equal('Unspecified Assembly', data.response.venueType);
                assert.equal('Private network', data.response.networkType);
                assert.equal(true, Array.isArray(data.response.domainList));
                assert.equal(true, Array.isArray(data.response.roamConsortOis));
                assert.equal(true, Array.isArray(data.response.mccMncs));
                assert.equal(true, Array.isArray(data.response.naiRealms));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsidHotspot20', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidHotspot20 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSsidHotspot20('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.enabled);
                assert.equal('Meraki Product Management', data.response.operatorName);
                assert.equal('SF Branch', data.response.venueName);
                assert.equal('Unspecified Assembly', data.response.venueType);
                assert.equal('Private network', data.response.networkType);
                assert.equal(true, Array.isArray(data.response.domainList));
                assert.equal(true, Array.isArray(data.response.roamConsortOis));
                assert.equal(true, Array.isArray(data.response.mccMncs));
                assert.equal(true, Array.isArray(data.response.naiRealms));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSsidHotspot20', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkHttpServers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkHttpServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkHttpServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkHttpServer('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('ABC123', data.response.id);
                assert.equal('N_123', data.response.networkId);
                assert.equal('My HTTP server', data.response.name);
                assert.equal('https://www.example.com/webhooks', data.response.url);
                assert.equal('foobar', data.response.sharedSecret);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkHttpServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkHttpServer('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('ABC123', data.response.id);
                assert.equal('N_123', data.response.networkId);
                assert.equal('My HTTP server', data.response.name);
                assert.equal('https://www.example.com/webhooks', data.response.url);
                assert.equal('foobar', data.response.sharedSecret);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkHttpServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkHttpServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkHttpServer('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('ABC123', data.response.id);
                assert.equal('N_123', data.response.networkId);
                assert.equal('My HTTP server', data.response.name);
                assert.equal('https://www.example.com/webhooks', data.response.url);
                assert.equal('foobar', data.response.sharedSecret);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkHttpServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkHttpServer - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkHttpServer('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkHttpServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkHttpServersWebhookTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkHttpServersWebhookTest('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('https://www.example.com/path', data.response.url);
                assert.equal('enqueued', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkHttpServersWebhookTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServersWebhookTest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkHttpServersWebhookTest('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('https://www.example.com/path', data.response.url);
                assert.equal('delivered', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkHttpServersWebhookTest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceManagementInterfaceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceManagementInterfaceSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.wan1);
                assert.equal('object', typeof data.response.wan2);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceManagementInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceManagementInterfaceSettingsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceManagementInterfaceSettingsV1('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.wan1);
                assert.equal('object', typeof data.response.wan2);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceManagementInterfaceSettingsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceManagementInterfaceSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkDeviceManagementInterfaceSettings('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.wan1);
                assert.equal('object', typeof data.response.wan2);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkDeviceManagementInterfaceSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceManagementInterfaceSettingsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkDeviceManagementInterfaceSettingsV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.wan1);
                assert.equal('object', typeof data.response.wan2);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkDeviceManagementInterfaceSettingsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkMerakiAuthUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkMerakiAuthUsers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkMerakiAuthUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkMerakiAuthUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkMerakiAuthUser('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('aGlAaGkuY29t', data.response.id);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('Miles Meraki', data.response.name);
                assert.equal(1518365681, data.response.createdAt);
                assert.equal('Guest', data.response.accountType);
                assert.equal(true, Array.isArray(data.response.authorizations));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkMerakiAuthUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationOpenapiSpec - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationOpenapiSpec('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('2.0', data.response.swagger);
                assert.equal('object', typeof data.response.info);
                assert.equal('api.meraki.com', data.response.host);
                assert.equal('/api/v0', data.response.basePath);
                assert.equal(true, Array.isArray(data.response.schemes));
                assert.equal(true, Array.isArray(data.response.consumes));
                assert.equal(true, Array.isArray(data.response.produces));
                assert.equal('object', typeof data.response.securityDefinitions);
                assert.equal(true, Array.isArray(data.response.security));
                assert.equal('object', typeof data.response.paths);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationOpenapiSpec', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizations((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganization('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2930418, data.response.id);
                assert.equal('My organization', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganization('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2930418, data.response.id);
                assert.equal('My organization', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2930418, data.response.id);
                assert.equal('My organization', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloneOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2930418, data.response.id);
                assert.equal('My organization', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'cloneOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicenseState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationLicenseState('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('OK', data.response.status);
                assert.equal('Feb 8, 2020 UTC', data.response.expirationDate);
                assert.equal('object', typeof data.response.licensedDeviceCounts);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationLicenseState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationInventory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationInventory('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationInventory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationInventoryV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationInventoryV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Organizations', 'getOrganizationInventory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDeviceStatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationDeviceStatuses('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationDeviceStatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSnmp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationSnmp('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.v2cEnabled);
                assert.equal(false, data.response.v3Enabled);
                assert.equal('SHA', data.response.v3AuthMode);
                assert.equal('AES128', data.response.v3PrivMode);
                assert.equal('123.123.123.1', data.response.peerIps);
                assert.equal('n1.meraki.com', data.response.hostname);
                assert.equal(443, data.response.port);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationSnmp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSnmp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationSnmp('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.v2cEnabled);
                assert.equal(false, data.response.v3Enabled);
                assert.equal('SHA', data.response.v3AuthMode);
                assert.equal('AES128', data.response.v3PrivMode);
                assert.equal('123.123.123.1', data.response.peerIps);
                assert.equal('n1.meraki.com', data.response.hostname);
                assert.equal(443, data.response.port);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationSnmp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.claimOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'claimOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationUplinksLossAndLatency - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationUplinksLossAndLatency('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationUplinksLossAndLatency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationUplinksLossAndLatencyV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationUplinksLossAndLatencyV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Organizations', 'getOrganizationUplinksLossAndLatency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationThirdPartyVPNPeers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationThirdPartyVPNPeers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationThirdPartyVPNPeers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationThirdPartyVPNPeers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationThirdPartyVPNPeers('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationThirdPartyVPNPeers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiPiiKeys - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPiiPiiKeys('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.N_1234);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPiiPiiKeys', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiSmDevicesForKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPiiSmDevicesForKey('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.N_1234));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPiiSmDevicesForKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiSmOwnersForKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPiiSmOwnersForKey('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.N_1234));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPiiSmOwnersForKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPiiRequests('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPiiRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkPiiRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkPiiRequest('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal(false, data.response.organizationWide);
                assert.equal('N_1234', data.response.networkId);
                assert.equal('delete', data.response.type);
                assert.equal('00:77:00:77:00:77', data.response.mac);
                assert.equal('[\'usage\', \'events\']', data.response.datasets);
                assert.equal('Not visible in Dashboard; database deletion in process', data.response.status);
                assert.equal(1524692227, data.response.createdAt);
                assert.equal('object', typeof data.response.completedAt);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkPiiRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPiiRequest('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal(false, data.response.organizationWide);
                assert.equal('N_1234', data.response.networkId);
                assert.equal('delete', data.response.type);
                assert.equal('00:77:00:77:00:77', data.response.mac);
                assert.equal('[\'usage\', \'events\']', data.response.datasets);
                assert.equal('Completed', data.response.status);
                assert.equal(1524692227, data.response.createdAt);
                assert.equal(1524702227, data.response.completedAt);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPiiRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkPiiRequest - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkPiiRequest('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkPiiRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceWirelessRadioSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceWirelessRadioSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('1234', data.response.rfProfileId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceWirelessRadioSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceWirelessRadioSettingsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceWirelessRadioSettingsV1('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('1234', data.response.rfProfileId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceWirelessRadioSettingsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceWirelessRadioSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkDeviceWirelessRadioSettings('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('1234', data.response.rfProfileId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkDeviceWirelessRadioSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceWirelessRadioSettingsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkDeviceWirelessRadioSettingsV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('1234', data.response.rfProfileId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkDeviceWirelessRadioSettingsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWirelessRfProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkWirelessRfProfiles('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkWirelessRfProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSamlRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationSamlRoles('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationSamlRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationSamlRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganizationSamlRole('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('TEdJIEN1c3RvbWVy', data.response.id);
                assert.equal('myrole', data.response.role);
                assert.equal('none', data.response.orgAccess);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createOrganizationSamlRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSamlRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationSamlRole('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('TEdJIEN1c3RvbWVy', data.response.id);
                assert.equal('myrole', data.response.role);
                assert.equal('none', data.response.orgAccess);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationSamlRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSamlRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationSamlRole('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('TEdJIEN1c3RvbWVy', data.response.id);
                assert.equal('myrole', data.response.role);
                assert.equal('none', data.response.orgAccess);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationSamlRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationSamlRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganizationSamlRole('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteOrganizationSamlRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientSecurityEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientSecurityEvents('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientSecurityEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSecurityEvents('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSecurityEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSecurityEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationSecurityEvents('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationSecurityEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSecurityIntrusionSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('prevention', data.response.mode);
                assert.equal('balanced', data.response.idsRulesets);
                assert.equal('object', typeof data.response.protectedNetworks);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSecurityIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSecurityIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSecurityIntrusionSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('prevention', data.response.mode);
                assert.equal('balanced', data.response.idsRulesets);
                assert.equal('object', typeof data.response.protectedNetworks);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSecurityIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSecurityIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationSecurityIntrusionSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.whitelistedRules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationSecurityIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSecurityIntrusionSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationSecurityIntrusionSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.whitelistedRules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateOrganizationSecurityIntrusionSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityMalwareSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSecurityMalwareSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('enabled', data.response.mode);
                assert.equal(true, Array.isArray(data.response.allowedUrls));
                assert.equal(true, Array.isArray(data.response.allowedFiles));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSecurityMalwareSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSecurityMalwareSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSecurityMalwareSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('enabled', data.response.mode);
                assert.equal(true, Array.isArray(data.response.allowedUrls));
                assert.equal(true, Array.isArray(data.response.allowedFiles));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSecurityMalwareSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmTargetGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmTargetGroups('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmTargetGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmTargetGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkSmTargetGroup('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My target group', data.response.name);
                assert.equal('none', data.response.scope);
                assert.equal('[]', data.response.tags);
                assert.equal('devices', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSmTargetGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmTargetGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmTargetGroup('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My target group', data.response.name);
                assert.equal('none', data.response.scope);
                assert.equal('[]', data.response.tags);
                assert.equal('devices', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmTargetGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmTargetGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSmTargetGroup('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My target group', data.response.name);
                assert.equal('none', data.response.scope);
                assert.equal('[]', data.response.tags);
                assert.equal('devices', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmTargetGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmTargetGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSmTargetGroup('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSmTargetGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileClarity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkSmProfileClarity('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSmProfileClarity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileClarity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSmProfileClarity('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmProfileClarity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSmProfileClarity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNetworkSmProfileClarity('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'addNetworkSmProfileClarity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfileClarity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSmProfileClarity('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmProfileClarity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfileClarity - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSmProfileClarity('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSmProfileClarity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileUmbrella - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkSmProfileUmbrella('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSmProfileUmbrella', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileUmbrella - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSmProfileUmbrella('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmProfileUmbrella', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSmProfileUmbrella - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNetworkSmProfileUmbrella('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'addNetworkSmProfileUmbrella', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfileUmbrella - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSmProfileUmbrella('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmProfileUmbrella', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfileUmbrella - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSmProfileUmbrella('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSmProfileUmbrella', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmAppPolaris - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkSmAppPolaris('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSmAppPolaris', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmAppPolaris - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSmAppPolaris('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmAppPolaris', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmAppPolaris - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSmAppPolaris('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmAppPolaris', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmAppPolaris - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSmAppPolaris('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSmAppPolaris', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmDevices('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDevicesV2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmDevicesV2('fakedata', null, (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmUsers('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('Miles Meraki', data.response.fullName);
                assert.equal('', data.response.username);
                assert.equal(false, data.response.hasPassword);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(true, Array.isArray(data.response.adGroups));
                assert.equal(true, Array.isArray(data.response.asmGroups));
                assert.equal(false, data.response.isExternal);
                assert.equal('Miles Meraki <miles@meraki.com>', data.response.displayName);
                assert.equal(false, data.response.hasIdentityCertificate);
                assert.equal('https://s3.amazonaws.com/image.extension', data.response.userThumbnail);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUserDeviceProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmUserDeviceProfiles('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmUserDeviceProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDeviceProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmDeviceProfiles('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmDeviceProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUserSoftwares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmUserSoftwares('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmUserSoftwares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmSoftwares - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmSoftwares('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmSoftwares', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmNetworkAdapters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmNetworkAdapters('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmNetworkAdapters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmWlanLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmWlanLists('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmWlanLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmSecurityCenters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmSecurityCenters('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmSecurityCenters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmRestrictions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmRestrictions('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmRestrictions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmCerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmCerts('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmCerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDevicesTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSmDevicesTags('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.success));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmDevicesTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDevicesTagsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSmDevicesTagsV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.success));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmDevicesTagsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDeviceFields - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSmDeviceFields('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.success));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmDeviceFields', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lockNetworkSmDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.lockNetworkSmDevices('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'lockNetworkSmDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wipeNetworkSmDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wipeNetworkSmDevice('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'wipeNetworkSmDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wipeNetworkSmDeviceV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wipeNetworkSmDeviceV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'wipeNetworkSmDeviceV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkinNetworkSmDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.checkinNetworkSmDevices('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'checkinNetworkSmDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkinNetworkSmDevicesV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.checkinNetworkSmDevicesV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'checkinNetworkSmDevicesV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveNetworkSmDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.moveNetworkSmDevices('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'moveNetworkSmDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveNetworkSmDevicesV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.moveNetworkSmDevicesV1('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'moveNetworkSmDevicesV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unenrollNetworkSmDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unenrollNetworkSmDevice('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'unenrollNetworkSmDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileReinstall - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSmProfileReinstall('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSmProfileReinstall', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmProfiles('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.profiles));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNetworkSmProfile('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSmProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkSmProfileCreate('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSmProfileCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmCellularUsageHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmCellularUsageHistory('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(138, data.response.sent);
                assert.equal(61, data.response.recv);
                assert.equal(1526087474, data.response.ts);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmCellularUsageHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmPerformanceHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmPerformanceHistory('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmPerformanceHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDesktopLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmDesktopLogs('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmDesktopLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDeviceCommandLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmDeviceCommandLogs('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('UpdateProfile', data.response.action);
                assert.equal('My profile', data.response.name);
                assert.equal('{}', data.response.details);
                assert.equal('Miles Meraki', data.response.dashboardUser);
                assert.equal(1526087474, data.response.ts);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmDeviceCommandLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmConnectivity - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSmConnectivity('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1518365681, data.response.firstSeenAt);
                assert.equal(1526087474, data.response.lastSeenAt);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSmConnectivity', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesDeviceId = 'fakedata';
    describe('#refreshTheDetailsOfADevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.refreshTheDetailsOfADevice('fakedata', devicesDeviceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SM', 'refreshTheDetailsOfADevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheSNMPSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheSNMPSettingsForANetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snmp', 'updateTheSNMPSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSNMPSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSNMPSettingsForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('users', data.response.access);
                assert.equal(true, Array.isArray(data.response.users));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snmp', 'returnTheSNMPSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snmpOrganizationId = 'fakedata';
    const snmpUpdateTheSNMPSettingsForAnOrganizationBodyParam = {
      v2cEnabled: '<boolean>',
      v3Enabled: '<boolean>',
      v3AuthMode: '<string>',
      v3AuthPass: '<string>',
      v3PrivMode: '<string>',
      v3PrivPass: '<string>',
      peerIps: [
        '<string>',
        '<string>'
      ]
    };
    describe('#updateTheSNMPSettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheSNMPSettingsForAnOrganization(snmpOrganizationId, snmpUpdateTheSNMPSettingsForAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snmp', 'updateTheSNMPSettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSNMPSettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSNMPSettingsForAnOrganization(snmpOrganizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.v2cEnabled);
                assert.equal(true, data.response.v3Enabled);
                assert.equal('SHA', data.response.v3AuthMode);
                assert.equal('AES128', data.response.v3PrivMode);
                assert.equal(true, Array.isArray(data.response.peerIps));
                assert.equal('snmp.meraki.com', data.response.hostname);
                assert.equal(443, data.response.port);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snmp', 'returnTheSNMPSettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const syslogServersNetworkId = 'fakedata';
    const syslogServersUpdateTheSyslogServersForANetworkBodyParam = {
      servers: [
        {
          host: '<string>',
          port: '<integer>',
          roles: [
            '<string>',
            '<string>'
          ]
        },
        {
          host: '<string>',
          port: '<integer>',
          roles: [
            '<string>',
            '<string>'
          ]
        }
      ]
    };

    describe('#returnTheSSIDStatusesOfAnAccessPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSSIDStatusesOfAnAccessPoint('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.basicServiceSets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'returnTheSSIDStatusesOfAnAccessPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSSIDStatusesOfAnAccessPointV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSSIDStatusesOfAnAccessPointV1('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.basicServiceSets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SSID', 'returnTheSSIDStatusesOfAnAccessPointV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accessPoliciesNetworkId = 'fakedata';
    const accessPoliciesCreateAnAccessPolicyForASwitchNetworkBodyParam = {
      name: '<string>',
      radiusServers: [
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        },
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        }
      ],
      radiusTestingEnabled: '<boolean>',
      radiusCoaSupportEnabled: '<boolean>',
      radiusAccountingEnabled: '<boolean>',
      hostMode: '<string>',
      urlRedirectWalledGardenEnabled: '<boolean>',
      radius: {
        criticalAuth: {
          dataVlanId: '<integer>',
          voiceVlanId: '<integer>',
          suspendPortBounce: '<boolean>'
        },
        failedAuthVlanId: '<integer>',
        reAuthenticationInterval: '<integer>'
      },
      radiusAccountingServers: [
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        },
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        }
      ],
      radiusGroupAttribute: '<string>',
      accessPolicyType: '<string>',
      increaseAccessSpeed: '<boolean>',
      guestVlanId: '<integer>',
      dot1x: {
        controlDirection: '<string>'
      },
      voiceVlanClients: '<boolean>',
      urlRedirectWalledGardenRanges: [
        '<string>',
        '<string>'
      ]
    };
    describe('#createAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAnAccessPolicyForASwitchNetwork(accessPoliciesNetworkId, accessPoliciesCreateAnAccessPolicyForASwitchNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Access policy #1', data.response.name);
                assert.equal(true, Array.isArray(data.response.radiusServers));
                assert.equal('object', typeof data.response.radius);
                assert.equal(false, data.response.radiusTestingEnabled);
                assert.equal(false, data.response.radiusCoaSupportEnabled);
                assert.equal(true, data.response.radiusAccountingEnabled);
                assert.equal(true, Array.isArray(data.response.radiusAccountingServers));
                assert.equal('11', data.response.radiusGroupAttribute);
                assert.equal('Single-Host', data.response.hostMode);
                assert.equal('Hybrid authentication', data.response.accessPolicyType);
                assert.equal(false, data.response.increaseAccessSpeed);
                assert.equal(100, data.response.guestVlanId);
                assert.equal('object', typeof data.response.dot1x);
                assert.equal(true, data.response.voiceVlanClients);
                assert.equal(true, data.response.urlRedirectWalledGardenEnabled);
                assert.equal(true, Array.isArray(data.response.urlRedirectWalledGardenRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicies', 'createAnAccessPolicyForASwitchNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheAccessPoliciesForASwitchNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheAccessPoliciesForASwitchNetwork(accessPoliciesNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicies', 'listTheAccessPoliciesForASwitchNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const accessPoliciesAccessPolicyNumber = 'fakedata';
    const accessPoliciesUpdateAnAccessPolicyForASwitchNetworkBodyParam = {
      name: '<string>',
      radiusServers: [
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        },
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        }
      ],
      radius: {
        criticalAuth: {
          dataVlanId: '<integer>',
          voiceVlanId: '<integer>',
          suspendPortBounce: '<boolean>'
        },
        failedAuthVlanId: '<integer>',
        reAuthenticationInterval: '<integer>'
      },
      radiusTestingEnabled: '<boolean>',
      radiusCoaSupportEnabled: '<boolean>',
      radiusAccountingEnabled: '<boolean>',
      radiusAccountingServers: [
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        },
        {
          host: '<string>',
          port: '<integer>',
          secret: samProps.authentication.password
        }
      ],
      radiusGroupAttribute: '<string>',
      hostMode: '<string>',
      accessPolicyType: '<string>',
      increaseAccessSpeed: '<boolean>',
      guestVlanId: '<integer>',
      dot1x: {
        controlDirection: '<string>'
      },
      voiceVlanClients: '<boolean>',
      urlRedirectWalledGardenEnabled: '<boolean>',
      urlRedirectWalledGardenRanges: [
        '<string>',
        '<string>'
      ]
    };
    describe('#updateAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnAccessPolicyForASwitchNetwork(accessPoliciesNetworkId, accessPoliciesAccessPolicyNumber, accessPoliciesUpdateAnAccessPolicyForASwitchNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicies', 'updateAnAccessPolicyForASwitchNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASpecificAccessPolicyForASwitchNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnASpecificAccessPolicyForASwitchNetwork(accessPoliciesNetworkId, accessPoliciesAccessPolicyNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Access policy #1', data.response.name);
                assert.equal(true, Array.isArray(data.response.radiusServers));
                assert.equal('object', typeof data.response.radius);
                assert.equal(false, data.response.radiusTestingEnabled);
                assert.equal(false, data.response.radiusCoaSupportEnabled);
                assert.equal(true, data.response.radiusAccountingEnabled);
                assert.equal(true, Array.isArray(data.response.radiusAccountingServers));
                assert.equal('11', data.response.radiusGroupAttribute);
                assert.equal('Single-Host', data.response.hostMode);
                assert.equal('Hybrid authentication', data.response.accessPolicyType);
                assert.equal(false, data.response.increaseAccessSpeed);
                assert.equal(100, data.response.guestVlanId);
                assert.equal('object', typeof data.response.dot1x);
                assert.equal(true, data.response.voiceVlanClients);
                assert.equal(true, data.response.urlRedirectWalledGardenEnabled);
                assert.equal(true, Array.isArray(data.response.urlRedirectWalledGardenRanges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicies', 'returnASpecificAccessPolicyForASwitchNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnAccessPolicyForASwitchNetwork(accessPoliciesNetworkId, accessPoliciesAccessPolicyNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AccessPolicies', 'deleteAnAccessPolicyForASwitchNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusesSerial = 'fakedata';
    describe('#returnTheStatusForAllThePortsOfASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheStatusForAllThePortsOfASwitch(statusesSerial, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch ports', 'returnTheStatusForAllThePortsOfASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const packetsSerial = 'fakedata';
    describe('#returnThePacketCountersForAllThePortsOfASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnThePacketCountersForAllThePortsOfASwitch(packetsSerial, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch ports', 'returnThePacketCountersForAllThePortsOfASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkBandwidthNetworkId = 'fakedata';
    const uplinkBandwidthUpdatesTheUplinkBandwidthSettingsForYourMXNetworkBodyParam = {
      bandwidthLimits: {
        wan1: {
          limitUp: '<integer>',
          limitDown: '<integer>'
        },
        wan2: {
          limitUp: '<integer>',
          limitDown: '<integer>'
        },
        cellular: {
          limitUp: '<integer>',
          limitDown: '<integer>'
        }
      }
    };
    describe('#updatesTheUplinkBandwidthSettingsForYourMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesTheUplinkBandwidthSettingsForYourMXNetwork(uplinkBandwidthNetworkId, uplinkBandwidthUpdatesTheUplinkBandwidthSettingsForYourMXNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UplinkBandwidth', 'updatesTheUplinkBandwidthSettingsForYourMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheUplinkBandwidthSettingsForYourMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsTheUplinkBandwidthSettingsForYourMXNetwork(uplinkBandwidthNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.bandwidthLimits);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UplinkBandwidth', 'returnsTheUplinkBandwidthSettingsForYourMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSplashLoginAttempts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSplashLoginAttempts('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSplashLoginAttempts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidSplashSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsidSplashSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(0, data.response.ssidNumber);
                assert.equal('Click-through splash page', data.response.splashPage);
                assert.equal('https://www.custom_splash_url.com', data.response.splashUrl);
                assert.equal(true, data.response.useSplashUrl);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsidSplashSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidSplashSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSsidSplashSettings('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(0, data.response.ssidNumber);
                assert.equal('Click-through splash page', data.response.splashPage);
                assert.equal('https://www.custom_splash_url.com', data.response.splashUrl);
                assert.equal(true, data.response.useSplashUrl);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSsidSplashSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsids - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsids('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsids', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsid('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(0, data.response.number);
                assert.equal('My SSID', data.response.name);
                assert.equal(true, data.response.enabled);
                assert.equal('Click-through splash page', data.response.splashPage);
                assert.equal(false, data.response.ssidAdminAccessible);
                assert.equal('open', data.response.authMode);
                assert.equal('NAT mode', data.response.ipAssignmentMode);
                assert.equal('5 GHz band only', data.response.bandSelection);
                assert.equal(0, data.response.perClientBandwidthLimitUp);
                assert.equal(0, data.response.perClientBandwidthLimitDown);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSsid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(0, data.response.number);
                assert.equal('My SSID', data.response.name);
                assert.equal(true, data.response.enabled);
                assert.equal('Click-through splash page', data.response.splashPage);
                assert.equal(false, data.response.ssidAdminAccessible);
                assert.equal('open', data.response.authMode);
                assert.equal('NAT mode', data.response.ipAssignmentMode);
                assert.equal('5 GHz band only', data.response.bandSelection);
                assert.equal(0, data.response.perClientBandwidthLimitUp);
                assert.equal(0, data.response.perClientBandwidthLimitDown);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSsid', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSwitchSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('N_24329156', data.response.networkId);
                assert.equal(false, data.response.useCombinedPower);
                assert.equal(true, Array.isArray(data.response.powerExceptions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSwitchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSwitchSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.useCombinedPower);
                assert.equal(true, Array.isArray(data.response.powerExceptions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSwitchSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSwitchPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceSwitchPorts('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceSwitchPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSwitchPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDeviceSwitchPort('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.number);
                assert.equal('My switch port', data.response.name);
                assert.equal('tag1 tag2', data.response.tags);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.poeEnabled);
                assert.equal('access', data.response.type);
                assert.equal(10, data.response.vlan);
                assert.equal(20, data.response.voiceVlan);
                assert.equal(false, data.response.isolationEnabled);
                assert.equal(true, data.response.rstpEnabled);
                assert.equal('disabled', data.response.stpGuard);
                assert.equal('1234', data.response.accessPolicyNumber);
                assert.equal('Auto negotiate', data.response.linkNegotiation);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getDeviceSwitchPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceSwitchPort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDeviceSwitchPort('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.number);
                assert.equal('My switch port', data.response.name);
                assert.equal('tag1 tag2', data.response.tags);
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.poeEnabled);
                assert.equal('access', data.response.type);
                assert.equal(10, data.response.vlan);
                assert.equal(20, data.response.voiceVlan);
                assert.equal(false, data.response.isolationEnabled);
                assert.equal(true, data.response.rstpEnabled);
                assert.equal('disabled', data.response.stpGuard);
                assert.equal('1234', data.response.accessPolicyNumber);
                assert.equal('Auto negotiate', data.response.linkNegotiation);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateDeviceSwitchPort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationConfigTemplateSwitchProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationConfigTemplateSwitchProfiles('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getOrganizationConfigTemplateSwitchProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchStacks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSwitchStacks('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSwitchStacks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkSwitchStack('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('8473', data.response.id);
                assert.equal('A cool stack', data.response.name);
                assert.equal(true, Array.isArray(data.response.serials));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkSwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSwitchStack('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('8473', data.response.id);
                assert.equal('A cool stack', data.response.name);
                assert.equal(true, Array.isArray(data.response.serials));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchStack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSwitchStack('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkSwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addNetworkSwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('8473', data.response.id);
                assert.equal('A cool stack', data.response.name);
                assert.equal(true, Array.isArray(data.response.serials));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'addNetworkSwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNetworkSwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.removeNetworkSwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('8473', data.response.id);
                assert.equal('A cool stack', data.response.name);
                assert.equal(true, Array.isArray(data.response.serials));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'removeNetworkSwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSyslogServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSyslogServers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSyslogServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSyslogServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSyslogServers('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSyslogServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkTrafficShaping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkTrafficShaping('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.defaultRulesEnabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkTrafficShaping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShaping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTrafficShaping('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.defaultRulesEnabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkTrafficShaping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidTrafficShaping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSsidTrafficShaping('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.trafficShapingEnabled);
                assert.equal(true, data.response.defaultRulesEnabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkSsidTrafficShaping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidTrafficShaping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSsidTrafficShaping('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.trafficShapingEnabled);
                assert.equal(true, data.response.defaultRulesEnabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkSsidTrafficShaping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShapingDscpTaggingOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTrafficShapingDscpTaggingOptions('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkTrafficShapingDscpTaggingOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShapingApplicationCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTrafficShapingApplicationCategories('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkTrafficShapingApplicationCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContentFilteringCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkContentFilteringCategories('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.categories));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkContentFilteringCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContentFiltering - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkContentFiltering('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.allowedUrlPatterns));
                assert.equal(true, Array.isArray(data.response.blockedUrlPatterns));
                assert.equal(true, Array.isArray(data.response.blockedUrlCategories));
                assert.equal('topSites', data.response.urlCategoryListSize);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkContentFiltering', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkContentFiltering - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkContentFiltering('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.allowedUrlPatterns));
                assert.equal(true, Array.isArray(data.response.blockedUrlPatterns));
                assert.equal(true, Array.isArray(data.response.blockedUrlCategories));
                assert.equal('topSites', data.response.urlCategoryListSize);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkContentFiltering', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFirewalledServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFirewalledServices('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkFirewalledServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFirewalledService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFirewalledService('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('web', data.response.service);
                assert.equal('restricted', data.response.access);
                assert.equal(true, Array.isArray(data.response.allowedIps));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkFirewalledService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkFirewalledService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkFirewalledService('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('web', data.response.service);
                assert.equal('restricted', data.response.access);
                assert.equal(true, Array.isArray(data.response.allowedIps));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkFirewalledService', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkOneToManyNatRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkOneToManyNatRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkOneToManyNatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkOneToManyNatRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkOneToManyNatRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkOneToManyNatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkOneToOneNatRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkOneToOneNatRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkOneToOneNatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkOneToOneNatRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkOneToOneNatRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkOneToOneNatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPortForwardingRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkPortForwardingRules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkPortForwardingRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkPortForwardingRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkPortForwardingRules('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkPortForwardingRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkStaticRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkStaticRoutes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkStaticRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkStaticRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkStaticRoute('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('My route', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('1.2.3.5', data.response.gatewayIp);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkStaticRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkStaticRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkStaticRoute('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('d7fa4948-7921-4dfa-af6b-ae8b16c20c39', data.response.id);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('My route', data.response.name);
                assert.equal('1.2.3.5', data.response.gatewayIp);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('{}', data.response.fixedIpAssignments);
                assert.equal('[]', data.response.reservedIpRanges);
                assert.equal(true, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkStaticRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkStaticRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkStaticRoute('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('d7fa4948-7921-4dfa-af6b-ae8b16c20c39', data.response.id);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('My route', data.response.name);
                assert.equal('1.2.3.5', data.response.gatewayIp);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('{}', data.response.fixedIpAssignments);
                assert.equal('[]', data.response.reservedIpRanges);
                assert.equal(true, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkStaticRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkStaticRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkStaticRoute('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkStaticRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkUplinkSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkUplinkSettings('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.bandwidthLimits);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkUplinkSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkUplinkSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkUplinkSettings('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.bandwidthLimits);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkUplinkSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkVlans('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkVlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkVlan('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('My VLAN', data.response.name);
                assert.equal('1.2.3.4', data.response.applianceIp);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('object', typeof data.response.fixedIpAssignments);
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal('google_dns', data.response.dnsNameservers);
                assert.equal('Run a DHCP server', data.response.dhcpHandling);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal(false, data.response.dhcpBootOptionsEnabled);
                assert.equal('object', typeof data.response.dhcpBootNextServer);
                assert.equal('object', typeof data.response.dhcpBootFilename);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'createNetworkVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkVlan('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('My VLAN', data.response.name);
                assert.equal('1.2.3.4', data.response.applianceIp);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('object', typeof data.response.fixedIpAssignments);
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal('google_dns', data.response.dnsNameservers);
                assert.equal('Run a DHCP server', data.response.dhcpHandling);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal(false, data.response.dhcpBootOptionsEnabled);
                assert.equal('object', typeof data.response.dhcpBootNextServer);
                assert.equal('object', typeof data.response.dhcpBootFilename);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkVlan('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1234', data.response.id);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('My VLAN', data.response.name);
                assert.equal('1.2.3.4', data.response.applianceIp);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('object', typeof data.response.fixedIpAssignments);
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal('google_dns', data.response.dnsNameservers);
                assert.equal('Run a DHCP server', data.response.dhcpHandling);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal(false, data.response.dhcpBootOptionsEnabled);
                assert.equal('object', typeof data.response.dhcpBootNextServer);
                assert.equal('object', typeof data.response.dhcpBootFilename);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkVlan - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkVlan('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'deleteNetworkVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlansEnabledState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkVlansEnabledState('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.enabled);
                assert.equal('N_24329156', data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkVlansEnabledState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkVlansEnabledState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkVlansEnabledState('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.enabled);
                assert.equal('N_24329156', data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'updateNetworkVlansEnabledState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkConnectionStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkConnectionStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.assoc);
                assert.equal(5, data.response.auth);
                assert.equal(0, data.response.dhcp);
                assert.equal(0, data.response.dns);
                assert.equal(51, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkConnectionStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicesConnectionStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevicesConnectionStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevicesConnectionStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceConnectionStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceConnectionStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q2JC-2MJM-FHRD', data.response.serial);
                assert.equal('object', typeof data.response.connectionStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceConnectionStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceConnectionStatsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceConnectionStatsV1('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q2JC-2MJM-FHRD', data.response.serial);
                assert.equal('object', typeof data.response.connectionStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceConnectionStatsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientsConnectionStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientsConnectionStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientsConnectionStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientConnectionStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientConnectionStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:61:71:c8:51:27', data.response.mac);
                assert.equal('object', typeof data.response.connectionStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientConnectionStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkLatencyStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkLatencyStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.backgroundTraffic);
                assert.equal('same shape as backgroundTraffic', data.response.bestEffortTraffic);
                assert.equal('same shape as backgroundTraffic', data.response.videoTraffic);
                assert.equal('same shape as backgroundTraffic', data.response.voiceTraffic);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkLatencyStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicesLatencyStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDevicesLatencyStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDevicesLatencyStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLatencyStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLatencyStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q2JC-2MJM-FHRD', data.response.serial);
                assert.equal('object', typeof data.response.latencyStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLatencyStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLatencyStatsV1 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkDeviceLatencyStatsV1('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Q2JC-2MJM-FHRD', data.response.serial);
                assert.equal('object', typeof data.response.latencyStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkDeviceLatencyStatsV1', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eapOverrideNetworkId = 'fakedata';
    const eapOverrideNumber = 'fakedata';
    const eapOverrideUpdateTheEAPOverriddenParametersForAnSSIDBodyParam = {
      timeout: '<integer>',
      identity: {
        retries: '<integer>',
        timeout: '<integer>'
      },
      maxRetries: '<integer>',
      eapolKey: {
        retries: '<integer>',
        timeoutInMs: '<integer>'
      }
    };
    describe('#updateTheEAPOverriddenParametersForAnSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheEAPOverriddenParametersForAnSSID(eapOverrideNetworkId, eapOverrideNumber, eapOverrideUpdateTheEAPOverriddenParametersForAnSSIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EapOverride', 'updateTheEAPOverriddenParametersForAnSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheEAPOverriddenParametersForAnSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheEAPOverriddenParametersForAnSSID(eapOverrideNetworkId, eapOverrideNumber, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.eap_timeout_override);
                assert.equal(5, data.response.eap_initial_timeout);
                assert.equal(false, data.response.eap_max_retries_override);
                assert.equal(5, data.response.eap_max_retries);
                assert.equal(false, data.response.eap_identity_timeout_override);
                assert.equal(5, data.response.eap_identity_timeout);
                assert.equal(false, data.response.eap_identity_retries_override);
                assert.equal(5, data.response.eap_identity_retries);
                assert.equal(false, data.response.eapol_key_timeout_override);
                assert.equal(500, data.response.eapol_key_timeout);
                assert.equal(false, data.response.eapol_key_retries_override);
                assert.equal(4, data.response.eapol_key_retries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EapOverride', 'returnTheEAPOverriddenParametersForAnSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientsLatencyStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientsLatencyStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientsLatencyStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientLatencyStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkClientLatencyStats('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('00:61:71:c8:51:27', data.response.mac);
                assert.equal('object', typeof data.response.latencyStats);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkClientLatencyStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFailedConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFailedConnections('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
                assert.equal('object', typeof data.response[4]);
                assert.equal('object', typeof data.response[5]);
              } else {
                runCommonAsserts(data, error);
              }

              saveMockData('Wireless health', 'getNetworkFailedConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cameraQualityRetentionProfilesNetworkId = 'fakedata';
    const cameraQualityRetentionProfilesCreateNetworkCameraQualityRetentionProfileBodyParam = {};
    describe('#createNetworkCameraQualityRetentionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkCameraQualityRetentionProfile(cameraQualityRetentionProfilesCreateNetworkCameraQualityRetentionProfileBodyParam, cameraQualityRetentionProfilesNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CameraQualityRetentionProfiles', 'createNetworkCameraQualityRetentionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraQualityRetentionProfiles - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCameraQualityRetentionProfiles(cameraQualityRetentionProfilesNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CameraQualityRetentionProfiles', 'getNetworkCameraQualityRetentionProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cameraQualityRetentionProfilesQualityRetentionProfileId = 'fakedata';
    const cameraQualityRetentionProfilesUpdateNetworkCameraQualityRetentionProfileBodyParam = {};
    describe('#updateNetworkCameraQualityRetentionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkCameraQualityRetentionProfile(cameraQualityRetentionProfilesUpdateNetworkCameraQualityRetentionProfileBodyParam, cameraQualityRetentionProfilesNetworkId, cameraQualityRetentionProfilesQualityRetentionProfileId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CameraQualityRetentionProfiles', 'updateNetworkCameraQualityRetentionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraQualityRetentionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCameraQualityRetentionProfile(cameraQualityRetentionProfilesNetworkId, cameraQualityRetentionProfilesQualityRetentionProfileId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CameraQualityRetentionProfiles', 'getNetworkCameraQualityRetentionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGConnectivityMonitoringDestinationsNetworkId = 'fakedata';
    const mGConnectivityMonitoringDestinationsUpdateNetworkCellularGatewaySettingsConnectivityMonitoringDestinationsBodyParam = {};
    describe('#updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(mGConnectivityMonitoringDestinationsNetworkId, mGConnectivityMonitoringDestinationsUpdateNetworkCellularGatewaySettingsConnectivityMonitoringDestinationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGConnectivityMonitoringDestinations', 'updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(mGConnectivityMonitoringDestinationsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGConnectivityMonitoringDestinations', 'getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGDHCPSettingsNetworkId = 'fakedata';
    const mGDHCPSettingsUpdateNetworkCellularGatewaySettingsDhcpBodyParam = {};
    describe('#updateNetworkCellularGatewaySettingsDhcp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsDhcp(mGDHCPSettingsNetworkId, mGDHCPSettingsUpdateNetworkCellularGatewaySettingsDhcpBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGDHCPSettings', 'updateNetworkCellularGatewaySettingsDhcp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsDhcp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsDhcp(mGDHCPSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGDHCPSettings', 'getNetworkCellularGatewaySettingsDhcp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGLANSettingsSerial = 'fakedata';
    const mGLANSettingsUpdateDeviceCellularGatewaySettingsBodyParam = {};
    describe('#updateDeviceCellularGatewaySettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceCellularGatewaySettings(mGLANSettingsSerial, mGLANSettingsUpdateDeviceCellularGatewaySettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGLANSettings', 'updateDeviceCellularGatewaySettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCellularGatewaySettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceCellularGatewaySettings(mGLANSettingsSerial, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGLANSettings', 'getDeviceCellularGatewaySettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGPortforwardingRulesSerial = 'fakedata';
    const mGPortforwardingRulesUpdateDeviceCellularGatewaySettingsPortForwardingRulesBodyParam = {};
    describe('#updateDeviceCellularGatewaySettingsPortForwardingRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDeviceCellularGatewaySettingsPortForwardingRules(mGPortforwardingRulesSerial, mGPortforwardingRulesUpdateDeviceCellularGatewaySettingsPortForwardingRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGPortforwardingRules', 'updateDeviceCellularGatewaySettingsPortForwardingRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCellularGatewaySettingsPortForwardingRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDeviceCellularGatewaySettingsPortForwardingRules(mGPortforwardingRulesSerial, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGPortforwardingRules', 'getDeviceCellularGatewaySettingsPortForwardingRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGSubnetPoolSettingsNetworkId = 'fakedata';
    const mGSubnetPoolSettingsUpdateNetworkCellularGatewaySettingsSubnetPoolBodyParam = {};
    describe('#updateNetworkCellularGatewaySettingsSubnetPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsSubnetPool(mGSubnetPoolSettingsNetworkId, mGSubnetPoolSettingsUpdateNetworkCellularGatewaySettingsSubnetPoolBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGSubnetPoolSettings', 'updateNetworkCellularGatewaySettingsSubnetPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsSubnetPool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsSubnetPool(mGSubnetPoolSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGSubnetPoolSettings', 'getNetworkCellularGatewaySettingsSubnetPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mGUplinkSettingsNetworkId = 'fakedata';
    const mGUplinkSettingsUpdateNetworkCellularGatewaySettingsUplinkBodyParam = {};
    describe('#updateNetworkCellularGatewaySettingsUplink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsUplink(mGUplinkSettingsNetworkId, mGUplinkSettingsUpdateNetworkCellularGatewaySettingsUplinkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGUplinkSettings', 'updateNetworkCellularGatewaySettingsUplink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsUplink - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsUplink(mGUplinkSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MGUplinkSettings', 'getNetworkCellularGatewaySettingsUplink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchACLsNetworkId = 'fakedata';
    const switchACLsUpdateNetworkSwitchAccessControlListsBodyParam = {};
    describe('#updateNetworkSwitchAccessControlLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchAccessControlLists(switchACLsUpdateNetworkSwitchAccessControlListsBodyParam, switchACLsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchACLs', 'updateNetworkSwitchAccessControlLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchAccessControlLists - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchAccessControlLists(switchACLsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchACLs', 'getNetworkSwitchAccessControlLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchPortsSchedulesNetworkId = 'fakedata';
    const switchPortsSchedulesCreateNetworkSwitchPortScheduleBodyParam = {};
    describe('#createNetworkSwitchPortSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkSwitchPortSchedule(switchPortsSchedulesNetworkId, switchPortsSchedulesCreateNetworkSwitchPortScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchPortsSchedules', 'createNetworkSwitchPortSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchPortSchedules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchPortSchedules(switchPortsSchedulesNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchPortsSchedules', 'getNetworkSwitchPortSchedules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchPortsSchedulesPortScheduleId = 'fakedata';
    const switchPortsSchedulesUpdateNetworkSwitchPortScheduleBodyParam = {};
    describe('#updateNetworkSwitchPortSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchPortSchedule(switchPortsSchedulesNetworkId, switchPortsSchedulesPortScheduleId, switchPortsSchedulesUpdateNetworkSwitchPortScheduleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchPortsSchedules', 'updateNetworkSwitchPortSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mXVLANPortsNetworkId = 'fakedata';
    describe('#getNetworkAppliancePorts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkAppliancePorts(mXVLANPortsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXVLANPorts', 'getNetworkAppliancePorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mXVLANPortsAppliancePortId = 'fakedata';
    const mXVLANPortsUpdateNetworkAppliancePortBodyParam = {};
    describe('#updateNetworkAppliancePort - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkAppliancePort(mXVLANPortsUpdateNetworkAppliancePortBodyParam, mXVLANPortsNetworkId, mXVLANPortsAppliancePortId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXVLANPorts', 'updateNetworkAppliancePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAppliancePort - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkAppliancePort(mXVLANPortsNetworkId, mXVLANPortsAppliancePortId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXVLANPorts', 'getNetworkAppliancePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mXWarmSpareSettingsNetworkId = 'fakedata';
    const mXWarmSpareSettingsSwapNetworkWarmspareBodyParam = {};
    describe('#swapNetworkWarmspare - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.swapNetworkWarmspare(mXWarmSpareSettingsNetworkId, mXWarmSpareSettingsSwapNetworkWarmspareBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXWarmSpareSettings', 'swapNetworkWarmspare', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mXWarmSpareSettingsUpdateNetworkWarmSpareSettingsBodyParam = {};
    describe('#updateNetworkWarmSpareSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkWarmSpareSettings(mXWarmSpareSettingsNetworkId, mXWarmSpareSettingsUpdateNetworkWarmSpareSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXWarmSpareSettings', 'updateNetworkWarmSpareSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWarmSpareSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkWarmSpareSettings(mXWarmSpareSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MXWarmSpareSettings', 'getNetworkWarmSpareSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesOrganizationId = 'fakedata';
    const licensesAssignOrganizationLicensesSeatsBodyParam = {};
    describe('#assignOrganizationLicensesSeats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignOrganizationLicensesSeats(licensesOrganizationId, licensesAssignOrganizationLicensesSeatsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'assignOrganizationLicensesSeats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesMoveOrganizationLicensesBodyParam = {};
    describe('#moveOrganizationLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.moveOrganizationLicenses(licensesOrganizationId, licensesMoveOrganizationLicensesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'moveOrganizationLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesMoveOrganizationLicensesSeatsBodyParam = {};
    describe('#moveOrganizationLicensesSeats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.moveOrganizationLicensesSeats(licensesMoveOrganizationLicensesSeatsBodyParam, licensesOrganizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'moveOrganizationLicensesSeats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesRenewOrganizationLicensesSeatsBodyParam = {};
    describe('#renewOrganizationLicensesSeats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.renewOrganizationLicensesSeats(licensesRenewOrganizationLicensesSeatsBodyParam, licensesOrganizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'renewOrganizationLicensesSeats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicenses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationLicenses(null, null, null, null, null, null, licensesOrganizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getOrganizationLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensesLicenseId = 'fakedata';
    const licensesUpdateOrganizationLicenseBodyParam = {};
    describe('#updateOrganizationLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateOrganizationLicense(licensesUpdateOrganizationLicenseBodyParam, licensesOrganizationId, licensesLicenseId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'updateOrganizationLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicense - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOrganizationLicense(licensesOrganizationId, licensesLicenseId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Licenses', 'getOrganizationLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsNetworkId = 'fakedata';
    const switchSettingsCreateNetworkSwitchSettingsQosRuleBodyParam = {};
    describe('#createNetworkSwitchSettingsQosRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkSwitchSettingsQosRule(switchSettingsCreateNetworkSwitchSettingsQosRuleBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'createNetworkSwitchSettingsQosRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsDhcpServerPolicyBodyParam = {};
    describe('#updateNetworkSwitchSettingsDhcpServerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsDhcpServerPolicy(switchSettingsUpdateNetworkSwitchSettingsDhcpServerPolicyBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsDhcpServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsDhcpServerPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsDhcpServerPolicy(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsDhcpServerPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsDscpToCosMappingsBodyParam = {};
    describe('#updateNetworkSwitchSettingsDscpToCosMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsDscpToCosMappings(switchSettingsUpdateNetworkSwitchSettingsDscpToCosMappingsBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsDscpToCosMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsDscpToCosMappings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsDscpToCosMappings(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsDscpToCosMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsMtuBodyParam = {};
    describe('#updateNetworkSwitchSettingsMtu - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsMtu(switchSettingsUpdateNetworkSwitchSettingsMtuBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsMtu', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsMtu - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsMtu(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsMtu', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRules - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRules(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch settings', 'getNetworkSwitchSettingsQosRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRule(switchSettingsNetworkId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Switch settings', 'getNetworkSwitchSettingsQosRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsQosRulesOrderBodyParam = {};
    describe('#updateNetworkSwitchSettingsQosRulesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsQosRulesOrder(switchSettingsUpdateNetworkSwitchSettingsQosRulesOrderBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsQosRulesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRulesOrder - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRulesOrder(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsQosRulesOrder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsQosRuleId = 'fakedata';
    const switchSettingsUpdateNetworkSwitchSettingsQosRuleBodyParam = {};
    describe('#updateNetworkSwitchSettingsQosRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsQosRule(switchSettingsNetworkId, switchSettingsQosRuleId, switchSettingsUpdateNetworkSwitchSettingsQosRuleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsQosRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsStormControlBodyParam = {};
    describe('#updateNetworkSwitchSettingsStormControl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsStormControl(switchSettingsNetworkId, switchSettingsUpdateNetworkSwitchSettingsStormControlBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsStormControl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsStormControl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsStormControl(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsStormControl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const switchSettingsUpdateNetworkSwitchSettingsStpBodyParam = {};
    describe('#updateNetworkSwitchSettingsStp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkSwitchSettingsStp(switchSettingsUpdateNetworkSwitchSettingsStpBodyParam, switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'updateNetworkSwitchSettingsStp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsStp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getNetworkSwitchSettingsStp(switchSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'getNetworkSwitchSettingsStp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkCameraQualityRetentionProfile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkCameraQualityRetentionProfile(cameraQualityRetentionProfilesNetworkId, cameraQualityRetentionProfilesQualityRetentionProfileId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CameraQualityRetentionProfiles', 'deleteNetworkCameraQualityRetentionProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchPortSchedule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSwitchPortSchedule(switchPortsSchedulesNetworkId, switchPortsSchedulesPortScheduleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchPortsSchedules', 'deleteNetworkSwitchPortSchedule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchSettingsQosRule - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSwitchSettingsQosRule(switchSettingsNetworkId, switchSettingsQosRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SwitchSettings', 'deleteNetworkSwitchSettingsQosRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const connectivityMonitoringDestinationsNetworkId = 'fakedata';
    const connectivityMonitoringDestinationsUpdateNetworkConnectivityMonitoringDestinationsBodyParam = {};
    describe('#updateNetworkConnectivityMonitoringDestinations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateNetworkConnectivityMonitoringDestinations(connectivityMonitoringDestinationsNetworkId, connectivityMonitoringDestinationsUpdateNetworkConnectivityMonitoringDestinationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectivityMonitoringDestinations', 'updateNetworkConnectivityMonitoringDestinations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkConnectivityMonitoringDestinations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkConnectivityMonitoringDestinations(connectivityMonitoringDestinationsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.destinations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectivityMonitoringDestinations', 'getNetworkConnectivityMonitoringDestinations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardBrandingPoliciesOrganizationId = 'fakedata';
    let dashboardBrandingPoliciesBrandingPolicyId = 'fakedata';
    const dashboardBrandingPoliciesCreateOrganizationBrandingPolicyBodyParam = {};
    describe('#createOrganizationBrandingPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createOrganizationBrandingPolicy(dashboardBrandingPoliciesOrganizationId, dashboardBrandingPoliciesCreateOrganizationBrandingPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.brandingPolicyId);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.adminSettings);
                assert.equal('object', typeof data.response.helpSettings);
              } else {
                runCommonAsserts(data, error);
              }
              dashboardBrandingPoliciesBrandingPolicyId = data.response.brandingPolicyId;
              saveMockData('DashboardBrandingPolicies', 'createOrganizationBrandingPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationBrandingPolicies(dashboardBrandingPoliciesOrganizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'getOrganizationBrandingPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationBrandingPolicy(dashboardBrandingPoliciesOrganizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.brandingPolicyId);
                assert.equal('string', data.response.name);
                assert.equal(false, data.response.enabled);
                assert.equal('object', typeof data.response.adminSettings);
                assert.equal('object', typeof data.response.helpSettings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'getOrganizationBrandingPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardBrandingPoliciesUpdateOrganizationBrandingPoliciesPrioritiesBodyParam = {};
    describe('#updateOrganizationBrandingPoliciesPriorities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationBrandingPoliciesPriorities(dashboardBrandingPoliciesOrganizationId, dashboardBrandingPoliciesUpdateOrganizationBrandingPoliciesPrioritiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'updateOrganizationBrandingPoliciesPriorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPoliciesPriorities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationBrandingPoliciesPriorities(dashboardBrandingPoliciesOrganizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.brandingPolicyIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'getOrganizationBrandingPoliciesPriorities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dashboardBrandingPoliciesUpdateOrganizationBrandingPolicyBodyParam = {};
    describe('#updateOrganizationBrandingPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateOrganizationBrandingPolicy(dashboardBrandingPoliciesOrganizationId, dashboardBrandingPoliciesBrandingPolicyId, dashboardBrandingPoliciesUpdateOrganizationBrandingPolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'updateOrganizationBrandingPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const eventsNetworkId = 'fakedata';
    describe('#getNetworkEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkEvents(null, null, null, null, null, null, null, null, null, null, null, null, null, null, eventsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.pageStartAt);
                assert.equal('string', data.response.pageEndAt);
                assert.equal(true, Array.isArray(data.response.events));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'getNetworkEvents', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEventsEventTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkEventsEventTypes(eventsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Events', 'getNetworkEventsEventTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const floorplansNetworkId = 'fakedata';
    const floorplansCreateNetworkFloorPlanBodyParam = {};
    describe('#createNetworkFloorPlan - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkFloorPlan(floorplansNetworkId, floorplansCreateNetworkFloorPlanBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Floorplans', 'createNetworkFloorPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFloorPlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFloorPlans(floorplansNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Floorplans', 'getNetworkFloorPlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const floorplansUpdateNetworkFloorPlanBodyParam = {};
    describe('#updateNetworkFloorPlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkFloorPlan(floorplansNetworkId, 'fakedata', floorplansUpdateNetworkFloorPlanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Floorplans', 'updateNetworkFloorPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFloorPlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkFloorPlan(floorplansNetworkId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.floorPlanId);
                assert.equal('string', data.response.imageUrl);
                assert.equal('string', data.response.imageUrlExpiresAt);
                assert.equal('string', data.response.imageExtension);
                assert.equal('string', data.response.imageMd5);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(4, data.response.width);
                assert.equal(9, data.response.height);
                assert.equal('object', typeof data.response.center);
                assert.equal('object', typeof data.response.bottomLeftCorner);
                assert.equal('object', typeof data.response.bottomRightCorner);
                assert.equal('object', typeof data.response.topLeftCorner);
                assert.equal('object', typeof data.response.topRightCorner);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Floorplans', 'getNetworkFloorPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let linkAggregationsNetworkId = 'fakedata';
    const linkAggregationsCreateNetworkSwitchLinkAggregationBodyParam = {};
    describe('#createNetworkSwitchLinkAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkSwitchLinkAggregation(linkAggregationsNetworkId, linkAggregationsCreateNetworkSwitchLinkAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.switchPorts));
              } else {
                runCommonAsserts(data, error);
              }
              linkAggregationsNetworkId = data.response.id;
              saveMockData('LinkAggregations', 'createNetworkSwitchLinkAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchLinkAggregations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkSwitchLinkAggregations(linkAggregationsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkAggregations', 'getNetworkSwitchLinkAggregations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkAggregationsLinkAggregationId = 'fakedata';
    const linkAggregationsUpdateNetworkSwitchLinkAggregationBodyParam = {};
    describe('#updateNetworkSwitchLinkAggregation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkSwitchLinkAggregation(linkAggregationsNetworkId, linkAggregationsLinkAggregationId, linkAggregationsUpdateNetworkSwitchLinkAggregationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkAggregations', 'updateNetworkSwitchLinkAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowSettingsNetworkId = 'fakedata';
    const netFlowSettingsUpdateNetworkNetflowSettingsBodyParam = {};
    describe('#updateNetworkNetflowSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkNetflowSettings(netFlowSettingsNetworkId, netFlowSettingsUpdateNetworkNetflowSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlowSettings', 'updateNetworkNetflowSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkNetflowSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkNetflowSettings(netFlowSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.reportingEnabled);
                assert.equal('string', data.response.collectorIp);
                assert.equal(7, data.response.collectorPort);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlowSettings', 'getNetworkNetflowSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trafficAnalysisSettingsNetworkId = 'fakedata';
    describe('#updateNetworkTrafficAnalysisSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkTrafficAnalysisSettings(trafficAnalysisSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficAnalysisSettings', 'updateNetworkTrafficAnalysisSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficAnalysisSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkTrafficAnalysisSettings(trafficAnalysisSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.mode);
                assert.equal(true, Array.isArray(data.response.customPieChartItems));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrafficAnalysisSettings', 'getNetworkTrafficAnalysisSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webhookLogsOrganizationId = 'fakedata';
    describe('#getOrganizationWebhookLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOrganizationWebhookLogs(null, null, null, null, null, null, null, webhookLogsOrganizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WebhookLogs', 'getOrganizationWebhookLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessSettingsNetworkId = 'fakedata';
    const wirelessSettingsUpdateNetworkWirelessSettingsBodyParam = {};
    describe('#updateNetworkWirelessSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateNetworkWirelessSettings(wirelessSettingsNetworkId, wirelessSettingsUpdateNetworkWirelessSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessSettings', 'updateNetworkWirelessSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWirelessSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworkWirelessSettings(wirelessSettingsNetworkId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.meshingEnabled);
                assert.equal(true, data.response.ipv6BridgeEnabled);
                assert.equal(true, data.response.locationAnalyticsEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessSettings', 'getNetworkWirelessSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationBrandingPolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOrganizationBrandingPolicy(dashboardBrandingPoliciesOrganizationId, dashboardBrandingPoliciesBrandingPolicyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DashboardBrandingPolicies', 'deleteOrganizationBrandingPolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const floorplansFloorPlanId = 'fakedata';
    describe('#deleteNetworkFloorPlan - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkFloorPlan(floorplansNetworkId, floorplansFloorPlanId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Floorplans', 'deleteNetworkFloorPlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchLinkAggregation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkSwitchLinkAggregation(linkAggregationsNetworkId, linkAggregationsLinkAggregationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkAggregations', 'deleteNetworkSwitchLinkAggregation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnstheidentityofthecurrentuser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnstheidentityofthecurrentuser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Miles Meraki', data.response.name);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('2018-02-11T00:00:00.090210Z', data.response.lastUsedDashboardAt);
                assert.equal('object', typeof data.response.authentication);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Me', 'returnstheidentityofthecurrentuser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheDHCPsubnetinformationforanappliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returntheDHCPsubnetinformationforanappliance('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'returntheDHCPsubnetinformationforanappliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getthesentandreceivedbytesforeachuplinkofanetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getthesentandreceivedbytesforeachuplinkofanetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsageHistory', 'getthesentandreceivedbytesforeachuplinkofanetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPusageovertimeforadeviceornetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAPusageovertimeforadeviceornetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsageHistory', 'returnAPusageovertimeforadeviceornetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchOnboardingStatusOfCameras - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fetchOnboardingStatusOfCameras('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'fetchOnboardingStatusOfCameras', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statusesNotifyThatCredentialHandoffToCameraHasCompletedBodyParam = {
      serial: '<string>',
      wirelessCredentialsSent: '<boolean>'
    };
    describe('#notifyThatCredentialHandoffToCameraHasCompleted - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.notifyThatCredentialHandoffToCameraHasCompleted('fakedata', statusesNotifyThatCredentialHandoffToCameraHasCompletedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'notifyThatCredentialHandoffToCameraHasCompleted', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNstatusfornetworksinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.showVPNstatusfornetworksinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'showVPNstatusfornetworksinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheStatusOfEveryMerakiDeviceInTheOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheStatusOfEveryMerakiDeviceInTheOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('My AP', data.response.name);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('123.123.123.1', data.response.publicIp);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('online', data.response.status);
                assert.equal('2018-02-11T00:00:00.090210Z', data.response.lastReportedAt);
                assert.equal('1.2.3.4', data.response.lanIp);
                assert.equal('1.2.3.5', data.response.gateway);
                assert.equal('dhcp', data.response.ipType);
                assert.equal('8.8.8.8', data.response.primaryDns);
                assert.equal('8.8.4.4', data.response.secondaryDns);
                assert.equal('wireless', data.response.productType);
                assert.equal('object', typeof data.response.components);
                assert.equal('MR34', data.response.model);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Statuses', 'listTheStatusOfEveryMerakiDeviceInTheOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNhistorystatfornetworksinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.showVPNhistorystatfornetworksinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'showVPNhistorystatfornetworksinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheFirewallSettingsForThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheFirewallSettingsForThisNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.spoofingProtection);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnTheFirewallSettingsForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateTheFirewallSettingsForThisNetworkBodyParam = {
      spoofingProtection: {
        ipSourceGuard: {
          mode: '<string>'
        }
      }
    };
    describe('#updateTheFirewallSettingsForThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheFirewallSettingsForThisNetwork('fakedata', settingsUpdateTheFirewallSettingsForThisNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateTheFirewallSettingsForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheApplianceSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheApplianceSettingsForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('MAC address', data.response.clientTrackingMethod);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnTheApplianceSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateTheApplianceSettingsForANetworkBodyParam = {
      clientTrackingMethod: '<string>',
      deploymentMode: '<string>'
    };
    describe('#updateTheApplianceSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheApplianceSettingsForANetwork('fakedata', settingsUpdateTheApplianceSettingsForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateTheApplianceSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheEnabledStatusOfVLANsForTheNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsTheEnabledStatusOfVLANsForTheNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.vlansEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnsTheEnabledStatusOfVLANsForTheNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsEnableDisableVLANsForTheGivenNetworkBodyParam = {
      vlansEnabled: '<boolean>'
    };
    describe('#enableDisableVLANsForTheGivenNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableDisableVLANsForTheGivenNetwork('fakedata', settingsEnableDisableVLANsForTheGivenNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'enableDisableVLANsForTheGivenNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSettingsForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.localStatusPageEnabled);
                assert.equal(true, data.response.remoteStatusPageEnabled);
                assert.equal('object', typeof data.response.localStatusPage);
                assert.equal('object', typeof data.response.secureConnect);
                assert.equal('object', typeof data.response.fips);
                assert.equal('object', typeof data.response.namedVlans);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnTheSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateTheSettingsForANetworkBodyParam = {
      localStatusPageEnabled: '<boolean>',
      remoteStatusPageEnabled: '<boolean>',
      secureConnect: {
        enabled: '<boolean>'
      },
      localStatusPage: {
        authentication: {
          enabled: '<boolean>',
          password: '<string>'
        }
      }
    };
    describe('#updateTheSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheSettingsForANetwork('fakedata', settingsUpdateTheSettingsForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateTheSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsGlobalAdaptivePolicySettingsInAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsGlobalAdaptivePolicySettingsInAnOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.enabledNetworks));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnsGlobalAdaptivePolicySettingsInAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateGlobalAdaptivePolicySettingsBodyParam = {
      enabledNetworks: [
        '<string>',
        '<string>'
      ]
    };
    describe('#updateGlobalAdaptivePolicySettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGlobalAdaptivePolicySettings('fakedata', settingsUpdateGlobalAdaptivePolicySettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateGlobalAdaptivePolicySettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBluetoothSettingsForAWirelessDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheBluetoothSettingsForAWirelessDevice('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('00000000-0000-0000-000-000000000000', data.response.uuid);
                assert.equal(13, data.response.major);
                assert.equal(125, data.response.minor);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnTheBluetoothSettingsForAWirelessDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateTheBluetoothSettingsForAWirelessDeviceBodyParam = {
      uuid: '<string>',
      major: '<integer>',
      minor: '<integer>'
    };
    describe('#updateTheBluetoothSettingsForAWirelessDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheBluetoothSettingsForAWirelessDevice('fakedata', settingsUpdateTheBluetoothSettingsForAWirelessDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateTheBluetoothSettingsForAWirelessDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.scanningEnabled);
                assert.equal(true, data.response.advertisingEnabled);
                assert.equal('00000000-0000-0000-000-000000000000', data.response.uuid);
                assert.equal('Non-unique', data.response.majorMinorAssignmentMode);
                assert.equal(1, data.response.major);
                assert.equal(1, data.response.minor);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateTheBluetoothSettingsForANetworkBodyParam = {
      scanningEnabled: '<boolean>',
      advertisingEnabled: '<boolean>',
      uuid: '<string>',
      majorMinorAssignmentMode: '<string>',
      major: '<integer>',
      minor: '<integer>'
    };
    describe('#updateTheBluetoothSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheBluetoothSettingsForANetwork('fakedata', settingsUpdateTheBluetoothSettingsForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateTheBluetoothSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAllThePortsOfASwitchProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAllThePortsOfASwitchProfile('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'returnAllThePortsOfASwitchProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASwitchProfilePort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnASwitchProfilePort('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.portId);
                assert.equal('My switch port', data.response.name);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(true, data.response.enabled);
                assert.equal(true, data.response.poeEnabled);
                assert.equal('access', data.response.type);
                assert.equal(10, data.response.vlan);
                assert.equal(20, data.response.voiceVlan);
                assert.equal('1,3,5-10', data.response.allowedVlans);
                assert.equal(false, data.response.isolationEnabled);
                assert.equal(true, data.response.rstpEnabled);
                assert.equal('disabled', data.response.stpGuard);
                assert.equal('Auto negotiate', data.response.linkNegotiation);
                assert.equal(true, Array.isArray(data.response.linkNegotiationCapabilities));
                assert.equal('1234', data.response.portScheduleId);
                assert.equal('Alert only', data.response.udld);
                assert.equal('Sticky MAC allow list', data.response.accessPolicyType);
                assert.equal(2, data.response.accessPolicyNumber);
                assert.equal(true, Array.isArray(data.response.macAllowList));
                assert.equal(true, Array.isArray(data.response.stickyMacAllowList));
                assert.equal(5, data.response.stickyMacAllowListLimit);
                assert.equal(true, data.response.stormControlEnabled);
                assert.equal(true, data.response.flexibleStackingEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'returnASwitchProfilePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portsUpdateASwitchProfilePortBodyParam = {
      name: '<string>',
      tags: [
        '<string>',
        '<string>'
      ],
      enabled: '<boolean>',
      poeEnabled: '<boolean>',
      type: '<string>',
      vlan: '<integer>',
      voiceVlan: '<integer>',
      allowedVlans: '<string>',
      isolationEnabled: '<boolean>',
      rstpEnabled: '<boolean>',
      stpGuard: '<string>',
      linkNegotiation: '<string>',
      portScheduleId: '<string>',
      udld: '<string>',
      accessPolicyType: '<string>',
      accessPolicyNumber: '<integer>',
      macAllowList: [
        '<string>',
        '<string>'
      ],
      stickyMacAllowList: [
        '<string>',
        '<string>'
      ],
      stickyMacAllowListLimit: '<integer>',
      stormControlEnabled: '<boolean>',
      flexibleStackingEnabled: '<boolean>'
    };
    describe('#updateASwitchProfilePort - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateASwitchProfilePort('fakedata', 'fakedata', 'fakedata', 'fakedata', portsUpdateASwitchProfilePortBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ports', 'updateASwitchProfilePort', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnSingleLANConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnSingleLANConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('192.168.1.2', data.response.applianceIp);
                assert.equal('object', typeof data.response.fixedIpAssignments);
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal('upstream_dns', data.response.dnsNameservers);
                assert.equal(true, Array.isArray(data.response.dnsCustomNameservers));
                assert.equal('Run a DHCP server', data.response.dhcpHandling);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal(false, data.response.dhcpBootOptionsEnabled);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SingleLan', 'returnSingleLANConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const singleLanUpdateSingleLANConfigurationBodyParam = {
      subnet: '<string>',
      applianceIp: '<string>'
    };
    describe('#updateSingleLANConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSingleLANConfiguration('fakedata', singleLanUpdateSingleLANConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SingleLan', 'updateSingleLANConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3StaticRoutesForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLayer3StaticRoutesForASwitch('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'listLayer3StaticRoutesForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const staticRoutesCreateALayer3StaticRouteForASwitchBodyParam = {
      subnet: '<string>',
      nextHopIp: '<string>',
      name: '<string>',
      advertiseViaOspfEnabled: '<boolean>',
      preferOverOspfRoutesEnabled: '<boolean>'
    };
    describe('#createALayer3StaticRouteForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createALayer3StaticRouteForASwitch('fakedata', staticRoutesCreateALayer3StaticRouteForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.staticRouteId);
                assert.equal('My route', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal(false, data.response.advertiseViaOspfEnabled);
                assert.equal(false, data.response.preferOverOspfRoutesEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'createALayer3StaticRouteForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3StaticRouteForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.staticRouteId);
                assert.equal('My route', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal(false, data.response.advertiseViaOspfEnabled);
                assert.equal(false, data.response.preferOverOspfRoutesEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'returnALayer3StaticRouteForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const staticRoutesUpdateALayer3StaticRouteForASwitchBodyParam = {
      name: '<string>',
      subnet: '<string>',
      nextHopIp: '<string>',
      advertiseViaOspfEnabled: '<boolean>',
      preferOverOspfRoutesEnabled: '<boolean>'
    };
    describe('#updateALayer3StaticRouteForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitch('fakedata', 'fakedata', staticRoutesUpdateALayer3StaticRouteForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'updateALayer3StaticRouteForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3StaticRouteForASwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'deleteALayer3StaticRouteForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3StaticRoutesForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLayer3StaticRoutesForASwitchStack('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'listLayer3StaticRoutesForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const staticRoutesCreateALayer3StaticRouteForASwitchStackBodyParam = {
      subnet: '<string>',
      nextHopIp: '<string>',
      name: '<string>',
      advertiseViaOspfEnabled: '<boolean>',
      preferOverOspfRoutesEnabled: '<boolean>'
    };
    describe('#createALayer3StaticRouteForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createALayer3StaticRouteForASwitchStack('fakedata', 'fakedata', staticRoutesCreateALayer3StaticRouteForASwitchStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.staticRouteId);
                assert.equal('My route', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal(false, data.response.advertiseViaOspfEnabled);
                assert.equal(false, data.response.preferOverOspfRoutesEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'createALayer3StaticRouteForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3StaticRouteForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.staticRouteId);
                assert.equal('My route', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('1.2.3.4', data.response.nextHopIp);
                assert.equal(false, data.response.advertiseViaOspfEnabled);
                assert.equal(false, data.response.preferOverOspfRoutesEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'returnALayer3StaticRouteForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const staticRoutesUpdateALayer3StaticRouteForASwitchStackBodyParam = {
      name: '<string>',
      subnet: '<string>',
      nextHopIp: '<string>',
      advertiseViaOspfEnabled: '<boolean>',
      preferOverOspfRoutesEnabled: '<boolean>'
    };
    describe('#updateALayer3StaticRouteForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitchStack('fakedata', 'fakedata', 'fakedata', staticRoutesUpdateALayer3StaticRouteForASwitchStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'updateALayer3StaticRouteForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3StaticRouteForASwitchStack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('StaticRoutes', 'deleteALayer3StaticRouteForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllCustomPerformanceClassesForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllCustomPerformanceClassesForAnMXNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomPerformanceClasses', 'listAllCustomPerformanceClassesForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customPerformanceClassesAddACustomPerformanceClassForAnMXNetworkBodyParam = {
      name: '<string>',
      maxLatency: '<integer>',
      maxJitter: '<integer>',
      maxLossPercentage: '<integer>'
    };
    describe('#addACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addACustomPerformanceClassForAnMXNetwork('fakedata', customPerformanceClassesAddACustomPerformanceClassForAnMXNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('123', data.response.customPerformanceClassId);
                assert.equal('myCustomPerformanceClass', data.response.name);
                assert.equal(100, data.response.maxLatency);
                assert.equal(100, data.response.maxJitter);
                assert.equal(5, data.response.maxLossPercentage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomPerformanceClasses', 'addACustomPerformanceClassForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnACustomPerformanceClassForAnMXNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('123', data.response.customPerformanceClassId);
                assert.equal('myCustomPerformanceClass', data.response.name);
                assert.equal(100, data.response.maxLatency);
                assert.equal(100, data.response.maxJitter);
                assert.equal(5, data.response.maxLossPercentage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomPerformanceClasses', 'returnACustomPerformanceClassForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customPerformanceClassesUpdateACustomPerformanceClassForAnMXNetworkBodyParam = {
      name: '<string>',
      maxLatency: '<integer>',
      maxJitter: '<integer>',
      maxLossPercentage: '<integer>'
    };
    describe('#updateACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateACustomPerformanceClassForAnMXNetwork('fakedata', 'fakedata', customPerformanceClassesUpdateACustomPerformanceClassForAnMXNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomPerformanceClasses', 'updateACustomPerformanceClassForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteACustomPerformanceClassFromAnMXNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteACustomPerformanceClassFromAnMXNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomPerformanceClasses', 'deleteACustomPerformanceClassFromAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rulesUpdateTheTrafficShapingSettingsRulesForAnMXNetworkBodyParam = {
      defaultRulesEnabled: '<boolean>',
      rules: [
        {
          definitions: {
            type: 'array',
            description: '    A list of objects describing the definitions of your traffic shaping rule. At least one definition is required.\n',
            maxItems: 2,
            minItems: 2,
            items: {
              type: 'object',
              required: [
                'type',
                'value'
              ],
              properties: {
                type: {
                  type: 'string',
                  enum: [
                    'application',
                    'applicationCategory',
                    'host',
                    'port',
                    'ipRange',
                    'localNet'
                  ],
                  description: 'The type of definition. Can be one of \'application\', \'applicationCategory\', \'host\', \'port\', \'ipRange\' or \'localNet\'.',
                  default: '<string>'
                },
                value: {
                  type: 'string',
                  description: '    If "type\' is \'host\', \'port\', \'ipRange\' or \'localNet\', then \'value\' must be a string, matching either\n    a hostname (e.g. \'somesite.com\'), a port (e.g. 8080), or an IP range (\'192.1.0.0\',\n    \'192.1.0.0/16\', or \'10.1.0.0/16:80\'). \'localNet\' also supports CIDR notation, excluding\n    custom ports.\n     If \'type\' is \'application\' or \'applicationCategory\', then \'value\' must be an object\n    with the structure { \'id\': \'meraki:layer7/...\' }, where \'id\' is the application category or\n    application ID (for a list of IDs for your network, use the trafficShaping/applicationCategories\n    endpoint).\n',
                  default: '<string>'
                }
              }
            }
          },
          perClientBandwidthLimits: {
            settings: '<string>',
            bandwidthLimits: {
              limitUp: '<integer>',
              limitDown: '<integer>'
            }
          },
          dscpTagValue: '<integer>',
          priority: '<string>'
        },
        {
          definitions: {
            type: 'array',
            description: '    A list of objects describing the definitions of your traffic shaping rule. At least one definition is required.\n',
            maxItems: 2,
            minItems: 2,
            items: {
              type: 'object',
              required: [
                'type',
                'value'
              ],
              properties: {
                type: {
                  type: 'string',
                  enum: [
                    'application',
                    'applicationCategory',
                    'host',
                    'port',
                    'ipRange',
                    'localNet'
                  ],
                  description: 'The type of definition. Can be one of \'application\', \'applicationCategory\', \'host\', \'port\', \'ipRange\' or \'localNet\'.',
                  default: '<string>'
                },
                value: {
                  type: 'string',
                  description: '    If \'type\' is \'host\', \'port\', \'ipRange\' or \'localNet\', then \'value\' must be a string, matching either\n    a hostname (e.g. \'somesite.com\'), a port (e.g. 8080), or an IP range (\'192.1.0.0\',\n    \'192.1.0.0/16\', or \'10.1.0.0/16:80\'). \'localNet\' also supports CIDR notation, excluding\n    custom ports.\n     If \'type\' is \'application\' or \'applicationCategory\', then \'value\' must be an object\n    with the structure { \'id\': \'meraki:layer7/...\' }, where \'id\' is the application category or\n    application ID (for a list of IDs for your network, use the trafficShaping/applicationCategories\n    endpoint).\n',
                  default: '<string>'
                }
              }
            }
          },
          perClientBandwidthLimits: {
            settings: '<string>',
            bandwidthLimits: {
              limitUp: '<integer>',
              limitDown: '<integer>'
            }
          },
          dscpTagValue: '<integer>',
          priority: '<string>'
        }
      ]
    };
    describe('#updateTheTrafficShapingSettingsRulesForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheTrafficShapingSettingsRulesForAnMXNetwork('fakedata', rulesUpdateTheTrafficShapingSettingsRulesForAnMXNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'updateTheTrafficShapingSettingsRulesForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#displayTheTrafficShapingSettingsRulesForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.displayTheTrafficShapingSettingsRulesForAnMXNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.defaultRulesEnabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rules', 'displayTheTrafficShapingSettingsRulesForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showUplinkSelectionSettingsForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.showUplinkSelectionSettingsForAnMXNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.activeActiveAutoVpnEnabled);
                assert.equal('wan1', data.response.defaultUplink);
                assert.equal(true, data.response.loadBalancingEnabled);
                assert.equal(true, Array.isArray(data.response.wanTrafficUplinkPreferences));
                assert.equal(true, Array.isArray(data.response.vpnTrafficUplinkPreferences));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UplinkSelection', 'showUplinkSelectionSettingsForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uplinkSelectionUpdateUplinkSelectionSettingsForAnMXNetworkBodyParam = {
      activeActiveAutoVpnEnabled: '<boolean>',
      defaultUplink: '<string>',
      loadBalancingEnabled: '<boolean>',
      wanTrafficUplinkPreferences: [
        {
          trafficFilters: [
            {
              type: '<string>',
              value: {
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>'
                },
                protocol: '<string>'
              }
            },
            {
              type: '<string>',
              value: {
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>'
                },
                protocol: '<string>'
              }
            }
          ],
          preferredUplink: '<string>'
        },
        {
          trafficFilters: [
            {
              type: '<string>',
              value: {
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>'
                },
                protocol: '<string>'
              }
            },
            {
              type: '<string>',
              value: {
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>'
                },
                protocol: '<string>'
              }
            }
          ],
          preferredUplink: '<string>'
        }
      ],
      vpnTrafficUplinkPreferences: [
        {
          trafficFilters: [
            {
              type: '<string>',
              value: {
                id: '<string>',
                protocol: '<string>',
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>',
                  fqdn: '<string>'
                }
              }
            },
            {
              type: '<string>',
              value: {
                id: '<string>',
                protocol: '<string>',
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>',
                  fqdn: '<string>'
                }
              }
            }
          ],
          preferredUplink: '<string>',
          failOverCriterion: '<string>',
          performanceClass: {
            type: '<string>',
            builtinPerformanceClassName: '<string>',
            customPerformanceClassId: '<string>'
          }
        },
        {
          trafficFilters: [
            {
              type: '<string>',
              value: {
                id: '<string>',
                protocol: '<string>',
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>',
                  fqdn: '<string>'
                }
              }
            },
            {
              type: '<string>',
              value: {
                id: '<string>',
                protocol: '<string>',
                source: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>'
                },
                destination: {
                  port: '<string>',
                  cidr: '<string>',
                  network: '<string>',
                  vlan: '<integer>',
                  host: '<integer>',
                  fqdn: '<string>'
                }
              }
            }
          ],
          preferredUplink: '<string>',
          failOverCriterion: '<string>',
          performanceClass: {
            type: '<string>',
            builtinPerformanceClassName: '<string>',
            customPerformanceClassId: '<string>'
          }
        }
      ]
    };
    describe('#updateUplinkSelectionSettingsForAnMXNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateUplinkSelectionSettingsForAnMXNetwork('fakedata', uplinkSelectionUpdateUplinkSelectionSettingsForAnMXNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UplinkSelection', 'updateUplinkSelectionSettingsForAnMXNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAHubBGPConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAHubBGPConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(64515, data.response.asNumber);
                assert.equal(120, data.response.ibgpHoldTimer);
                assert.equal(true, Array.isArray(data.response.neighbors));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgp', 'returnAHubBGPConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bgpUpdateAHubBGPConfigurationBodyParam = {
      enabled: '<boolean>',
      asNumber: '<integer>',
      ibgpHoldTimer: '<integer>',
      neighbors: [
        {
          remoteAsNumber: '<integer>',
          ebgpHoldTimer: '<integer>',
          ebgpMultihop: '<integer>',
          ip: '<string>',
          receiveLimit: '<integer>',
          allowTransit: '<boolean>'
        },
        {
          remoteAsNumber: '<integer>',
          ebgpHoldTimer: '<integer>',
          ebgpMultihop: '<integer>',
          ip: '<string>',
          receiveLimit: '<integer>',
          allowTransit: '<boolean>'
        }
      ]
    };
    describe('#updateAHubBGPConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAHubBGPConfiguration('fakedata', bgpUpdateAHubBGPConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgp', 'updateAHubBGPConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnWarmSpareConfigurationForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnWarmSpareConfigurationForASwitch('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal('Q234-ABCD-0001', data.response.primarySerial);
                assert.equal('Q234-ABCD-0002', data.response.spareSerial);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WarmSpare', 'returnWarmSpareConfigurationForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const warmSpareUpdateWarmSpareConfigurationForASwitchBodyParam = {
      enabled: '<boolean>',
      spareSerial: '<string>'
    };
    describe('#updateWarmSpareConfigurationForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateWarmSpareConfigurationForASwitch('fakedata', warmSpareUpdateWarmSpareConfigurationForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WarmSpare', 'updateWarmSpareConfigurationForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnoverviewstatisticsfornetworkclients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnoverviewstatisticsfornetworkclients('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.counts);
                assert.equal('object', typeof data.response.usages);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Overview', 'returnoverviewstatisticsfornetworkclients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.usage);
                assert.equal('object', typeof data.response.counts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Overview', 'returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnanoverviewofcurrentdevicestatuses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnanoverviewofcurrentdevicestatuses('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.counts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Overview', 'returnanoverviewofcurrentdevicestatuses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsadaptivepolicyaggregatestatisticsforanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsadaptivepolicyaggregatestatisticsforanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.counts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Overview', 'returnsadaptivepolicyaggregatestatisticsforanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnanoverviewofthelicensestateforanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnanoverviewofthelicensestateforanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('OK', data.response.status);
                assert.equal('Feb 8, 2020 UTC', data.response.expirationDate);
                assert.equal('object', typeof data.response.licensedDeviceCounts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Overview', 'returnanoverviewofthelicensestateforanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('History', 'returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomAnalyticsArtifacts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listCustomAnalyticsArtifacts('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Artifacts', 'listCustomAnalyticsArtifacts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const artifactsCreateCustomAnalyticsArtifactBodyParam = {
      name: '<string>'
    };
    describe('#createCustomAnalyticsArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomAnalyticsArtifact('fakedata', artifactsCreateCustomAnalyticsArtifactBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.artifactId);
                assert.equal('2', data.response.organizationId);
                assert.equal('example', data.response.name);
                assert.equal('object', typeof data.response.status);
                assert.equal('00112233445566778899aabbccddeeff', data.response.uploadId);
                assert.equal('https://meraki_custom_cv_upload_url', data.response.uploadUrl);
                assert.equal('2022-01-23T01:23:45.123456+00:00', data.response.uploadUrlExpiry);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Artifacts', 'createCustomAnalyticsArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAnalyticsArtifact - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCustomAnalyticsArtifact('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.artifactId);
                assert.equal('2', data.response.organizationId);
                assert.equal('example', data.response.name);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Artifacts', 'getCustomAnalyticsArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAnalyticsArtifact - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomAnalyticsArtifact('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Artifacts', 'deleteCustomAnalyticsArtifact', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnCustomAnalyticsSettingsForACamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnCustomAnalyticsSettingsForACamera('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal('1', data.response.artifactId);
                assert.equal(true, Array.isArray(data.response.parameters));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomAnalytics', 'returnCustomAnalyticsSettingsForACamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customAnalyticsUpdateCustomAnalyticsSettingsForACameraBodyParam = {
      enabled: '<boolean>',
      artifactId: '<string>',
      parameters: [
        {
          name: '<string>',
          value: '<string>'
        },
        {
          name: '<string>',
          value: '<string>'
        }
      ]
    };
    describe('#updateCustomAnalyticsSettingsForACamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCustomAnalyticsSettingsForACamera('fakedata', customAnalyticsUpdateCustomAnalyticsSettingsForACameraBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomAnalytics', 'updateCustomAnalyticsSettingsForACamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnstheMVSenseobjectdetectionmodellistforthegivencamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnstheMVSenseobjectdetectionmodellistforthegivencamera('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ObjectDetectionModels', 'returnstheMVSenseobjectdetectionmodellistforthegivencamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsSenseSettingsForAGivenCamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsSenseSettingsForAGivenCamera('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.senseEnabled);
                assert.equal('object', typeof data.response.audioDetection);
                assert.equal('1234', data.response.mqttBrokerId);
                assert.equal(true, Array.isArray(data.response.mqttTopics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sense', 'returnsSenseSettingsForAGivenCamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const senseUpdateSenseSettingsForTheGivenCameraBodyParam = {
      senseEnabled: '<boolean>',
      mqttBrokerId: '<string>',
      audioDetection: {
        enabled: '<boolean>'
      },
      detectionModelId: '<string>'
    };
    describe('#updateSenseSettingsForTheGivenCamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSenseSettingsForTheGivenCamera('fakedata', senseUpdateSenseSettingsForTheGivenCameraBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Sense', 'updateSenseSettingsForTheGivenCamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsWirelessProfileAssignedToTheGivenCamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsWirelessProfileAssignedToTheGivenCamera('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ids);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'returnsWirelessProfileAssignedToTheGivenCamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessProfilesAssignWirelessProfilesToTheGivenCameraBodyParam = {
      ids: {
        primary: '<string>',
        secondary: '<string>',
        backup: '<string>'
      }
    };
    describe('#assignWirelessProfilesToTheGivenCamera - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignWirelessProfilesToTheGivenCamera('fakedata', wirelessProfilesAssignWirelessProfilesToTheGivenCameraBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'assignWirelessProfilesToTheGivenCamera', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessProfilesCreatesANewCameraWirelessProfileForThisNetworkBodyParam = {
      name: '<string>',
      ssid: {
        name: '<string>',
        authMode: '<string>',
        encryptionMode: '<string>',
        psk: '<string>'
      },
      identity: {
        username: '<string>',
        password: '<string>'
      }
    };
    describe('#createsANewCameraWirelessProfileForThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsANewCameraWirelessProfileForThisNetwork('fakedata', wirelessProfilesCreatesANewCameraWirelessProfileForThisNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('152', data.response.id);
                assert.equal('wireless profile A', data.response.name);
                assert.equal(0, data.response.appliedDeviceCount);
                assert.equal('object', typeof data.response.ssid);
                assert.equal('object', typeof data.response.identity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'createsANewCameraWirelessProfileForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheCameraWirelessProfilesForThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheCameraWirelessProfilesForThisNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'listTheCameraWirelessProfilesForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveASingleCameraWirelessProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.retrieveASingleCameraWirelessProfile('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('152', data.response.id);
                assert.equal('wireless profile A', data.response.name);
                assert.equal(0, data.response.appliedDeviceCount);
                assert.equal('object', typeof data.response.ssid);
                assert.equal('object', typeof data.response.identity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'retrieveASingleCameraWirelessProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessProfilesUpdateAnExistingCameraWirelessProfileInThisNetworkBodyParam = {
      name: '<string>',
      ssid: {
        name: '<string>',
        authMode: '<string>',
        encryptionMode: '<string>',
        psk: '<string>'
      },
      identity: {
        username: '<string>',
        password: '<string>'
      }
    };
    describe('#updateAnExistingCameraWirelessProfileInThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnExistingCameraWirelessProfileInThisNetwork('fakedata', 'fakedata', wirelessProfilesUpdateAnExistingCameraWirelessProfileInThisNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'updateAnExistingCameraWirelessProfileInThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnExistingCameraWirelessProfileForThisNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnExistingCameraWirelessProfileForThisNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WirelessProfiles', 'deleteAnExistingCameraWirelessProfileForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheOutageScheduleForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheOutageScheduleForTheSSID('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(true, Array.isArray(data.response.ranges));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'listTheOutageScheduleForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesUpdateTheOutageScheduleForTheSSIDBodyParam = {
      enabled: '<boolean>',
      ranges: [
        {
          startDay: '<string>',
          startTime: '<string>',
          endDay: '<string>',
          endTime: '<string>'
        },
        {
          startDay: '<string>',
          startTime: '<string>',
          endDay: '<string>',
          endTime: '<string>'
        }
      ],
      rangesInSeconds: [
        {
          start: '<integer>',
          end: '<integer>'
        },
        {
          start: '<integer>',
          end: '<integer>'
        }
      ]
    };
    describe('#updateTheOutageScheduleForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheOutageScheduleForTheSSID('fakedata', 'fakedata', schedulesUpdateTheOutageScheduleForTheSSIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'updateTheOutageScheduleForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceDHCPConfigurationForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('dhcpServer', data.response.dhcpMode);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal('custom', data.response.dnsNameserversOption);
                assert.equal(true, Array.isArray(data.response.dnsCustomNameservers));
                assert.equal(true, data.response.bootOptionsEnabled);
                assert.equal('1.2.3.4', data.response.bootNextServer);
                assert.equal('home_boot_file', data.response.bootFileName);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal(true, Array.isArray(data.response.fixedIpAssignments));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'returnALayer3InterfaceDHCPConfigurationForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateALayer3InterfaceDHCPConfigurationForASwitchBodyParam = {
      dhcpMode: '<string>',
      dhcpRelayServerIps: [
        '<string>',
        '<string>'
      ],
      dhcpLeaseTime: '<string>',
      dnsNameserversOption: '<string>',
      dnsCustomNameservers: [
        '<string>',
        '<string>'
      ],
      bootOptionsEnabled: '<boolean>',
      bootNextServer: '<string>',
      bootFileName: '<string>',
      dhcpOptions: [
        {
          code: '<string>',
          type: '<string>',
          value: '<string>'
        },
        {
          code: '<string>',
          type: '<string>',
          value: '<string>'
        }
      ],
      reservedIpRanges: [
        {
          start: '<string>',
          end: '<string>',
          comment: '<string>'
        },
        {
          start: '<string>',
          end: '<string>',
          comment: '<string>'
        }
      ],
      fixedIpAssignments: [
        {
          name: '<string>',
          mac: '<string>',
          ip: '<string>'
        },
        {
          name: '<string>',
          mac: '<string>',
          ip: '<string>'
        }
      ]
    };
    describe('#updateALayer3InterfaceDHCPConfigurationForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitch('fakedata', 'fakedata', dhcpUpdateALayer3InterfaceDHCPConfigurationForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateALayer3InterfaceDHCPConfigurationForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceDHCPConfigurationForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('dhcpServer', data.response.dhcpMode);
                assert.equal('1 day', data.response.dhcpLeaseTime);
                assert.equal('custom', data.response.dnsNameserversOption);
                assert.equal(true, Array.isArray(data.response.dnsCustomNameservers));
                assert.equal(true, data.response.bootOptionsEnabled);
                assert.equal('1.2.3.4', data.response.bootNextServer);
                assert.equal('home_boot_file', data.response.bootFileName);
                assert.equal(true, Array.isArray(data.response.dhcpOptions));
                assert.equal(true, Array.isArray(data.response.reservedIpRanges));
                assert.equal(true, Array.isArray(data.response.fixedIpAssignments));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'returnALayer3InterfaceDHCPConfigurationForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpUpdateALayer3InterfaceDHCPConfigurationForASwitchStackBodyParam = {
      dhcpMode: '<string>',
      dhcpRelayServerIps: [
        '<string>',
        '<string>'
      ],
      dhcpLeaseTime: '<string>',
      dnsNameserversOption: '<string>',
      dnsCustomNameservers: [
        '<string>',
        '<string>'
      ],
      bootOptionsEnabled: '<boolean>',
      bootNextServer: '<string>',
      bootFileName: '<string>',
      dhcpOptions: [
        {
          code: '<string>',
          type: '<string>',
          value: '<string>'
        },
        {
          code: '<string>',
          type: '<string>',
          value: '<string>'
        }
      ],
      reservedIpRanges: [
        {
          start: '<string>',
          end: '<string>',
          comment: '<string>'
        },
        {
          start: '<string>',
          end: '<string>',
          comment: '<string>'
        }
      ],
      fixedIpAssignments: [
        {
          name: '<string>',
          mac: '<string>',
          ip: '<string>'
        },
        {
          name: '<string>',
          mac: '<string>',
          ip: '<string>'
        }
      ]
    };
    describe('#updateALayer3InterfaceDHCPConfigurationForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitchStack('fakedata', 'fakedata', 'fakedata', dhcpUpdateALayer3InterfaceDHCPConfigurationForASwitchStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dhcp', 'updateALayer3InterfaceDHCPConfigurationForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheSwitchPortProfilesinanetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheSwitchPortProfilesinanetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'listtheSwitchPortProfilesinanetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallprofilesinanetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listallprofilesinanetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'listallprofilesinanetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const profilesCreateAnOrganizationWideAlertConfigurationBodyParam = {
      type: '<string>',
      alertCondition: {
        duration: '<integer>',
        window: '<integer>',
        bit_rate_bps: '<integer>',
        loss_ratio: '<float>',
        latency_ms: '<integer>',
        jitter_ms: '<integer>',
        mos: '<float>',
        interface: '<string>'
      },
      recipients: {
        emails: [
          '<string>',
          '<string>'
        ],
        httpServerIds: [
          '<string>',
          '<string>'
        ]
      },
      networkTags: [
        '<string>',
        '<string>'
      ],
      description: '<string>'
    };
    describe('#createAnOrganizationWideAlertConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAnOrganizationWideAlertConfiguration('fakedata', profilesCreateAnOrganizationWideAlertConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.id);
                assert.equal('wanUtilization', data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.alertCondition);
                assert.equal('object', typeof data.response.recipients);
                assert.equal(true, Array.isArray(data.response.networkTags));
                assert.equal('WAN 1 high utilization', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'createAnOrganizationWideAlertConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllOrganizationWideAlertConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllOrganizationWideAlertConfigurations('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'listAllOrganizationWideAlertConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removesAnOrganizationWideAlertConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removesAnOrganizationWideAlertConfig('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'removesAnOrganizationWideAlertConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const profilesUpdateAnOrganizationWideAlertConfigBodyParam = {
      enabled: '<boolean>',
      type: '<string>',
      alertCondition: {
        duration: '<integer>',
        window: '<integer>',
        bit_rate_bps: '<integer>',
        loss_ratio: '<float>',
        latency_ms: '<integer>',
        jitter_ms: '<integer>',
        mos: '<float>',
        interface: '<string>'
      },
      recipients: {
        emails: [
          '<string>',
          '<string>'
        ],
        httpServerIds: [
          '<string>',
          '<string>'
        ]
      },
      networkTags: [
        '<string>',
        '<string>'
      ],
      description: '<string>'
    };
    describe('#updateAnOrganizationWideAlertConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnOrganizationWideAlertConfig('fakedata', 'fakedata', profilesUpdateAnOrganizationWideAlertConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Profiles', 'updateAnOrganizationWideAlertConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheswitchportsinanorganizationbyswitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheswitchportsinanorganizationbyswitch('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BySwitch', 'listtheswitchportsinanorganizationbyswitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3InterfacesForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLayer3InterfacesForASwitch('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'listLayer3InterfacesForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesCreateALayer3InterfaceForASwitchBodyParam = {
      name: '<string>',
      subnet: '<string>',
      interfaceIp: '<string>',
      multicastRouting: '<string>',
      vlanId: '<integer>',
      defaultGateway: '<string>',
      ospfSettings: {
        area: '<string>',
        cost: '<integer>',
        isPassiveEnabled: '<boolean>'
      },
      ipv6: {
        assignmentMode: '<string>',
        prefix: '<string>',
        address: '<string>',
        gateway: '<string>'
      }
    };
    describe('#createALayer3InterfaceForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createALayer3InterfaceForASwitch('fakedata', interfacesCreateALayer3InterfaceForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.interfaceId);
                assert.equal('L3 interface', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('disabled', data.response.multicastRouting);
                assert.equal(100, data.response.vlanId);
                assert.equal('192.168.1.1', data.response.defaultGateway);
                assert.equal('object', typeof data.response.ospfSettings);
                assert.equal('object', typeof data.response.ospfV3);
                assert.equal('object', typeof data.response.ipv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'createALayer3InterfaceForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3InterfaceForASwitch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.interfaceId);
                assert.equal('L3 interface', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('disabled', data.response.multicastRouting);
                assert.equal(100, data.response.vlanId);
                assert.equal('192.168.1.1', data.response.defaultGateway);
                assert.equal('object', typeof data.response.ospfSettings);
                assert.equal('object', typeof data.response.ospfV3);
                assert.equal('object', typeof data.response.ipv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'returnALayer3InterfaceForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesUpdateALayer3InterfaceForASwitchBodyParam = {
      name: '<string>',
      subnet: '<string>',
      interfaceIp: '<string>',
      multicastRouting: '<string>',
      vlanId: '<integer>',
      defaultGateway: '<string>',
      ospfSettings: {
        area: '<string>',
        cost: '<integer>',
        isPassiveEnabled: '<boolean>'
      },
      ipv6: {
        assignmentMode: '<string>',
        prefix: '<string>',
        address: '<string>',
        gateway: '<string>'
      }
    };
    describe('#updateALayer3InterfaceForASwitch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3InterfaceForASwitch('fakedata', 'fakedata', interfacesUpdateALayer3InterfaceForASwitchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'updateALayer3InterfaceForASwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3InterfaceFromTheSwitch - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteALayer3InterfaceFromTheSwitch('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'deleteALayer3InterfaceFromTheSwitch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3InterfacesForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listLayer3InterfacesForASwitchStack('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'listLayer3InterfacesForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesCreateALayer3InterfaceForASwitchStackBodyParam = {
      name: '<string>',
      vlanId: '<integer>',
      subnet: '<string>',
      interfaceIp: '<string>',
      multicastRouting: '<string>',
      defaultGateway: '<string>',
      ospfSettings: {
        area: '<string>',
        cost: '<integer>',
        isPassiveEnabled: '<boolean>'
      },
      ipv6: {
        assignmentMode: '<string>',
        prefix: '<string>',
        address: '<string>',
        gateway: '<string>'
      }
    };
    describe('#createALayer3InterfaceForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createALayer3InterfaceForASwitchStack('fakedata', 'fakedata', interfacesCreateALayer3InterfaceForASwitchStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.interfaceId);
                assert.equal('L3 interface', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('disabled', data.response.multicastRouting);
                assert.equal(100, data.response.vlanId);
                assert.equal('192.168.1.1', data.response.defaultGateway);
                assert.equal('object', typeof data.response.ospfSettings);
                assert.equal('object', typeof data.response.ospfV3);
                assert.equal('object', typeof data.response.ipv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'createALayer3InterfaceForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceFromASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnALayer3InterfaceFromASwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.interfaceId);
                assert.equal('L3 interface', data.response.name);
                assert.equal('192.168.1.0/24', data.response.subnet);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('disabled', data.response.multicastRouting);
                assert.equal(100, data.response.vlanId);
                assert.equal('192.168.1.1', data.response.defaultGateway);
                assert.equal('object', typeof data.response.ospfSettings);
                assert.equal('object', typeof data.response.ospfV3);
                assert.equal('object', typeof data.response.ipv6);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'returnALayer3InterfaceFromASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesUpdateALayer3InterfaceForASwitchStackBodyParam = {
      name: '<string>',
      subnet: '<string>',
      interfaceIp: '<string>',
      multicastRouting: '<string>',
      vlanId: '<integer>',
      defaultGateway: '<string>',
      ospfSettings: {
        area: '<string>',
        cost: '<integer>',
        isPassiveEnabled: '<boolean>'
      },
      ipv6: {
        assignmentMode: '<string>',
        prefix: '<string>',
        address: '<string>',
        gateway: '<string>'
      }
    };
    describe('#updateALayer3InterfaceForASwitchStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateALayer3InterfaceForASwitchStack('fakedata', 'fakedata', 'fakedata', interfacesUpdateALayer3InterfaceForASwitchStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'updateALayer3InterfaceForASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3InterfaceFromASwitchStack - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteALayer3InterfaceFromASwitchStack('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'deleteALayer3InterfaceFromASwitchStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMulticastRendezvousPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listMulticastRendezvousPoints('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RendezvousPoints', 'listMulticastRendezvousPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rendezvousPointsCreateAMulticastRendezvousPointBodyParam = {
      interfaceIp: '<string>',
      multicastGroup: '<string>'
    };
    describe('#createAMulticastRendezvousPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAMulticastRendezvousPoint('fakedata', rendezvousPointsCreateAMulticastRendezvousPointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.rendezvousPointId);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('192.168.128.0/24', data.response.multicastGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RendezvousPoints', 'createAMulticastRendezvousPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAMulticastRendezvousPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAMulticastRendezvousPoint('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.rendezvousPointId);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('l3_interface_0', data.response.interfaceName);
                assert.equal('192.168.1.2', data.response.interfaceIp);
                assert.equal('Any', data.response.multicastGroup);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RendezvousPoints', 'returnAMulticastRendezvousPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAMulticastRendezvousPoint - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAMulticastRendezvousPoint('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RendezvousPoints', 'deleteAMulticastRendezvousPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rendezvousPointsUpdateAMulticastRendezvousPointBodyParam = {
      interfaceIp: '<string>',
      multicastGroup: '<string>'
    };
    describe('#updateAMulticastRendezvousPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAMulticastRendezvousPoint('fakedata', 'fakedata', rendezvousPointsUpdateAMulticastRendezvousPointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RendezvousPoints', 'updateAMulticastRendezvousPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnMulticastSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnMulticastSettingsForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.defaultSettings);
                assert.equal(true, Array.isArray(data.response.overrides));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'returnMulticastSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastUpdateMulticastSettingsForANetworkBodyParam = {
      defaultSettings: {
        igmpSnoopingEnabled: '<boolean>',
        floodUnknownMulticastTrafficEnabled: '<boolean>'
      },
      overrides: [
        {
          igmpSnoopingEnabled: '<boolean>',
          floodUnknownMulticastTrafficEnabled: '<boolean>',
          switchProfiles: [
            '<string>',
            '<string>'
          ],
          switches: [
            '<string>',
            '<string>'
          ],
          stacks: [
            '<string>',
            '<string>'
          ]
        },
        {
          igmpSnoopingEnabled: '<boolean>',
          floodUnknownMulticastTrafficEnabled: '<boolean>',
          switchProfiles: [
            '<string>',
            '<string>'
          ],
          switches: [
            '<string>',
            '<string>'
          ],
          stacks: [
            '<string>',
            '<string>'
          ]
        }
      ]
    };
    describe('#updateMulticastSettingsForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateMulticastSettingsForANetwork('fakedata', multicastUpdateMulticastSettingsForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'updateMulticastSettingsForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnLayer3OSPFRoutingConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnLayer3OSPFRoutingConfiguration('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(10, data.response.helloTimerInSeconds);
                assert.equal(40, data.response.deadTimerInSeconds);
                assert.equal(true, Array.isArray(data.response.areas));
                assert.equal('object', typeof data.response.v3);
                assert.equal(true, data.response.md5AuthenticationEnabled);
                assert.equal('object', typeof data.response.md5AuthenticationKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'returnLayer3OSPFRoutingConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ospfUpdateLayer3OSPFRoutingConfigurationBodyParam = {
      enabled: '<boolean>',
      helloTimerInSeconds: '<integer>',
      deadTimerInSeconds: '<integer>',
      areas: [
        {
          areaId: '<string>',
          areaName: '<string>',
          areaType: '<string>'
        },
        {
          areaId: '<string>',
          areaName: '<string>',
          areaType: '<string>'
        }
      ],
      v3: {
        enabled: '<boolean>',
        helloTimerInSeconds: '<integer>',
        deadTimerInSeconds: '<integer>',
        areas: [
          {
            areaId: '<string>',
            areaName: '<string>',
            areaType: '<string>'
          },
          {
            areaId: '<string>',
            areaName: '<string>',
            areaType: '<string>'
          }
        ]
      },
      md5AuthenticationEnabled: '<boolean>',
      md5AuthenticationKey: {
        id: '<integer>',
        passphrase: '<string>'
      }
    };
    describe('#updateLayer3OSPFRoutingConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateLayer3OSPFRoutingConfiguration('fakedata', ospfUpdateLayer3OSPFRoutingConfigurationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'updateLayer3OSPFRoutingConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSwitchAlternateManagementInterfaceForTheNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheSwitchAlternateManagementInterfaceForTheNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(100, data.response.vlanId);
                assert.equal(true, Array.isArray(data.response.protocols));
                assert.equal(true, Array.isArray(data.response.switches));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlternateManagementInterface', 'returnTheSwitchAlternateManagementInterfaceForTheNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alternateManagementInterfaceUpdateTheSwitchAlternateManagementInterfaceForTheNetworkBodyParam = {
      enabled: '<boolean>',
      vlanId: '<integer>',
      protocols: [
        '<string>',
        '<string>'
      ],
      switches: [
        {
          serial: '<string>',
          alternateManagementIp: '<string>',
          subnetMask: '<string>',
          gateway: '<string>'
        },
        {
          serial: '<string>',
          alternateManagementIp: '<string>',
          subnetMask: '<string>',
          gateway: '<string>'
        }
      ]
    };
    describe('#updateTheSwitchAlternateManagementInterfaceForTheNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheSwitchAlternateManagementInterfaceForTheNetwork('fakedata', alternateManagementInterfaceUpdateTheSwitchAlternateManagementInterfaceForTheNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlternateManagementInterface', 'updateTheSwitchAlternateManagementInterfaceForTheNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAlternateManagementInterfaceAndDevicesWithIPAssigned - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAlternateManagementInterfaceAndDevicesWithIPAssigned('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(100, data.response.vlanId);
                assert.equal(true, Array.isArray(data.response.protocols));
                assert.equal(true, Array.isArray(data.response.accessPoints));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlternateManagementInterface', 'returnAlternateManagementInterfaceAndDevicesWithIPAssigned', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alternateManagementInterfaceUpdateAlternateManagementInterfaceAndDeviceStaticIPBodyParam = {
      enabled: '<boolean>',
      vlanId: '<integer>',
      protocols: [
        '<string>',
        '<string>'
      ],
      accessPoints: [
        {
          serial: '<string>',
          alternateManagementIp: '<string>',
          subnetMask: '<string>',
          gateway: '<string>',
          dns1: '<string>',
          dns2: '<string>'
        },
        {
          serial: '<string>',
          alternateManagementIp: '<string>',
          subnetMask: '<string>',
          gateway: '<string>',
          dns1: '<string>',
          dns2: '<string>'
        }
      ]
    };
    describe('#updateAlternateManagementInterfaceAndDeviceStaticIP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAlternateManagementInterfaceAndDeviceStaticIP('fakedata', alternateManagementInterfaceUpdateAlternateManagementInterfaceAndDeviceStaticIPBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlternateManagementInterface', 'updateAlternateManagementInterfaceAndDeviceStaticIP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Seen', 'returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedServers', 'returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedServersAddAServerToBeTrustedByDynamicARPInspectionOnThisNetworkBodyParam = {
      mac: '<string>',
      vlan: '<integer>',
      ipv4: {
        address: '<string>'
      }
    };
    describe('#addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork('fakedata', trustedServersAddAServerToBeTrustedByDynamicARPInspectionOnThisNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('123', data.response.trustedServerId);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal(100, data.response.vlan);
                assert.equal('object', typeof data.response.ipv4);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedServers', 'addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const trustedServersUpdateAServerThatIsTrustedByDynamicARPInspectionOnThisNetworkBodyParam = {
      mac: '<string>',
      vlan: '<integer>',
      ipv4: {
        address: '<string>'
      }
    };
    describe('#updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork('fakedata', 'fakedata', trustedServersUpdateAServerThatIsTrustedByDynamicARPInspectionOnThisNetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedServers', 'updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedServers', 'removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByDevice', 'returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthepowerstatusinformationfordevicesinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listthepowerstatusinformationfordevicesinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByDevice', 'listthepowerstatusinformationfordevicesinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthecurrentuplinkaddressesfordevicesinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listthecurrentuplinkaddressesfordevicesinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByDevice', 'listthecurrentuplinkaddressesfordevicesinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const devicesCloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitchesBodyParam = {
      sourceSerial: '<string>',
      targetSerials: [
        '<string>',
        '<string>'
      ]
    };
    describe('#cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches('fakedata', devicesCloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitchesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Q234-ABCD-5678', data.response.sourceSerial);
                assert.equal(true, Array.isArray(data.response.targetSerials));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASingleDeviceFromTheInventoryOfAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnASingleDeviceFromTheInventoryOfAnOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('My AP', data.response.name);
                assert.equal('MR34', data.response.model);
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('4C1234567', data.response.orderNumber);
                assert.equal('2018-02-11T00:00:00.090210Z', data.response.claimedAt);
                assert.equal('2020-05-02T10:52:44.012345Z', data.response.licenseExpirationDate);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('wireless', data.response.productType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Devices', 'returnASingleDeviceFromTheInventoryOfAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBillingSettingsOfThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnTheBillingSettingsOfThisNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('USD', data.response.currency);
                assert.equal(true, Array.isArray(data.response.plans));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'returnTheBillingSettingsOfThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const billingUpdateTheBillingSettingsBodyParam = {
      currency: '<string>',
      plans: [
        {
          price: '<float>',
          bandwidthLimits: {
            limitUp: '<integer>',
            limitDown: '<integer>'
          },
          timeLimit: '<string>',
          id: '<string>'
        },
        {
          price: '<float>',
          bandwidthLimits: {
            limitUp: '<integer>',
            limitDown: '<integer>'
          },
          timeLimit: '<string>',
          id: '<string>'
        }
      ]
    };
    describe('#updateTheBillingSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheBillingSettings('fakedata', billingUpdateTheBillingSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Billing', 'updateTheBillingSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheBonjourForwardingSettingAndRulesForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheBonjourForwardingSettingAndRulesForTheSSID('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BonjourForwarding', 'listTheBonjourForwardingSettingAndRulesForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bonjourForwardingUpdateTheBonjourForwardingSettingAndRulesForTheSSIDBodyParam = {
      enabled: '<boolean>',
      rules: [
        {
          vlanId: '<string>',
          services: [
            '<string>',
            '<string>'
          ],
          description: '<string>'
        },
        {
          vlanId: '<string>',
          services: [
            '<string>',
            '<string>'
          ],
          description: '<string>'
        }
      ]
    };
    describe('#updateTheBonjourForwardingSettingAndRulesForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheBonjourForwardingSettingAndRulesForTheSSID('fakedata', 'fakedata', bonjourForwardingUpdateTheBonjourForwardingSettingAndRulesForTheSSIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BonjourForwarding', 'updateTheBonjourForwardingSettingAndRulesForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllIdentityPSKsInAWirelessNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAllIdentityPSKsInAWirelessNetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPsks', 'listAllIdentityPSKsInAWirelessNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityPsksCreateAnIdentityPSKBodyParam = {
      name: '<string>',
      groupPolicyId: '<string>',
      passphrase: '<string>'
    };
    describe('#createAnIdentityPSK - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAnIdentityPSK('fakedata', 'fakedata', identityPsksCreateAnIdentityPSKBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.id);
                assert.equal('Sample Identity PSK', data.response.name);
                assert.equal('NIalareK', data.response.passphrase);
                assert.equal('101', data.response.groupPolicyId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPsks', 'createAnIdentityPSK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnIdentityPSK - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAnIdentityPSK('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Identity PSK Name', data.response.name);
                assert.equal('1284392014819', data.response.id);
                assert.equal('1284392014819', data.response.groupPolicyId);
                assert.equal('secret', data.response.passphrase);
                assert.equal('1284392014819', data.response.wifiPersonalNetworkId);
                assert.equal('miles@meraki.com', data.response.email);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPsks', 'returnAnIdentityPSK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const identityPsksUpdateAnIdentityPSKBodyParam = {
      name: '<string>',
      passphrase: '<string>',
      groupPolicyId: '<string>'
    };
    describe('#updateAnIdentityPSK - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnIdentityPSK('fakedata', 'fakedata', 'fakedata', identityPsksUpdateAnIdentityPSKBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPsks', 'updateAnIdentityPSK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnIdentityPSK - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnIdentityPSK('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IdentityPsks', 'deleteAnIdentityPSK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheVPNSettingsForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheVPNSettingsForTheSSID('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.concentrator);
                assert.equal('object', typeof data.response.failover);
                assert.equal('object', typeof data.response.splitTunnel);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vpn', 'listTheVPNSettingsForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vpnUpdateTheVPNSettingsForTheSSIDBodyParam = {
      concentrator: {
        networkId: '<string>',
        vlanId: '<integer>'
      },
      splitTunnel: {
        enabled: '<boolean>',
        rules: [
          {
            destCidr: '<string>',
            policy: '<string>',
            protocol: '<string>',
            destPort: '<string>',
            comment: '<string>'
          },
          {
            destCidr: '<string>',
            policy: '<string>',
            protocol: '<string>',
            destPort: '<string>',
            comment: '<string>'
          }
        ]
      },
      failover: {
        requestIp: '<string>',
        heartbeatInterval: '<integer>',
        idleTimeout: '<integer>'
      }
    };
    describe('#updateTheVPNSettingsForTheSSID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheVPNSettingsForTheSSID('fakedata', 'fakedata', vpnUpdateTheVPNSettingsForTheSSIDBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vpn', 'updateTheVPNSettingsForTheSSID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPchannelutilizationovertimeforadeviceornetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAPchannelutilizationovertimeforadeviceornetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChannelUtilizationHistory', 'returnAPchannelutilizationovertimeforadeviceornetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ClientCountHistory', 'returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConnectivityEvents', 'listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LatencyHistory', 'returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DataRateHistory', 'returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listwirelessmeshstatusesforrepeaters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listwirelessmeshstatusesforrepeaters('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal(true, Array.isArray(data.response.meshRoute));
                assert.equal('object', typeof data.response.latestMeshPerformance);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MeshStatuses', 'listwirelessmeshstatusesforrepeaters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SignalQualityHistory', 'returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getapplicationhealthbytime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getapplicationhealthbytime('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HealthByTime', 'getapplicationhealthbytime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallInsighttrackedapplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listallInsighttrackedapplications('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Applications', 'listallInsighttrackedapplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Latest', 'returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTrustedAccessConfigs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTrustedAccessConfigs('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TrustedAccessConfigs', 'listTrustedAccessConfigs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserAccessDevicesAndItsTrustedAccessConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUserAccessDevicesAndItsTrustedAccessConnections('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAccessDevices', 'listUserAccessDevicesAndItsTrustedAccessConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAUserAccessDevice - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAUserAccessDevice('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAccessDevices', 'deleteAUserAccessDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gettheorganizationSAPNScertificate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gettheorganizationSAPNScertificate('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('          -----BEGIN CERTIFICATE-----\n          MIIFdjCCBF6gAwIBAgIIM/hhf5ww8MwwDQYJKoZIhvcNAQELBQAwgYwxQDA+BgNV\n          BAMMN0FwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIDIgQ2VydGlmaWNhdGlv\n          biBBdXRob3JpdHkxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9y\n          aXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0yMDAyMjYx\n          ODIzNDJaFw0yMTAyMjUxODIzNDJaMIGPMUwwSgYKCZImiZPyLGQBAQw8Y29tLmFw\n          cGxlLm1nbXQuRXh0ZXJuYWwuOTA3NDJhYmYtZDhhZC00MTc2LTllZmQtMGNiMzg1\n          MTM1MGM0MTIwMAYDVQQDDClBUFNQOjkwNzQyYWJmLWQ4YWQtNDE3Ni05ZWZkLTBj\n          YjM4NTEzNTBjNDELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw\n          ggEKAoIBAQDANdpo62hfxkP1IpMPXuO1+xKekUkY+iYae6cRaP886bodUaH1OwCj\n          Qd011u9Vng6m8I9rcLdIOS+IkFGKcTAHRYY3noqfEQUPyi5TN6yM1/mVYVoWZUnY\n          TrNWqDN/HfRagdYfZyQ7kAtOY2K8TF78HLLqQm7ez2+r4qibumoSli9+qCzKwDW/\n          hbx7JTeMlbYkhLTFgBkRxlp+usKymsLKm8D7kdbxtct4mx6p9z1FiNu4U1Hi/PgK\n          I/V3zHD4Ww7SzTICiLdCPeAmt042JvXAMQi0qhzrEdDiapmWwUC9xiiORN0BTIRA\n          T+DddTx8Xcly4wj9vQFdGUGLrJnzB3xZAgMBAAGjggHVMIIB0TAJBgNVHRMEAjAA\n          MB8GA1UdIwQYMBaAFPe+fCFgkds9G3vYOjKBad+ebH+bMIIBHAYDVR0gBIIBEzCC\n          AQ8wggELBgkqhkiG92NkBQEwgf0wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ug\n          b24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRh\n          bmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNv\n          bmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmlj\n          YXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNQYIKwYBBQUHAgEWKWh0dHA6Ly93\n          d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5MBMGA1UdJQQMMAoGCCsG\n          AQUFBwMCMDAGA1UdHwQpMCcwJaAjoCGGH2h0dHA6Ly9jcmwuYXBwbGUuY29tL2Fh\n          aTJjYS5jcmwwHQYDVR0OBBYEFDj4Jizt9bQX7dn3ypIanvaNIy8fMAsGA1UdDwQE\n          AwIHgDAQBgoqhkiG92NkBgMCBAIFADANBgkqhkiG9w0BAQsFAAOCAQEARmLmy4Mh\n          80hTBHMj2whrC2LR0dIe2ngAUwYGSocyPZOzlGZYntUvpsNGwflbWSPNxFpVF15z\n          exEcLPKM4f9KGdM27s/m/x1Es2us9Vve+wS+N0C84zMC++FJBIxj3yAINXqSpYJv\n          bA5wccHlzP9F9Ks7sVNQB8y0mibYahtxVV959gC4522t5SRaEEsd82oTCtXE2Ljg\n          fQ1IAmWi4MuMSPwp26oDSwun8Wxyx+sfi/it9YWxD36Ga9mrfIjK1WIHyhge0HHr\n          olnvMfxgwI9E5gGV/4bQzPlmsHdz+/pLupWMkALaAxI9D7ajUG7iyyjJBCOpsr1s\n          FjDvo6WUkaqMHA==\n          -----END CERTIFICATE-----\n', data.response.certificate);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApnsCert', 'gettheorganizationSAPNScertificate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheVPPAccountsInTheOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheVPPAccountsInTheOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VppAccounts', 'listTheVPPAccountsInTheOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.id);
                assert.equal('eyJleHBEYXRlIjoiMjAyMS0wMi0yNVQxNjo1NToyMy0wODAwIiwidG9rZW4iOiJBZFN4YWxyQmhFK0FhSWUrYTJDbDE1aDg1N0ZkUDkrL2c5c00xUWhlMG1WS0ZKWkdOL0hxRnQxeitSVERSTkNLNkxhMEExellML3JFa2tpOG1pbk8xRDlJWHhZQnBERy8wZllhTE44UlUrMDhYZkZsNlVwenkrRE8wQW41ZGgxWncwSmZiYmlXU21VVG94TFJmV1BCcVE9PSIsIm9yZ05hbWUiOiJNZXJha2kgTExDICsgRGV2ZWxvcGVyIn0=', data.response.vppServiceToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VppAccounts', 'getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pingEnqueueAJobToPingATargetHostFromTheDeviceBodyParam = {
      target: '<string>',
      count: '<integer>'
    };
    describe('#enqueueAJobToPingATargetHostFromTheDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enqueueAJobToPingATargetHostFromTheDevice('fakedata', pingEnqueueAJobToPingATargetHostFromTheDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.pingId);
                assert.equal('/devices/SERIAL/liveTools/ping/1284392014819', data.response.url);
                assert.equal('object', typeof data.response.request);
                assert.equal('complete', data.response.status);
                assert.equal('object', typeof data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ping', 'enqueueAJobToPingATargetHostFromTheDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPingJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAPingJob('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.pingId);
                assert.equal('/devices/SERIAL/liveTools/ping/1284392014819', data.response.url);
                assert.equal('object', typeof data.response.request);
                assert.equal('complete', data.response.status);
                assert.equal('object', typeof data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ping', 'returnAPingJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pingDeviceEnqueueAJobToCheckConnectivityStatusToTheDeviceBodyParam = {
      count: '<integer>'
    };
    describe('#enqueueAJobToCheckConnectivityStatusToTheDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enqueueAJobToCheckConnectivityStatusToTheDevice('fakedata', pingDeviceEnqueueAJobToCheckConnectivityStatusToTheDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.pingId);
                assert.equal('/devices/SERIAL/liveTools/ping/1284392014819', data.response.url);
                assert.equal('object', typeof data.response.request);
                assert.equal('complete', data.response.status);
                assert.equal('object', typeof data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PingDevice', 'enqueueAJobToCheckConnectivityStatusToTheDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPingDeviceJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAPingDeviceJob('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1284392014819', data.response.pingId);
                assert.equal('/devices/SERIAL/liveTools/ping/1284392014819', data.response.url);
                assert.equal('object', typeof data.response.request);
                assert.equal('complete', data.response.status);
                assert.equal('object', typeof data.response.results);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PingDevice', 'returnAPingDeviceJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const claimClaimavMXintoanetworkBodyParam = {
      size: '<string>'
    };
    describe('#claimavMXintoanetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.claimavMXintoanetwork('fakedata', claimClaimavMXintoanetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('My AP', data.response.name);
                assert.equal(37.4180951010362, data.response.lat);
                assert.equal(-122.098531723022, data.response.lng);
                assert.equal('Q234-ABCD-5678', data.response.serial);
                assert.equal('00:11:22:33:44:55', data.response.mac);
                assert.equal('VMX-S', data.response.model);
                assert.equal('1600 Pennsylvania Ave', data.response.address);
                assert.equal('My AP\'s note', data.response.notes);
                assert.equal('1.2.3.4', data.response.lanIp);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('N_24329156', data.response.networkId);
                assert.equal('object', typeof data.response.beaconIdParams);
                assert.equal('wireless-25-14', data.response.firmware);
                assert.equal('g_1234567', data.response.floorPlanId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Claim', 'claimavMXintoanetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rollbacksRollbackaFirmwareUpgradeForANetworkBodyParam = {
      reasons: [
        {
          category: '<string>',
          comment: '<string>'
        },
        {
          category: '<string>',
          comment: '<string>'
        }
      ],
      product: '<string>',
      time: '<dateTime>',
      toVersion: {
        id: '<string>'
      }
    };
    describe('#rollbackaFirmwareUpgradeForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rollbackaFirmwareUpgradeForANetwork('fakedata', rollbacksRollbackaFirmwareUpgradeForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('switch', data.response.product);
                assert.equal('pending', data.response.status);
                assert.equal('23456', data.response.upgradeBatchId);
                assert.equal('2020-10-21T02:00:00Z', data.response.time);
                assert.equal('object', typeof data.response.toVersion);
                assert.equal(true, Array.isArray(data.response.reasons));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rollbacks', 'rollbackaFirmwareUpgradeForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirmwareUpgradeInformationForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getFirmwareUpgradeInformationForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.upgradeWindow);
                assert.equal('America/Los_Angeles', data.response.timezone);
                assert.equal('object', typeof data.response.products);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirmwareUpgrades', 'getFirmwareUpgradeInformationForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const firmwareUpgradesUpdateFirmwareUpgradeInformationForANetworkBodyParam = {
      upgradeWindow: {
        dayOfWeek: '<string>',
        hourOfDay: '<string>'
      },
      timezone: '<string>',
      products: {
        wireless: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        },
        appliance: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        },
        switch: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        },
        camera: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        },
        cellularGateway: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        },
        sensor: {
          nextUpgrade: {
            time: '<string>',
            toVersion: {
              id: '<string>'
            }
          },
          participateInNextBetaRelease: '<boolean>'
        }
      }
    };
    describe('#updateFirmwareUpgradeInformationForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateFirmwareUpgradeInformationForANetwork('fakedata', firmwareUpgradesUpdateFirmwareUpgradeInformationForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FirmwareUpgrades', 'updateFirmwareUpgradeInformationForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnallglobalalertsonthisnetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnallglobalalertsonthisnetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.id);
                assert.equal('Connectivity', data.response.category);
                assert.equal('Poor connectivity to the Meraki cloud', data.response.type);
                assert.equal('warning', data.response.severity);
                assert.equal('object', typeof data.response.scope);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alerts', 'returnallglobalalertsonthisnetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const merakiAuthUsersAuthorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCapBodyParam = {
      email: '<string>',
      authorizations: [
        {
          ssidNumber: '<integer>',
          expiresAt: 'Never'
        },
        {
          ssidNumber: '<integer>',
          expiresAt: 'Never'
        }
      ],
      name: '<string>',
      password: '<string>',
      accountType: '802.1X',
      emailPasswordToUser: '<boolean>',
      isAdmin: '<boolean>'
    };
    describe('#authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap('fakedata', merakiAuthUsersAuthorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCapBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('aGlAaGkuY29t', data.response.id);
                assert.equal('miles@meraki.com', data.response.email);
                assert.equal('Miles Meraki', data.response.name);
                assert.equal('2018-02-11T00:00:00.090210Z', data.response.createdAt);
                assert.equal('802.1X', data.response.accountType);
                assert.equal(false, data.response.isAdmin);
                assert.equal(true, Array.isArray(data.response.authorizations));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MerakiAuthUsers', 'authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deauthorizeAUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deauthorizeAUser('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MerakiAuthUsers', 'deauthorizeAUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const merakiAuthUsersUpdateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdatedBodyParam = {
      name: '<string>',
      password: '<string>',
      emailPasswordToUser: '<boolean>',
      authorizations: [
        {
          ssidNumber: '<integer>',
          expiresAt: 'Never'
        },
        {
          ssidNumber: '<integer>',
          expiresAt: 'Never'
        }
      ]
    };
    describe('#updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated('fakedata', 'fakedata', merakiAuthUsersUpdateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdatedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MerakiAuthUsers', 'updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mqttBrokersAddAnMQTTBrokerBodyParam = {
      name: '<string>',
      host: '<string>',
      port: '<integer>',
      security: {
        mode: '<string>',
        security: {
          caCertificate: '<string>',
          verifyHostnames: '<boolean>'
        }
      },
      authentication: {}
    };
    describe('#addAnMQTTBroker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAnMQTTBroker('fakedata', mqttBrokersAddAnMQTTBrokerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.id);
                assert.equal('MQTT_Broker_1', data.response.name);
                assert.equal('1.1.1.1', data.response.host);
                assert.equal(1234, data.response.port);
                assert.equal('object', typeof data.response.security);
                assert.equal('object', typeof data.response.authentication);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MqttBrokers', 'addAnMQTTBroker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheMQTTBrokersForThisNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheMQTTBrokersForThisNetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MqttBrokers', 'listTheMQTTBrokersForThisNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnMQTTBroker - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnMQTTBroker('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MqttBrokers', 'deleteAnMQTTBroker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnMQTTBroker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAnMQTTBroker('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.id);
                assert.equal('MQTT_Broker_1', data.response.name);
                assert.equal('1.1.1.1', data.response.host);
                assert.equal(1234, data.response.port);
                assert.equal('object', typeof data.response.security);
                assert.equal('object', typeof data.response.authentication);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MqttBrokers', 'returnAnMQTTBroker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mqttBrokersUpdateAnMQTTBrokerBodyParam = {
      name: '<string>',
      host: '<string>',
      port: '<integer>',
      security: {
        mode: '<string>',
        security: {
          caCertificate: '<string>',
          verifyHostnames: '<boolean>'
        }
      },
      authentication: {}
    };
    describe('#updateAnMQTTBroker - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnMQTTBroker('fakedata', 'fakedata', mqttBrokersUpdateAnMQTTBrokerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MqttBrokers', 'updateAnMQTTBroker', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpoliciesforallclientswithpolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getpoliciesforallclientswithpolicies('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByClient', 'getpoliciesforallclientswithpolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listofdevicesandconnectionsamongthemwithinthenetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listofdevicesandconnectionsamongthemwithinthenetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.nodes));
                assert.equal(true, Array.isArray(data.response.links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkLayer', 'listofdevicesandconnectionsamongthemwithinthenetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const payloadTemplatesCreateAWebhookPayloadTemplateForANetworkBodyParam = {
      name: '<string>',
      body: '<string>',
      headers: '<string>',
      bodyFile: '<byte>',
      headersFile: '<byte>'
    };
    describe('#createAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createAWebhookPayloadTemplateForANetwork('fakedata', payloadTemplatesCreateAWebhookPayloadTemplateForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('wpt_343', data.response.payloadTemplateId);
                assert.equal('custom', data.response.type);
                assert.equal('Weeb Hooks', data.response.name);
                assert.equal('{"event_type":"{{alertTypeId}}","client_payload":{"text":"{{alertData}}"}}', data.response.body);
                assert.equal('object', typeof data.response.headers);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PayloadTemplates', 'createAWebhookPayloadTemplateForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheWebhookPayloadTemplatesForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheWebhookPayloadTemplatesForANetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PayloadTemplates', 'listTheWebhookPayloadTemplatesForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#destroyAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.destroyAWebhookPayloadTemplateForANetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PayloadTemplates', 'destroyAWebhookPayloadTemplateForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheWebhookPayloadTemplateForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTheWebhookPayloadTemplateForANetwork('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('wpt_343', data.response.payloadTemplateId);
                assert.equal('custom', data.response.type);
                assert.equal('Weeb Hooks', data.response.name);
                assert.equal('{"event_type":"{{alertTypeId}}","client_payload":{"text":"{{alertData}}"}}', data.response.body);
                assert.equal('object', typeof data.response.headers);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PayloadTemplates', 'getTheWebhookPayloadTemplateForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const payloadTemplatesUpdateAWebhookPayloadTemplateForANetworkBodyParam = {
      name: '<string>',
      body: '<string>',
      headers: '<string>',
      bodyFile: '<byte>',
      headersFile: '<byte>'
    };
    describe('#updateAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAWebhookPayloadTemplateForANetwork('fakedata', 'fakedata', payloadTemplatesUpdateAWebhookPayloadTemplateForANetworkBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PayloadTemplates', 'updateAWebhookPayloadTemplateForANetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheapplicationusagedataforclients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returntheapplicationusagedataforclients('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationUsage', 'returntheapplicationusagedataforclients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthUsageHistory', 'returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BandwidthUsageHistory', 'returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheusagehistoriesforclients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returntheusagehistoriesforclients('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UsageHistories', 'returntheusagehistoriesforclients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getthechannelutilizationovereachradioforallAPsinanetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getthechannelutilizationovereachradioforallAPsinanetwork('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ChannelUtilization', 'getthechannelutilizationovereachradioforallAPsinanetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclsCreatesNewAdaptivePolicyACLBodyParam = {
      name: '<string>',
      rules: [
        {
          policy: '<string>',
          protocol: '<string>',
          srcPort: '<string>',
          dstPort: '<string>'
        },
        {
          policy: '<string>',
          protocol: '<string>',
          srcPort: '<string>',
          dstPort: '<string>'
        }
      ],
      ipVersion: '<string>',
      description: ''
    };
    describe('#createsNewAdaptivePolicyACL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsNewAdaptivePolicyACL('fakedata', aclsCreatesNewAdaptivePolicyACLBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('Block sensitive web traffic', data.response.name);
                assert.equal('Blocks sensitive web traffic', data.response.description);
                assert.equal('ipv6', data.response.ipVersion);
                assert.equal(true, Array.isArray(data.response.rules));
                assert.equal(123, data.response.id);
                assert.equal('2021-05-19T17:08:25Z', data.response.createdAt);
                assert.equal('2021-05-19T17:11:54Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acls', 'createsNewAdaptivePolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePolicyACLsInAOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAdaptivePolicyACLsInAOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acls', 'listAdaptivePolicyACLsInAOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesTheSpecifiedAdaptivePolicyACL - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyACL('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acls', 'deletesTheSpecifiedAdaptivePolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheAdaptivePolicyACLInformation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsTheAdaptivePolicyACLInformation('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('12345678', data.response.aclId);
                assert.equal('Block sensitive web traffic', data.response.name);
                assert.equal('Blocks sensitive web traffic', data.response.description);
                assert.equal('ipv6', data.response.ipVersion);
                assert.equal(true, Array.isArray(data.response.rules));
                assert.equal('2021-05-19T17:08:25Z', data.response.createdAt);
                assert.equal('2021-05-19T17:11:54Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acls', 'returnsTheAdaptivePolicyACLInformation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aclsUpdatesAnAdaptivePolicyACLBodyParam = {
      name: '<string>',
      description: '<string>',
      rules: [
        {
          policy: '<string>',
          protocol: '<string>',
          srcPort: '<string>',
          dstPort: '<string>'
        },
        {
          policy: '<string>',
          protocol: '<string>',
          srcPort: '<string>',
          dstPort: '<string>'
        }
      ],
      ipVersion: '<string>'
    };
    describe('#updatesAnAdaptivePolicyACL - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesAnAdaptivePolicyACL('fakedata', 'fakedata', aclsUpdatesAnAdaptivePolicyACLBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Acls', 'updatesAnAdaptivePolicyACL', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsCreatesANewAdaptivePolicyGroupBodyParam = {
      name: '<string>',
      sgt: '<integer>',
      description: '<string>',
      policyObjects: [
        {
          id: '<string>',
          name: '<string>'
        },
        {
          id: '<string>',
          name: '<string>'
        }
      ]
    };
    describe('#createsANewAdaptivePolicyGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createsANewAdaptivePolicyGroup('fakedata', groupsCreatesANewAdaptivePolicyGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.groupId);
                assert.equal('Employee Group', data.response.name);
                assert.equal(1000, data.response.sgt);
                assert.equal('Group of XYZ Corp Employees', data.response.description);
                assert.equal(true, Array.isArray(data.response.policyObjects));
                assert.equal(false, data.response.isDefaultGroup);
                assert.equal(true, Array.isArray(data.response.requiredIpMappings));
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.createdAt);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'createsANewAdaptivePolicyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePolicyGroupsInAOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAdaptivePolicyGroupsInAOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'listAdaptivePolicyGroupsInAOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsAnAdaptivePolicyGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsAnAdaptivePolicyGroup('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1234', data.response.groupId);
                assert.equal('Employee Group', data.response.name);
                assert.equal(1000, data.response.sgt);
                assert.equal('Group of XYZ Corp Employees', data.response.description);
                assert.equal(true, Array.isArray(data.response.policyObjects));
                assert.equal(false, data.response.isDefaultGroup);
                assert.equal(true, Array.isArray(data.response.requiredIpMappings));
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.createdAt);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'returnsAnAdaptivePolicyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsUpdatesAnAdaptivePolicyGroupBodyParam = {
      name: '<string>',
      sgt: '<integer>',
      description: '<string>',
      policyObjects: [
        {
          id: '<string>',
          name: '<string>'
        },
        {
          id: '<string>',
          name: '<string>'
        }
      ]
    };
    describe('#updatesAnAdaptivePolicyGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesAnAdaptivePolicyGroup('fakedata', 'fakedata', groupsUpdatesAnAdaptivePolicyGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'updatesAnAdaptivePolicyGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesAddAnAdaptivePolicyBodyParam = {
      sourceGroup: {
        id: '<string>',
        name: '<string>',
        sgt: '<integer>'
      },
      destinationGroup: {
        id: '<string>',
        name: '<string>',
        sgt: '<integer>'
      },
      acls: [
        {
          id: '<string>',
          name: '<string>'
        },
        {
          id: '<string>',
          name: '<string>'
        }
      ],
      lastEntryRule: '<string>'
    };
    describe('#addAnAdaptivePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addAnAdaptivePolicy('fakedata', policiesAddAnAdaptivePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('111', data.response.adaptivePolicyId);
                assert.equal('object', typeof data.response.sourceGroup);
                assert.equal('object', typeof data.response.destinationGroup);
                assert.equal(true, Array.isArray(data.response.acls));
                assert.equal('allow', data.response.lastEntryRule);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.createdAt);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'addAnAdaptivePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePoliciesInAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listAdaptivePoliciesInAnOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'listAdaptivePoliciesInAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAdaptivePolicy - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnAdaptivePolicy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'deleteAnAdaptivePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnAdaptivePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnAnAdaptivePolicy('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('111', data.response.adaptivePolicyId);
                assert.equal('object', typeof data.response.sourceGroup);
                assert.equal('object', typeof data.response.destinationGroup);
                assert.equal(true, Array.isArray(data.response.acls));
                assert.equal('allow', data.response.lastEntryRule);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.createdAt);
                assert.equal('2019-06-27T21:34:25.253480Z', data.response.updatedAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'returnAnAdaptivePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const policiesUpdateAnAdaptivePolicyBodyParam = {
      sourceGroup: {
        id: '<string>',
        name: '<string>',
        sgt: '<integer>'
      },
      destinationGroup: {
        id: '<string>',
        name: '<string>',
        sgt: '<integer>'
      },
      acls: [
        {
          id: '<string>',
          name: '<string>'
        },
        {
          id: '<string>',
          name: '<string>'
        }
      ],
      lastEntryRule: '<string>'
    };
    describe('#updateAnAdaptivePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnAdaptivePolicy('fakedata', 'fakedata', policiesUpdateAnAdaptivePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Policies', 'updateAnAdaptivePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheclientdetailsinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returntheclientdetailsinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('k74272e', data.response.clientId);
                assert.equal('22:33:44:55:66:77', data.response.mac);
                assert.equal('Apple', data.response.manufacturer);
                assert.equal(true, Array.isArray(data.response.records));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Search', 'returntheclientdetailsinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configTemplatesCreateANewConfigurationTemplateBodyParam = {
      name: '<string>',
      timeZone: '<string>',
      copyFromNetworkId: '<string>'
    };
    describe('#createANewConfigurationTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createANewConfigurationTemplate('fakedata', configTemplatesCreateANewConfigurationTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('N_24329156', data.response.id);
                assert.equal('My config template', data.response.name);
                assert.equal(true, Array.isArray(data.response.productTypes));
                assert.equal('America/Los_Angeles', data.response.timeZone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigTemplates', 'createANewConfigurationTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASingleConfigurationTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnASingleConfigurationTemplate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('N_24329156', data.response.id);
                assert.equal('My config template', data.response.name);
                assert.equal(true, Array.isArray(data.response.productTypes));
                assert.equal('America/Los_Angeles', data.response.timeZone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigTemplates', 'returnASingleConfigurationTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configTemplatesUpdateAConfigurationTemplateBodyParam = {
      name: '<string>',
      timeZone: '<string>'
    };
    describe('#updateAConfigurationTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAConfigurationTemplate('fakedata', 'fakedata', configTemplatesUpdateAConfigurationTemplateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigTemplates', 'updateAConfigurationTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const optInsCreateANewEarlyAccessFeatureOptInForAnOrganizationBodyParam = {
      shortName: '<string>',
      limitScopeToNetworks: [
        '<string>',
        '<string>'
      ]
    };
    describe('#createANewEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createANewEarlyAccessFeatureOptInForAnOrganization('fakedata', optInsCreateANewEarlyAccessFeatureOptInForAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.id);
                assert.equal('has_new_feature', data.response.shortName);
                assert.equal(true, Array.isArray(data.response.limitScopeToNetworks));
                assert.equal('2022-05-01T04:07:15Z', data.response.createdAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OptIns', 'createANewEarlyAccessFeatureOptInForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheEarlyAccessFeatureOptInsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheEarlyAccessFeatureOptInsForAnOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OptIns', 'listTheEarlyAccessFeatureOptInsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnEarlyAccessFeatureOptIn - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteAnEarlyAccessFeatureOptIn('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OptIns', 'deleteAnEarlyAccessFeatureOptIn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAnEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.showAnEarlyAccessFeatureOptInForAnOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('1', data.response.id);
                assert.equal('has_new_feature', data.response.shortName);
                assert.equal(true, Array.isArray(data.response.limitScopeToNetworks));
                assert.equal('2022-05-01T04:07:15Z', data.response.createdAt);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OptIns', 'showAnEarlyAccessFeatureOptInForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const optInsUpdateAnEarlyAccessFeatureOptInForAnOrganizationBodyParam = {
      limitScopeToNetworks: [
        '<string>',
        '<string>'
      ]
    };
    describe('#updateAnEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateAnEarlyAccessFeatureOptInForAnOrganization('fakedata', 'fakedata', optInsUpdateAnEarlyAccessFeatureOptInForAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OptIns', 'updateAnEarlyAccessFeatureOptInForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheAvailableEarlyAccessFeaturesForOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheAvailableEarlyAccessFeaturesForOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Features', 'listTheAvailableEarlyAccessFeaturesForOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryClaimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventoryBodyParam = {
      orders: [
        '<string>',
        '<string>'
      ],
      serials: [
        '<string>',
        '<string>'
      ],
      licenses: [
        {
          key: '<string>',
          mode: '<string>'
        },
        {
          key: '<string>',
          mode: '<string>'
        }
      ]
    };
    describe('#claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory('fakedata', inventoryClaimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventoryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.orders));
                assert.equal(true, Array.isArray(data.response.serials));
                assert.equal(true, Array.isArray(data.response.licenses));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryReleaseAListOfClaimedDevicesFromAnOrganizationBodyParam = {
      serials: [
        '<string>',
        '<string>'
      ]
    };
    describe('#releaseAListOfClaimedDevicesFromAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.releaseAListOfClaimedDevicesFromAnOrganization('fakedata', inventoryReleaseAListOfClaimedDevicesFromAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.serials));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'releaseAListOfClaimedDevicesFromAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheLoginSecuritySettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsTheLoginSecuritySettingsForAnOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enforcePasswordExpiration);
                assert.equal(90, data.response.passwordExpirationDays);
                assert.equal(true, data.response.enforceDifferentPasswords);
                assert.equal(3, data.response.numDifferentPasswords);
                assert.equal(true, data.response.enforceStrongPasswords);
                assert.equal(true, data.response.enforceAccountLockout);
                assert.equal(3, data.response.accountLockoutAttempts);
                assert.equal(true, data.response.enforceIdleTimeout);
                assert.equal(30, data.response.idleTimeoutMinutes);
                assert.equal(true, data.response.enforceTwoFactorAuth);
                assert.equal(true, data.response.enforceLoginIpRanges);
                assert.equal(true, Array.isArray(data.response.loginIpRanges));
                assert.equal('object', typeof data.response.apiAuthentication);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSecurity', 'returnsTheLoginSecuritySettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loginSecurityUpdateTheLoginSecuritySettingsForAnOrganizationBodyParam = {
      enforcePasswordExpiration: '<boolean>',
      passwordExpirationDays: '<integer>',
      enforceDifferentPasswords: '<boolean>',
      numDifferentPasswords: '<integer>',
      enforceStrongPasswords: '<boolean>',
      enforceAccountLockout: '<boolean>',
      accountLockoutAttempts: '<integer>',
      enforceIdleTimeout: '<boolean>',
      idleTimeoutMinutes: '<integer>',
      enforceTwoFactorAuth: '<boolean>',
      enforceLoginIpRanges: '<boolean>',
      loginIpRanges: [
        '<string>',
        '<string>'
      ],
      apiAuthentication: {
        ipRestrictionsForKeys: {
          enabled: '<boolean>',
          ranges: [
            '<string>',
            '<string>'
          ]
        }
      }
    };
    describe('#updateTheLoginSecuritySettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateTheLoginSecuritySettingsForAnOrganization('fakedata', loginSecurityUpdateTheLoginSecuritySettingsForAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSecurity', 'updateTheLoginSecuritySettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const idpsCreateASAMLIdPForYourOrganizationBodyParam = {
      x509certSha1Fingerprint: '<string>',
      sloLogoutUrl: '<string>'
    };
    describe('#createASAMLIdPForYourOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createASAMLIdPForYourOrganization('fakedata', idpsCreateASAMLIdPForYourOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ab0c1de23Fg', data.response.idpId);
                assert.equal('https://n7.meraki.com/saml/login/XXX', data.response.consumerUrl);
                assert.equal('00:11:22:33:44:55:66:77:88:99:00:11:22:33:44:55:66:77:88:99', data.response.x509certSha1Fingerprint);
                assert.equal('https://somewhere.com', data.response.sloLogoutUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idps', 'createASAMLIdPForYourOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheSAMLIdPsInYourOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listTheSAMLIdPsInYourOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idps', 'listTheSAMLIdPsInYourOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASAMLIdPFromYourOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getASAMLIdPFromYourOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('ab0c1de23Fg', data.response.idpId);
                assert.equal('https://n7.meraki.com/saml/login/XXX', data.response.consumerUrl);
                assert.equal('00:11:22:33:44:55:66:77:88:99:00:11:22:33:44:55:66:77:88:99', data.response.x509certSha1Fingerprint);
                assert.equal('https://somewhere.com', data.response.sloLogoutUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idps', 'getASAMLIdPFromYourOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeASAMLIdPInYourOrganization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeASAMLIdPInYourOrganization('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-meraki-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idps', 'removeASAMLIdPInYourOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const idpsUpdateASAMLIdPInYourOrganizationBodyParam = {
      x509certSha1Fingerprint: '<string>',
      sloLogoutUrl: '<string>'
    };
    describe('#updateASAMLIdPInYourOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateASAMLIdPInYourOrganization('fakedata', 'fakedata', idpsUpdateASAMLIdPInYourOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idps', 'updateASAMLIdPInYourOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheSAMLSSOEnabledSettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnsTheSAMLSSOEnabledSettingsForAnOrganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.enabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Saml', 'returnsTheSAMLSSOEnabledSettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const samlUpdatesTheSAMLSSOEnabledSettingsForAnOrganizationBodyParam = {
      enabled: '<boolean>'
    };
    describe('#updatesTheSAMLSSOEnabledSettingsForAnOrganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updatesTheSAMLSSOEnabledSettingsForAnOrganization('fakedata', samlUpdatesTheSAMLSSOEnabledSettingsForAnOrganizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Saml', 'updatesTheSAMLSSOEnabledSettingsForAnOrganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheavailabilityinformationfordevicesinanorganization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listtheavailabilityinformationfordevicesinanorganization('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Availabilities', 'listtheavailabilityinformationfordevicesinanorganization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthetop10appliancessortedbyutilizationovergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnthetop10appliancessortedbyutilizationovergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUtilization', 'returnthetop10appliancessortedbyutilizationovergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUsage', 'returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUsage', 'returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUsage', 'returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUsage', 'returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByUsage', 'returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ByEnergyUsage', 'returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnalistofalerttypestobeusedwithmanagingwebhookalerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.returnalistofalerttypestobeusedwithmanagingwebhookalerts('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AlertTypes', 'returnalistofalerttypestobeusedwithmanagingwebhookalerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
