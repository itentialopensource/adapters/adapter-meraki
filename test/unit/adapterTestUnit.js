/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-meraki',
      type: 'Meraki',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Meraki = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Meraki Adapter Test', () => {
  describe('Meraki Class Tests', () => {
    const a = new Meraki(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('meraki'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('meraki'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Meraki', pronghornDotJson.export);
          assert.equal('Meraki', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-meraki', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('meraki'));
          assert.equal('Meraki', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-meraki', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-meraki', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createOrganizationActionBatch - errors', () => {
      it('should have a createOrganizationActionBatch function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationActionBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createOrganizationActionBatch(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationActionBatches - errors', () => {
      it('should have a getOrganizationActionBatches function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationActionBatches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationActionBatches(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationActionBatches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationActionBatch - errors', () => {
      it('should have a getOrganizationActionBatch function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationActionBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationActionBatch(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationActionBatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationActionBatch - errors', () => {
      it('should have a deleteOrganizationActionBatch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganizationActionBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteOrganizationActionBatch(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteOrganizationActionBatch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationActionBatch - errors', () => {
      it('should have a updateOrganizationActionBatch function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationActionBatch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationActionBatch(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOrganizationActionBatch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationActionBatch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationAdmins - errors', () => {
      it('should have a getOrganizationAdmins function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationAdmins === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationAdmins(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationAdmins', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationAdmin - errors', () => {
      it('should have a createOrganizationAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createOrganizationAdmin(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createOrganizationAdmin', (done) => {
        try {
          a.createOrganizationAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'createOrganizationAdmin is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationAdmin - errors', () => {
      it('should have a updateOrganizationAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationAdmin(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOrganizationAdmin('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationAdmin - errors', () => {
      it('should have a deleteOrganizationAdmin function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganizationAdmin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteOrganizationAdmin(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteOrganizationAdmin('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationAdmin', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAlertSettings - errors', () => {
      it('should have a getNetworkAlertSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAlertSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkAlertSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAlertSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkAlertSettings - errors', () => {
      it('should have a updateNetworkAlertSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkAlertSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkAlertSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkAlertSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsOverview - errors', () => {
      it('should have a getDeviceCameraAnalyticsOverview function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsOverview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsOverview(null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsOverview', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsOverviewV2 - errors', () => {
      it('should have a getDeviceCameraAnalyticsOverviewV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsOverviewV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsOverviewV2(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsOverviewV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZones - errors', () => {
      it('should have a getDeviceCameraAnalyticsZones function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsZones(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZoneHistory - errors', () => {
      it('should have a getDeviceCameraAnalyticsZoneHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsZoneHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistory(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsZoneHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistory('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsZoneHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsZoneHistoryV2 - errors', () => {
      it('should have a getDeviceCameraAnalyticsZoneHistoryV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsZoneHistoryV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistoryV2(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsZoneHistoryV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneId', (done) => {
        try {
          a.getDeviceCameraAnalyticsZoneHistoryV2('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'zoneId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsZoneHistoryV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsRecent - errors', () => {
      it('should have a getDeviceCameraAnalyticsRecent function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsRecent === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsRecent(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsRecent', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsRecentV2 - errors', () => {
      it('should have a getDeviceCameraAnalyticsRecentV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsRecentV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsRecentV2(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsRecentV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCameraAnalyticsLive - errors', () => {
      it('should have a getDeviceCameraAnalyticsLive function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCameraAnalyticsLive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCameraAnalyticsLive(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCameraAnalyticsLive', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationApiRequests - errors', () => {
      it('should have a getOrganizationApiRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationApiRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationApiRequests(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationApiRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationApiRequestsV2 - errors', () => {
      it('should have a getOrganizationApiRequestsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationApiRequestsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationApiRequestsV2(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationApiRequestsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClient - errors', () => {
      it('should have a getNetworkBluetoothClient function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBluetoothClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkBluetoothClient(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkBluetoothClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bluetoothClientId', (done) => {
        try {
          a.getNetworkBluetoothClient('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'bluetoothClientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkBluetoothClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClients - errors', () => {
      it('should have a getNetworkBluetoothClients function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBluetoothClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkBluetoothClients(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkBluetoothClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothClientsV2 - errors', () => {
      it('should have a getNetworkBluetoothClientsV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBluetoothClientsV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkBluetoothClientsV2(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkBluetoothClientsV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBluetoothSettings - errors', () => {
      it('should have a getNetworkBluetoothSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBluetoothSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkBluetoothSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkBluetoothSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkBluetoothSettings - errors', () => {
      it('should have a updateNetworkBluetoothSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkBluetoothSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkBluetoothSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkBluetoothSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationNetworks - errors', () => {
      it('should have a getOrganizationNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationNetworks(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationNetwork - errors', () => {
      it('should have a createOrganizationNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createOrganizationNetwork(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createOrganizationNetwork', (done) => {
        try {
          a.createOrganizationNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'createOrganizationNetwork is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetwork - errors', () => {
      it('should have a getNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetwork - errors', () => {
      it('should have a updateNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should have a deleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bindNetwork - errors', () => {
      it('should have a bindNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.bindNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.bindNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-bindNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bindNetwork', (done) => {
        try {
          a.bindNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'bindNetwork is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-bindNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unbindNetwork - errors', () => {
      it('should have a unbindNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.unbindNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.unbindNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-unbindNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTraffic - errors', () => {
      it('should have a getNetworkTraffic function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTraffic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTraffic(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTraffic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timespan', (done) => {
        try {
          a.getNetworkTraffic('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'timespan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTraffic', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficV2 - errors', () => {
      it('should have a getNetworkTrafficV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTrafficV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTrafficV2(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTrafficV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAccessPolicies - errors', () => {
      it('should have a getNetworkAccessPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAccessPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkAccessPolicies(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAccessPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAirMarshal - errors', () => {
      it('should have a getNetworkAirMarshal function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAirMarshal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkAirMarshal(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAirMarshal', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#combineOrganizationNetworks - errors', () => {
      it('should have a combineOrganizationNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.combineOrganizationNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.combineOrganizationNetworks(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-combineOrganizationNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing combineOrganizationNetworks', (done) => {
        try {
          a.combineOrganizationNetworks('fakeparam', null, (data, error) => {
            try {
              const displayE = 'combineOrganizationNetworks is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-combineOrganizationNetworks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#splitNetwork - errors', () => {
      it('should have a splitNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.splitNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.splitNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-splitNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSiteToSiteVpn - errors', () => {
      it('should have a getNetworkSiteToSiteVpn function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSiteToSiteVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSiteToSiteVpn(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSiteToSiteVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSiteToSiteVpn - errors', () => {
      it('should have a updateNetworkSiteToSiteVpn function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSiteToSiteVpn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSiteToSiteVpn(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSiteToSiteVpn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraVideoLink - errors', () => {
      it('should have a getNetworkCameraVideoLink function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCameraVideoLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCameraVideoLink(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCameraVideoLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkCameraVideoLink('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCameraVideoLink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateNetworkCameraSnapshot - errors', () => {
      it('should have a generateNetworkCameraSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.generateNetworkCameraSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.generateNetworkCameraSnapshot(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-generateNetworkCameraSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.generateNetworkCameraSnapshot('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-generateNetworkCameraSnapshot', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClient - errors', () => {
      it('should have a getNetworkClient function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClient(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClient('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionNetworkClients - errors', () => {
      it('should have a provisionNetworkClients function', (done) => {
        try {
          assert.equal(true, typeof a.provisionNetworkClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.provisionNetworkClients(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-provisionNetworkClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientUsageHistory - errors', () => {
      it('should have a getNetworkClientUsageHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientUsageHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientUsageHistory(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientUsageHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientUsageHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientUsageHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientPolicy - errors', () => {
      it('should have a getNetworkClientPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientPolicy(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkClientPolicy - errors', () => {
      it('should have a updateNetworkClientPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkClientPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkClientPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkClientPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.updateNetworkClientPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkClientPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientSplashAuthorizationStatus - errors', () => {
      it('should have a getNetworkClientSplashAuthorizationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientSplashAuthorizationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientSplashAuthorizationStatus(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientSplashAuthorizationStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientSplashAuthorizationStatus('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientSplashAuthorizationStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkClientSplashAuthorizationStatus - errors', () => {
      it('should have a updateNetworkClientSplashAuthorizationStatus function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkClientSplashAuthorizationStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkClientSplashAuthorizationStatus(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkClientSplashAuthorizationStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.updateNetworkClientSplashAuthorizationStatus('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkClientSplashAuthorizationStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClients - errors', () => {
      it('should have a getNetworkClients function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClients(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceClients - errors', () => {
      it('should have a getDeviceClients function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceClients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceClients(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceClients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientTrafficHistory - errors', () => {
      it('should have a getNetworkClientTrafficHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientTrafficHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientTrafficHistory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientTrafficHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientTrafficHistory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientTrafficHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientEvents - errors', () => {
      it('should have a getNetworkClientEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientEvents(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientEvents('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientLatencyHistory - errors', () => {
      it('should have a getNetworkClientLatencyHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientLatencyHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientLatencyHistory(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientLatencyHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientLatencyHistory('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientLatencyHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationConfigTemplates - errors', () => {
      it('should have a getOrganizationConfigTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationConfigTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationConfigTemplates(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationConfigTemplates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationConfigTemplate - errors', () => {
      it('should have a deleteOrganizationConfigTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganizationConfigTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteOrganizationConfigTemplate(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationConfigTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteOrganizationConfigTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationConfigTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLldpCdp - errors', () => {
      it('should have a getNetworkDeviceLldpCdp function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLldpCdp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceLldpCdp(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLldpCdp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLldpCdp('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLldpCdp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLldpCdpV1 - errors', () => {
      it('should have a getNetworkDeviceLldpCdpV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLldpCdpV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLldpCdpV1(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLldpCdpV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDevices - errors', () => {
      it('should have a getOrganizationDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationDevices(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevices - errors', () => {
      it('should have a getNetworkDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDevices(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevice - errors', () => {
      it('should have a getNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceV1 - errors', () => {
      it('should have a getNetworkDeviceV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDevice - errors', () => {
      it('should have a updateNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateNetworkDevice('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicePerformance - errors', () => {
      it('should have a getNetworkDevicePerformance function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevicePerformance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDevicePerformance(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevicePerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDevicePerformance('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevicePerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicePerformanceV1 - errors', () => {
      it('should have a getNetworkDevicePerformanceV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevicePerformanceV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDevicePerformanceV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevicePerformanceV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceUplink - errors', () => {
      it('should have a getNetworkDeviceUplink function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceUplink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceUplink(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceUplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceUplink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceUplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimNetworkDevices - errors', () => {
      it('should have a claimNetworkDevices function', (done) => {
        try {
          assert.equal(true, typeof a.claimNetworkDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.claimNetworkDevices(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimNetworkDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing claimNetworkDevices', (done) => {
        try {
          a.claimNetworkDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'claimNetworkDevices is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimNetworkDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNetworkDevice - errors', () => {
      it('should have a removeNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.removeNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.removeNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.removeNetworkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLossAndLatencyHistory - errors', () => {
      it('should have a getNetworkDeviceLossAndLatencyHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLossAndLatencyHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistory(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLossAndLatencyHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistory('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLossAndLatencyHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLossAndLatencyHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLossAndLatencyHistoryV1 - errors', () => {
      it('should have a getNetworkDeviceLossAndLatencyHistoryV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLossAndLatencyHistoryV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistoryV1(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLossAndLatencyHistoryV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.getNetworkDeviceLossAndLatencyHistoryV1('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLossAndLatencyHistoryV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootNetworkDevice - errors', () => {
      it('should have a rebootNetworkDevice function', (done) => {
        try {
          assert.equal(true, typeof a.rebootNetworkDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.rebootNetworkDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-rebootNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.rebootNetworkDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-rebootNetworkDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootNetworkDeviceV1 - errors', () => {
      it('should have a rebootNetworkDeviceV1 function', (done) => {
        try {
          assert.equal(true, typeof a.rebootNetworkDeviceV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.rebootNetworkDeviceV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-rebootNetworkDeviceV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blinkNetworkDeviceLeds - errors', () => {
      it('should have a blinkNetworkDeviceLeds function', (done) => {
        try {
          assert.equal(true, typeof a.blinkNetworkDeviceLeds === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.blinkNetworkDeviceLeds(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-blinkNetworkDeviceLeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.blinkNetworkDeviceLeds('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-blinkNetworkDeviceLeds', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#blinkNetworkDeviceLedsV1 - errors', () => {
      it('should have a blinkNetworkDeviceLedsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.blinkNetworkDeviceLedsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.blinkNetworkDeviceLedsV1(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-blinkNetworkDeviceLedsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularFirewallRules - errors', () => {
      it('should have a getNetworkCellularFirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCellularFirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCellularFirewallRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCellularFirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularFirewallRules - errors', () => {
      it('should have a updateNetworkCellularFirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCellularFirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCellularFirewallRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCellularFirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL3FirewallRules - errors', () => {
      it('should have a getNetworkL3FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkL3FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkL3FirewallRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkL3FirewallRules - errors', () => {
      it('should have a updateNetworkL3FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkL3FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkL3FirewallRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL7FirewallRulesApplicationCategories - errors', () => {
      it('should have a getNetworkL7FirewallRulesApplicationCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkL7FirewallRulesApplicationCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkL7FirewallRulesApplicationCategories(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkL7FirewallRulesApplicationCategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkL7FirewallRules - errors', () => {
      it('should have a getNetworkL7FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkL7FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkL7FirewallRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkL7FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkL7FirewallRules - errors', () => {
      it('should have a updateNetworkL7FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkL7FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkL7FirewallRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkL7FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationVpnFirewallRules - errors', () => {
      it('should have a getOrganizationVpnFirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationVpnFirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationVpnFirewallRules(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationVpnFirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationVpnFirewallRules - errors', () => {
      it('should have a updateOrganizationVpnFirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationVpnFirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationVpnFirewallRules(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationVpnFirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidL3FirewallRules - errors', () => {
      it('should have a getNetworkSsidL3FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsidL3FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsidL3FirewallRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getNetworkSsidL3FirewallRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidL3FirewallRules - errors', () => {
      it('should have a updateNetworkSsidL3FirewallRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSsidL3FirewallRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSsidL3FirewallRules(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateNetworkSsidL3FirewallRules('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidL3FirewallRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupPolicies - errors', () => {
      it('should have a getNetworkGroupPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkGroupPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkGroupPolicies(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkGroupPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkGroupPolicy - errors', () => {
      it('should have a createNetworkGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkGroupPolicy(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createNetworkGroupPolicy', (done) => {
        try {
          a.createNetworkGroupPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'createNetworkGroupPolicy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkGroupPolicy - errors', () => {
      it('should have a getNetworkGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkGroupPolicy(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPolicyId', (done) => {
        try {
          a.getNetworkGroupPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkGroupPolicy - errors', () => {
      it('should have a updateNetworkGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkGroupPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPolicyId', (done) => {
        try {
          a.updateNetworkGroupPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkGroupPolicy - errors', () => {
      it('should have a deleteNetworkGroupPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkGroupPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkGroupPolicy(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPolicyId', (done) => {
        try {
          a.deleteNetworkGroupPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkGroupPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidHotspot20 - errors', () => {
      it('should have a getNetworkSsidHotspot20 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsidHotspot20 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsidHotspot20(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidHotspot20', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getNetworkSsidHotspot20('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidHotspot20', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidHotspot20 - errors', () => {
      it('should have a updateNetworkSsidHotspot20 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSsidHotspot20 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSsidHotspot20(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidHotspot20', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateNetworkSsidHotspot20('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidHotspot20', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServers - errors', () => {
      it('should have a getNetworkHttpServers function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkHttpServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkHttpServers(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkHttpServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkHttpServer - errors', () => {
      it('should have a createNetworkHttpServer function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkHttpServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkHttpServer(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServer - errors', () => {
      it('should have a getNetworkHttpServer function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkHttpServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkHttpServer(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkHttpServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkHttpServer - errors', () => {
      it('should have a updateNetworkHttpServer function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkHttpServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkHttpServer(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateNetworkHttpServer('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkHttpServer - errors', () => {
      it('should have a deleteNetworkHttpServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkHttpServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkHttpServer(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteNetworkHttpServer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkHttpServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkHttpServersWebhookTest - errors', () => {
      it('should have a createNetworkHttpServersWebhookTest function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkHttpServersWebhookTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkHttpServersWebhookTest(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkHttpServersWebhookTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkHttpServersWebhookTest - errors', () => {
      it('should have a getNetworkHttpServersWebhookTest function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkHttpServersWebhookTest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkHttpServersWebhookTest(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkHttpServersWebhookTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkHttpServersWebhookTest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkHttpServersWebhookTest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceManagementInterfaceSettings - errors', () => {
      it('should have a getNetworkDeviceManagementInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceManagementInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceManagementInterfaceSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceManagementInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceManagementInterfaceSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceManagementInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceManagementInterfaceSettingsV1 - errors', () => {
      it('should have a getNetworkDeviceManagementInterfaceSettingsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceManagementInterfaceSettingsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceManagementInterfaceSettingsV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceManagementInterfaceSettingsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceManagementInterfaceSettings - errors', () => {
      it('should have a updateNetworkDeviceManagementInterfaceSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDeviceManagementInterfaceSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkDeviceManagementInterfaceSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceManagementInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateNetworkDeviceManagementInterfaceSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceManagementInterfaceSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceManagementInterfaceSettingsV1 - errors', () => {
      it('should have a updateNetworkDeviceManagementInterfaceSettingsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDeviceManagementInterfaceSettingsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateNetworkDeviceManagementInterfaceSettingsV1(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceManagementInterfaceSettingsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkMerakiAuthUsers - errors', () => {
      it('should have a getNetworkMerakiAuthUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkMerakiAuthUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkMerakiAuthUsers(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkMerakiAuthUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkMerakiAuthUser - errors', () => {
      it('should have a getNetworkMerakiAuthUser function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkMerakiAuthUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkMerakiAuthUser(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkMerakiAuthUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkMerakiAuthUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkMerakiAuthUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationOpenapiSpec - errors', () => {
      it('should have a getOrganizationOpenapiSpec function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationOpenapiSpec === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationOpenapiSpec(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationOpenapiSpec', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizations - errors', () => {
      it('should have a getOrganizations function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganization - errors', () => {
      it('should have a createOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganization - errors', () => {
      it('should have a getOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganization(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganization - errors', () => {
      it('should have a updateOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOrganization(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneOrganization - errors', () => {
      it('should have a cloneOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.cloneOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.cloneOrganization(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-cloneOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicenseState - errors', () => {
      it('should have a getOrganizationLicenseState function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationLicenseState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationLicenseState(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationLicenseState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationInventory - errors', () => {
      it('should have a getOrganizationInventory function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationInventory(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationInventory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationInventoryV2 - errors', () => {
      it('should have a getOrganizationInventoryV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationInventoryV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationInventoryV2(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationInventoryV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationDeviceStatuses - errors', () => {
      it('should have a getOrganizationDeviceStatuses function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationDeviceStatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationDeviceStatuses(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationDeviceStatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSnmp - errors', () => {
      it('should have a getOrganizationSnmp function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationSnmp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationSnmp(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSnmp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSnmp - errors', () => {
      it('should have a updateOrganizationSnmp function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationSnmp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOrganizationSnmp(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationSnmp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimOrganization - errors', () => {
      it('should have a claimOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.claimOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.claimOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationUplinksLossAndLatency - errors', () => {
      it('should have a getOrganizationUplinksLossAndLatency function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationUplinksLossAndLatency === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationUplinksLossAndLatency(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationUplinksLossAndLatency', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationUplinksLossAndLatencyV2 - errors', () => {
      it('should have a getOrganizationUplinksLossAndLatencyV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationUplinksLossAndLatencyV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationUplinksLossAndLatencyV2(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationUplinksLossAndLatencyV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationThirdPartyVPNPeers - errors', () => {
      it('should have a getOrganizationThirdPartyVPNPeers function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationThirdPartyVPNPeers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationThirdPartyVPNPeers(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationThirdPartyVPNPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationThirdPartyVPNPeers - errors', () => {
      it('should have a updateOrganizationThirdPartyVPNPeers function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationThirdPartyVPNPeers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationThirdPartyVPNPeers(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationThirdPartyVPNPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateOrganizationThirdPartyVPNPeers', (done) => {
        try {
          a.updateOrganizationThirdPartyVPNPeers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateOrganizationThirdPartyVPNPeers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationThirdPartyVPNPeers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiPiiKeys - errors', () => {
      it('should have a getNetworkPiiPiiKeys function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPiiPiiKeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPiiPiiKeys(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiPiiKeys', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiSmDevicesForKey - errors', () => {
      it('should have a getNetworkPiiSmDevicesForKey function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPiiSmDevicesForKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPiiSmDevicesForKey(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiSmDevicesForKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiSmOwnersForKey - errors', () => {
      it('should have a getNetworkPiiSmOwnersForKey function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPiiSmOwnersForKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPiiSmOwnersForKey(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiSmOwnersForKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiRequests - errors', () => {
      it('should have a getNetworkPiiRequests function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPiiRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPiiRequests(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiRequests', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkPiiRequest - errors', () => {
      it('should have a createNetworkPiiRequest function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkPiiRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkPiiRequest(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkPiiRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPiiRequest - errors', () => {
      it('should have a getNetworkPiiRequest function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPiiRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPiiRequest(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.getNetworkPiiRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPiiRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkPiiRequest - errors', () => {
      it('should have a deleteNetworkPiiRequest function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkPiiRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkPiiRequest(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkPiiRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestId', (done) => {
        try {
          a.deleteNetworkPiiRequest('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkPiiRequest', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceWirelessRadioSettings - errors', () => {
      it('should have a getNetworkDeviceWirelessRadioSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceWirelessRadioSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceWirelessRadioSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceWirelessRadioSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceWirelessRadioSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceWirelessRadioSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceWirelessRadioSettingsV1 - errors', () => {
      it('should have a getNetworkDeviceWirelessRadioSettingsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceWirelessRadioSettingsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceWirelessRadioSettingsV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceWirelessRadioSettingsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceWirelessRadioSettings - errors', () => {
      it('should have a updateNetworkDeviceWirelessRadioSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDeviceWirelessRadioSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkDeviceWirelessRadioSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceWirelessRadioSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateNetworkDeviceWirelessRadioSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceWirelessRadioSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkDeviceWirelessRadioSettingsV1 - errors', () => {
      it('should have a updateNetworkDeviceWirelessRadioSettingsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkDeviceWirelessRadioSettingsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateNetworkDeviceWirelessRadioSettingsV1(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkDeviceWirelessRadioSettingsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWirelessRfProfiles - errors', () => {
      it('should have a getNetworkWirelessRfProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkWirelessRfProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkWirelessRfProfiles(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkWirelessRfProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSamlRoles - errors', () => {
      it('should have a getOrganizationSamlRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationSamlRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationSamlRoles(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSamlRoles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationSamlRole - errors', () => {
      it('should have a createOrganizationSamlRole function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationSamlRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createOrganizationSamlRole(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSamlRole - errors', () => {
      it('should have a getOrganizationSamlRole function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationSamlRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationSamlRole(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getOrganizationSamlRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSamlRole - errors', () => {
      it('should have a updateOrganizationSamlRole function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationSamlRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationSamlRole(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateOrganizationSamlRole('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationSamlRole - errors', () => {
      it('should have a deleteOrganizationSamlRole function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganizationSamlRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteOrganizationSamlRole(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteOrganizationSamlRole('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationSamlRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientSecurityEvents - errors', () => {
      it('should have a getNetworkClientSecurityEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientSecurityEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientSecurityEvents(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientSecurityEvents('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing perPage', (done) => {
        try {
          a.getNetworkClientSecurityEvents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'perPage is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityEvents - errors', () => {
      it('should have a getNetworkSecurityEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSecurityEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSecurityEvents(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing perPage', (done) => {
        try {
          a.getNetworkSecurityEvents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'perPage is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSecurityEvents - errors', () => {
      it('should have a getOrganizationSecurityEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationSecurityEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationSecurityEvents(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing perPage', (done) => {
        try {
          a.getOrganizationSecurityEvents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'perPage is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSecurityEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityIntrusionSettings - errors', () => {
      it('should have a getNetworkSecurityIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSecurityIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSecurityIntrusionSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSecurityIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSecurityIntrusionSettings - errors', () => {
      it('should have a updateNetworkSecurityIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSecurityIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSecurityIntrusionSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSecurityIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationSecurityIntrusionSettings - errors', () => {
      it('should have a getOrganizationSecurityIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationSecurityIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationSecurityIntrusionSettings(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationSecurityIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationSecurityIntrusionSettings - errors', () => {
      it('should have a updateOrganizationSecurityIntrusionSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationSecurityIntrusionSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationSecurityIntrusionSettings(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationSecurityIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateOrganizationSecurityIntrusionSettings', (done) => {
        try {
          a.updateOrganizationSecurityIntrusionSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateOrganizationSecurityIntrusionSettings is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationSecurityIntrusionSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSecurityMalwareSettings - errors', () => {
      it('should have a getNetworkSecurityMalwareSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSecurityMalwareSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSecurityMalwareSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSecurityMalwareSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSecurityMalwareSettings - errors', () => {
      it('should have a updateNetworkSecurityMalwareSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSecurityMalwareSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSecurityMalwareSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSecurityMalwareSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmTargetGroups - errors', () => {
      it('should have a getNetworkSmTargetGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmTargetGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmTargetGroups(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmTargetGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmTargetGroup - errors', () => {
      it('should have a createNetworkSmTargetGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSmTargetGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSmTargetGroup(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmTargetGroup - errors', () => {
      it('should have a getNetworkSmTargetGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmTargetGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmTargetGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetGroupId', (done) => {
        try {
          a.getNetworkSmTargetGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmTargetGroup - errors', () => {
      it('should have a updateNetworkSmTargetGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmTargetGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmTargetGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetGroupId', (done) => {
        try {
          a.updateNetworkSmTargetGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'targetGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmTargetGroup - errors', () => {
      it('should have a deleteNetworkSmTargetGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSmTargetGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSmTargetGroup(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetGroupId', (done) => {
        try {
          a.deleteNetworkSmTargetGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'targetGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmTargetGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileClarity - errors', () => {
      it('should have a createNetworkSmProfileClarity function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSmProfileClarity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSmProfileClarity(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileClarity - errors', () => {
      it('should have a updateNetworkSmProfileClarity function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmProfileClarity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmProfileClarity(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateNetworkSmProfileClarity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSmProfileClarity - errors', () => {
      it('should have a addNetworkSmProfileClarity function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkSmProfileClarity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addNetworkSmProfileClarity(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.addNetworkSmProfileClarity('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfileClarity - errors', () => {
      it('should have a getNetworkSmProfileClarity function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmProfileClarity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmProfileClarity(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getNetworkSmProfileClarity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfileClarity - errors', () => {
      it('should have a deleteNetworkSmProfileClarity function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSmProfileClarity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSmProfileClarity(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteNetworkSmProfileClarity('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmProfileClarity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileUmbrella - errors', () => {
      it('should have a createNetworkSmProfileUmbrella function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSmProfileUmbrella === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSmProfileUmbrella(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileUmbrella - errors', () => {
      it('should have a updateNetworkSmProfileUmbrella function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmProfileUmbrella === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmProfileUmbrella(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateNetworkSmProfileUmbrella('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSmProfileUmbrella - errors', () => {
      it('should have a addNetworkSmProfileUmbrella function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkSmProfileUmbrella === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addNetworkSmProfileUmbrella(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.addNetworkSmProfileUmbrella('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfileUmbrella - errors', () => {
      it('should have a getNetworkSmProfileUmbrella function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmProfileUmbrella === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmProfileUmbrella(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.getNetworkSmProfileUmbrella('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfileUmbrella - errors', () => {
      it('should have a deleteNetworkSmProfileUmbrella function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSmProfileUmbrella === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSmProfileUmbrella(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.deleteNetworkSmProfileUmbrella('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmProfileUmbrella', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmAppPolaris - errors', () => {
      it('should have a createNetworkSmAppPolaris function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSmAppPolaris === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSmAppPolaris(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmAppPolaris - errors', () => {
      it('should have a getNetworkSmAppPolaris function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmAppPolaris === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmAppPolaris(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmAppPolaris - errors', () => {
      it('should have a updateNetworkSmAppPolaris function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmAppPolaris === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmAppPolaris(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.updateNetworkSmAppPolaris('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmAppPolaris - errors', () => {
      it('should have a deleteNetworkSmAppPolaris function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSmAppPolaris === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSmAppPolaris(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appId', (done) => {
        try {
          a.deleteNetworkSmAppPolaris('fakeparam', null, (data, error) => {
            try {
              const displayE = 'appId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmAppPolaris', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDevices - errors', () => {
      it('should have a getNetworkSmDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmDevices(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDevicesV2 - errors', () => {
      it('should have a getNetworkSmDevicesV2 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmDevicesV2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmDevicesV2(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDevicesV2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUsers - errors', () => {
      it('should have a getNetworkSmUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmUsers(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUserDeviceProfiles - errors', () => {
      it('should have a getNetworkSmUserDeviceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmUserDeviceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmUserDeviceProfiles(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmUserDeviceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmUserDeviceProfiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmUserDeviceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDeviceProfiles - errors', () => {
      it('should have a getNetworkSmDeviceProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmDeviceProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmDeviceProfiles(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDeviceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmDeviceProfiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDeviceProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmUserSoftwares - errors', () => {
      it('should have a getNetworkSmUserSoftwares function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmUserSoftwares === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmUserSoftwares(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmUserSoftwares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmUserSoftwares('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmUserSoftwares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmSoftwares - errors', () => {
      it('should have a getNetworkSmSoftwares function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmSoftwares === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmSoftwares(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmSoftwares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmSoftwares('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmSoftwares', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmNetworkAdapters - errors', () => {
      it('should have a getNetworkSmNetworkAdapters function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmNetworkAdapters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmNetworkAdapters(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmNetworkAdapters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmNetworkAdapters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmNetworkAdapters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmWlanLists - errors', () => {
      it('should have a getNetworkSmWlanLists function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmWlanLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmWlanLists(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmWlanLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmWlanLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmWlanLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmSecurityCenters - errors', () => {
      it('should have a getNetworkSmSecurityCenters function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmSecurityCenters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmSecurityCenters(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmSecurityCenters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmSecurityCenters('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmSecurityCenters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmRestrictions - errors', () => {
      it('should have a getNetworkSmRestrictions function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmRestrictions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmRestrictions(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmRestrictions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmRestrictions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmCerts - errors', () => {
      it('should have a getNetworkSmCerts function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmCerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmCerts(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmCerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmCerts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmCerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDevicesTags - errors', () => {
      it('should have a updateNetworkSmDevicesTags function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmDevicesTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmDevicesTags(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmDevicesTags', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDevicesTagsV1 - errors', () => {
      it('should have a updateNetworkSmDevicesTagsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmDevicesTagsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmDevicesTagsV1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmDevicesTagsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmDeviceFields - errors', () => {
      it('should have a updateNetworkSmDeviceFields function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmDeviceFields === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmDeviceFields(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmDeviceFields', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lockNetworkSmDevices - errors', () => {
      it('should have a lockNetworkSmDevices function', (done) => {
        try {
          assert.equal(true, typeof a.lockNetworkSmDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.lockNetworkSmDevices(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-lockNetworkSmDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wipeNetworkSmDevice - errors', () => {
      it('should have a wipeNetworkSmDevice function', (done) => {
        try {
          assert.equal(true, typeof a.wipeNetworkSmDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.wipeNetworkSmDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-wipeNetworkSmDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wipeNetworkSmDeviceV1 - errors', () => {
      it('should have a wipeNetworkSmDeviceV1 function', (done) => {
        try {
          assert.equal(true, typeof a.wipeNetworkSmDeviceV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.wipeNetworkSmDeviceV1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-wipeNetworkSmDeviceV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkinNetworkSmDevices - errors', () => {
      it('should have a checkinNetworkSmDevices function', (done) => {
        try {
          assert.equal(true, typeof a.checkinNetworkSmDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.checkinNetworkSmDevices(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-checkinNetworkSmDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkinNetworkSmDevicesV1 - errors', () => {
      it('should have a checkinNetworkSmDevicesV1 function', (done) => {
        try {
          assert.equal(true, typeof a.checkinNetworkSmDevicesV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.checkinNetworkSmDevicesV1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-checkinNetworkSmDevicesV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveNetworkSmDevices - errors', () => {
      it('should have a moveNetworkSmDevices function', (done) => {
        try {
          assert.equal(true, typeof a.moveNetworkSmDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.moveNetworkSmDevices(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-moveNetworkSmDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveNetworkSmDevicesV1 - errors', () => {
      it('should have a moveNetworkSmDevicesV1 function', (done) => {
        try {
          assert.equal(true, typeof a.moveNetworkSmDevicesV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.moveNetworkSmDevicesV1(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-moveNetworkSmDevicesV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unenrollNetworkSmDevice - errors', () => {
      it('should have a unenrollNetworkSmDevice function', (done) => {
        try {
          assert.equal(true, typeof a.unenrollNetworkSmDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.unenrollNetworkSmDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-unenrollNetworkSmDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceId', (done) => {
        try {
          a.unenrollNetworkSmDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-unenrollNetworkSmDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSmProfileReinstall - errors', () => {
      it('should have a updateNetworkSmProfileReinstall function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSmProfileReinstall === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSmProfileReinstall(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSmProfileReinstall', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmProfiles - errors', () => {
      it('should have a getNetworkSmProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmProfiles(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSmProfile - errors', () => {
      it('should have a deleteNetworkSmProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSmProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSmProfile(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSmProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSmProfileCreate - errors', () => {
      it('should have a createNetworkSmProfileCreate function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSmProfileCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSmProfileCreate(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSmProfileCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmCellularUsageHistory - errors', () => {
      it('should have a getNetworkSmCellularUsageHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmCellularUsageHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmCellularUsageHistory(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmCellularUsageHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmCellularUsageHistory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmCellularUsageHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmPerformanceHistory - errors', () => {
      it('should have a getNetworkSmPerformanceHistory function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmPerformanceHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmPerformanceHistory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmPerformanceHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmPerformanceHistory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmPerformanceHistory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDesktopLogs - errors', () => {
      it('should have a getNetworkSmDesktopLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmDesktopLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmDesktopLogs(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDesktopLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmDesktopLogs('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDesktopLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmDeviceCommandLogs - errors', () => {
      it('should have a getNetworkSmDeviceCommandLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmDeviceCommandLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmDeviceCommandLogs(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDeviceCommandLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmDeviceCommandLogs('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmDeviceCommandLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSmConnectivity - errors', () => {
      it('should have a getNetworkSmConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSmConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSmConnectivity(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSmConnectivity('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSmConnectivity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#refreshTheDetailsOfADevice - errors', () => {
      it('should have a refreshTheDetailsOfADevice function', (done) => {
        try {
          assert.equal(true, typeof a.refreshTheDetailsOfADevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.refreshTheDetailsOfADevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-refreshTheDetailsOfADevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSNMPSettingsForANetwork - errors', () => {
      it('should have a returnTheSNMPSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSNMPSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheSNMPSettingsForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSNMPSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheSNMPSettingsForANetwork - errors', () => {
      it('should have a updateTheSNMPSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheSNMPSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheSNMPSettingsForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSNMPSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheSNMPSettingsForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSNMPSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSNMPSettingsForAnOrganization - errors', () => {
      it('should have a returnTheSNMPSettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSNMPSettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnTheSNMPSettingsForAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSNMPSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheSNMPSettingsForAnOrganization - errors', () => {
      it('should have a updateTheSNMPSettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheSNMPSettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateTheSNMPSettingsForAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSNMPSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheSNMPSettingsForAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSNMPSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSSIDStatusesOfAnAccessPoint - errors', () => {
      it('should have a returnTheSSIDStatusesOfAnAccessPoint function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSSIDStatusesOfAnAccessPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheSSIDStatusesOfAnAccessPoint(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSSIDStatusesOfAnAccessPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnTheSSIDStatusesOfAnAccessPoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSSIDStatusesOfAnAccessPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSSIDStatusesOfAnAccessPointV1 - errors', () => {
      it('should have a returnTheSSIDStatusesOfAnAccessPointV1 function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSSIDStatusesOfAnAccessPointV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnTheSSIDStatusesOfAnAccessPointV1(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSSIDStatusesOfAnAccessPointV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheAccessPoliciesForASwitchNetwork - errors', () => {
      it('should have a listTheAccessPoliciesForASwitchNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listTheAccessPoliciesForASwitchNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheAccessPoliciesForASwitchNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheAccessPoliciesForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should have a createAnAccessPolicyForASwitchNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createAnAccessPolicyForASwitchNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createAnAccessPolicyForASwitchNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAnAccessPolicyForASwitchNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASpecificAccessPolicyForASwitchNetwork - errors', () => {
      it('should have a returnASpecificAccessPolicyForASwitchNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnASpecificAccessPolicyForASwitchNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnASpecificAccessPolicyForASwitchNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASpecificAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accessPolicyNumber', (done) => {
        try {
          a.returnASpecificAccessPolicyForASwitchNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'accessPolicyNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASpecificAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should have a updateAnAccessPolicyForASwitchNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnAccessPolicyForASwitchNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAnAccessPolicyForASwitchNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accessPolicyNumber', (done) => {
        try {
          a.updateAnAccessPolicyForASwitchNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'accessPolicyNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnAccessPolicyForASwitchNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAccessPolicyForASwitchNetwork - errors', () => {
      it('should have a deleteAnAccessPolicyForASwitchNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAccessPolicyForASwitchNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAnAccessPolicyForASwitchNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing accessPolicyNumber', (done) => {
        try {
          a.deleteAnAccessPolicyForASwitchNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'accessPolicyNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnAccessPolicyForASwitchNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnThePacketCountersForAllThePortsOfASwitch - errors', () => {
      it('should have a returnThePacketCountersForAllThePortsOfASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnThePacketCountersForAllThePortsOfASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnThePacketCountersForAllThePortsOfASwitch(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnThePacketCountersForAllThePortsOfASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheStatusForAllThePortsOfASwitch - errors', () => {
      it('should have a returnTheStatusForAllThePortsOfASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheStatusForAllThePortsOfASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnTheStatusForAllThePortsOfASwitch(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheStatusForAllThePortsOfASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheUplinkBandwidthSettingsForYourMXNetwork - errors', () => {
      it('should have a returnsTheUplinkBandwidthSettingsForYourMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheUplinkBandwidthSettingsForYourMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnsTheUplinkBandwidthSettingsForYourMXNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheUplinkBandwidthSettingsForYourMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatesTheUplinkBandwidthSettingsForYourMXNetwork - errors', () => {
      it('should have a updatesTheUplinkBandwidthSettingsForYourMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updatesTheUplinkBandwidthSettingsForYourMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updatesTheUplinkBandwidthSettingsForYourMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesTheUplinkBandwidthSettingsForYourMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatesTheUplinkBandwidthSettingsForYourMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesTheUplinkBandwidthSettingsForYourMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSplashLoginAttempts - errors', () => {
      it('should have a getNetworkSplashLoginAttempts function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSplashLoginAttempts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getNetworkSplashLoginAttempts(null, null, null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSplashLoginAttempts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidSplashSettings - errors', () => {
      it('should have a getNetworkSsidSplashSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsidSplashSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsidSplashSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidSplashSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getNetworkSsidSplashSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidSplashSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidSplashSettings - errors', () => {
      it('should have a updateNetworkSsidSplashSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSsidSplashSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSsidSplashSettings(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidSplashSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateNetworkSsidSplashSettings('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidSplashSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsids - errors', () => {
      it('should have a getNetworkSsids function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsids === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsids(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsids', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsid - errors', () => {
      it('should have a getNetworkSsid function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsid(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getNetworkSsid('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsid - errors', () => {
      it('should have a updateNetworkSsid function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSsid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSsid(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateNetworkSsid('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettings - errors', () => {
      it('should have a getNetworkSwitchSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettings - errors', () => {
      it('should have a updateNetworkSwitchSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSwitchPorts - errors', () => {
      it('should have a getDeviceSwitchPorts function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceSwitchPorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceSwitchPorts(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceSwitchPorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceSwitchPort - errors', () => {
      it('should have a getDeviceSwitchPort function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceSwitchPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceSwitchPort(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceSwitchPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getDeviceSwitchPort('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceSwitchPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceSwitchPort - errors', () => {
      it('should have a updateDeviceSwitchPort function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceSwitchPort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateDeviceSwitchPort(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateDeviceSwitchPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateDeviceSwitchPort('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateDeviceSwitchPort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationConfigTemplateSwitchProfiles - errors', () => {
      it('should have a getOrganizationConfigTemplateSwitchProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationConfigTemplateSwitchProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationConfigTemplateSwitchProfiles(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationConfigTemplateSwitchProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.getOrganizationConfigTemplateSwitchProfiles('fakeparam', null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationConfigTemplateSwitchProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchStacks - errors', () => {
      it('should have a getNetworkSwitchStacks function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchStacks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchStacks(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchStacks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSwitchStack - errors', () => {
      it('should have a createNetworkSwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSwitchStack(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createNetworkSwitchStack', (done) => {
        try {
          a.createNetworkSwitchStack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'createNetworkSwitchStack is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchStack - errors', () => {
      it('should have a getNetworkSwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchStack(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.getNetworkSwitchStack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchStack - errors', () => {
      it('should have a deleteNetworkSwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSwitchStack(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.deleteNetworkSwitchStack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNetworkSwitchStack - errors', () => {
      it('should have a addNetworkSwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.addNetworkSwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addNetworkSwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.addNetworkSwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addNetworkSwitchStack', (done) => {
        try {
          a.addNetworkSwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'addNetworkSwitchStack is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeNetworkSwitchStack - errors', () => {
      it('should have a removeNetworkSwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.removeNetworkSwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkID', (done) => {
        try {
          a.removeNetworkSwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkID is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.removeNetworkSwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing removeNetworkSwitchStack', (done) => {
        try {
          a.removeNetworkSwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'removeNetworkSwitchStack is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeNetworkSwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSyslogServers - errors', () => {
      it('should have a getNetworkSyslogServers function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSyslogServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSyslogServers(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSyslogServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSyslogServers - errors', () => {
      it('should have a updateNetworkSyslogServers function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSyslogServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSyslogServers(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSyslogServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateNetworkSyslogServers', (done) => {
        try {
          a.updateNetworkSyslogServers('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateNetworkSyslogServers is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSyslogServers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkTrafficShaping - errors', () => {
      it('should have a updateNetworkTrafficShaping function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkTrafficShaping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkTrafficShaping(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShaping - errors', () => {
      it('should have a getNetworkTrafficShaping function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTrafficShaping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTrafficShaping(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSsidTrafficShaping - errors', () => {
      it('should have a updateNetworkSsidTrafficShaping function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSsidTrafficShaping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSsidTrafficShaping(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateNetworkSsidTrafficShaping('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSsidTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSsidTrafficShaping - errors', () => {
      it('should have a getNetworkSsidTrafficShaping function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSsidTrafficShaping === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSsidTrafficShaping(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.getNetworkSsidTrafficShaping('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSsidTrafficShaping', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShapingDscpTaggingOptions - errors', () => {
      it('should have a getNetworkTrafficShapingDscpTaggingOptions function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTrafficShapingDscpTaggingOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTrafficShapingDscpTaggingOptions(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTrafficShapingDscpTaggingOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficShapingApplicationCategories - errors', () => {
      it('should have a getNetworkTrafficShapingApplicationCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTrafficShapingApplicationCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTrafficShapingApplicationCategories(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTrafficShapingApplicationCategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContentFilteringCategories - errors', () => {
      it('should have a getNetworkContentFilteringCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkContentFilteringCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkContentFilteringCategories(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkContentFilteringCategories', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContentFiltering - errors', () => {
      it('should have a getNetworkContentFiltering function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkContentFiltering === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkContentFiltering(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkContentFiltering', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkContentFiltering - errors', () => {
      it('should have a updateNetworkContentFiltering function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkContentFiltering === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkContentFiltering(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkContentFiltering', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFirewalledServices - errors', () => {
      it('should have a getNetworkFirewalledServices function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFirewalledServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkFirewalledServices(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFirewalledServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFirewalledService - errors', () => {
      it('should have a getNetworkFirewalledService function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFirewalledService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkFirewalledService(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFirewalledService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.getNetworkFirewalledService('fakeparam', null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFirewalledService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkFirewalledService - errors', () => {
      it('should have a updateNetworkFirewalledService function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkFirewalledService === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkFirewalledService(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkFirewalledService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.updateNetworkFirewalledService('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkFirewalledService', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkOneToManyNatRules - errors', () => {
      it('should have a getNetworkOneToManyNatRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkOneToManyNatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkOneToManyNatRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkOneToManyNatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkOneToManyNatRules - errors', () => {
      it('should have a updateNetworkOneToManyNatRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkOneToManyNatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkOneToManyNatRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkOneToManyNatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkOneToOneNatRules - errors', () => {
      it('should have a getNetworkOneToOneNatRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkOneToOneNatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkOneToOneNatRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkOneToOneNatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkOneToOneNatRules - errors', () => {
      it('should have a updateNetworkOneToOneNatRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkOneToOneNatRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkOneToOneNatRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkOneToOneNatRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkPortForwardingRules - errors', () => {
      it('should have a getNetworkPortForwardingRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkPortForwardingRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkPortForwardingRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkPortForwardingRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkPortForwardingRules - errors', () => {
      it('should have a updateNetworkPortForwardingRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkPortForwardingRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkPortForwardingRules(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkPortForwardingRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkStaticRoutes - errors', () => {
      it('should have a getNetworkStaticRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkStaticRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkStaticRoutes(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkStaticRoutes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkStaticRoute - errors', () => {
      it('should have a createNetworkStaticRoute function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkStaticRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkStaticRoute(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkStaticRoute - errors', () => {
      it('should have a getNetworkStaticRoute function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkStaticRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkStaticRoute(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srId', (done) => {
        try {
          a.getNetworkStaticRoute('fakeparam', null, (data, error) => {
            try {
              const displayE = 'srId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkStaticRoute - errors', () => {
      it('should have a updateNetworkStaticRoute function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkStaticRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkStaticRoute(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srId', (done) => {
        try {
          a.updateNetworkStaticRoute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'srId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkStaticRoute - errors', () => {
      it('should have a deleteNetworkStaticRoute function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkStaticRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkStaticRoute(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srId', (done) => {
        try {
          a.deleteNetworkStaticRoute('fakeparam', null, (data, error) => {
            try {
              const displayE = 'srId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkStaticRoute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkUplinkSettings - errors', () => {
      it('should have a getNetworkUplinkSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkUplinkSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkUplinkSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkUplinkSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkUplinkSettings - errors', () => {
      it('should have a updateNetworkUplinkSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkUplinkSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkUplinkSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkUplinkSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlans - errors', () => {
      it('should have a getNetworkVlans function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkVlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkVlans(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkVlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkVlan - errors', () => {
      it('should have a createNetworkVlan function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkVlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkVlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createNetworkVlan', (done) => {
        try {
          a.createNetworkVlan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'createNetworkVlan is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlan - errors', () => {
      it('should have a getNetworkVlan function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkVlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkVlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlanId', (done) => {
        try {
          a.getNetworkVlan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkVlan - errors', () => {
      it('should have a updateNetworkVlan function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkVlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkVlan(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlanId', (done) => {
        try {
          a.updateNetworkVlan('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkVlan - errors', () => {
      it('should have a deleteNetworkVlan function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkVlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkVlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vlanId', (done) => {
        try {
          a.deleteNetworkVlan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkVlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkVlansEnabledState - errors', () => {
      it('should have a getNetworkVlansEnabledState function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkVlansEnabledState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkVlansEnabledState(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkVlansEnabledState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkVlansEnabledState - errors', () => {
      it('should have a updateNetworkVlansEnabledState function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkVlansEnabledState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkVlansEnabledState(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkVlansEnabledState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updateNetworkVlansEnabledState', (done) => {
        try {
          a.updateNetworkVlansEnabledState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updateNetworkVlansEnabledState is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkVlansEnabledState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkConnectionStats - errors', () => {
      it('should have a getNetworkConnectionStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkConnectionStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkConnectionStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicesConnectionStats - errors', () => {
      it('should have a getNetworkDevicesConnectionStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevicesConnectionStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDevicesConnectionStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevicesConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceConnectionStats - errors', () => {
      it('should have a getNetworkDeviceConnectionStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceConnectionStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceConnectionStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceConnectionStats('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceConnectionStatsV1 - errors', () => {
      it('should have a getNetworkDeviceConnectionStatsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceConnectionStatsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceConnectionStatsV1(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceConnectionStatsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheEAPOverriddenParametersForAnSSID - errors', () => {
      it('should have a returnTheEAPOverriddenParametersForAnSSID function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheEAPOverriddenParametersForAnSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheEAPOverriddenParametersForAnSSID(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheEAPOverriddenParametersForAnSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.returnTheEAPOverriddenParametersForAnSSID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheEAPOverriddenParametersForAnSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheEAPOverriddenParametersForAnSSID - errors', () => {
      it('should have a updateTheEAPOverriddenParametersForAnSSID function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheEAPOverriddenParametersForAnSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheEAPOverriddenParametersForAnSSID(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheEAPOverriddenParametersForAnSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateTheEAPOverriddenParametersForAnSSID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheEAPOverriddenParametersForAnSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheEAPOverriddenParametersForAnSSID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheEAPOverriddenParametersForAnSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientsConnectionStats - errors', () => {
      it('should have a getNetworkClientsConnectionStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientsConnectionStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientsConnectionStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientsConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientConnectionStats - errors', () => {
      it('should have a getNetworkClientConnectionStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientConnectionStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientConnectionStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientConnectionStats('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientConnectionStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkLatencyStats - errors', () => {
      it('should have a getNetworkLatencyStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkLatencyStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkLatencyStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDevicesLatencyStats - errors', () => {
      it('should have a getNetworkDevicesLatencyStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDevicesLatencyStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDevicesLatencyStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDevicesLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLatencyStats - errors', () => {
      it('should have a getNetworkDeviceLatencyStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLatencyStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkDeviceLatencyStats(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLatencyStats('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDeviceLatencyStatsV1 - errors', () => {
      it('should have a getNetworkDeviceLatencyStatsV1 function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDeviceLatencyStatsV1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getNetworkDeviceLatencyStatsV1(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkDeviceLatencyStatsV1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientsLatencyStats - errors', () => {
      it('should have a getNetworkClientsLatencyStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientsLatencyStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientsLatencyStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientsLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkClientLatencyStats - errors', () => {
      it('should have a getNetworkClientLatencyStats function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkClientLatencyStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkClientLatencyStats(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.getNetworkClientLatencyStats('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkClientLatencyStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFailedConnections - errors', () => {
      it('should have a getNetworkFailedConnections function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFailedConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkFailedConnections(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFailedConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraQualityRetentionProfiles - errors', () => {
      it('should have a getNetworkCameraQualityRetentionProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCameraQualityRetentionProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCameraQualityRetentionProfiles(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCameraQualityRetentionProfiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkCameraQualityRetentionProfile - errors', () => {
      it('should have a createNetworkCameraQualityRetentionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkCameraQualityRetentionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkCameraQualityRetentionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCameraQualityRetentionProfile - errors', () => {
      it('should have a getNetworkCameraQualityRetentionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCameraQualityRetentionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCameraQualityRetentionProfile(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qualityRetentionProfileId', (done) => {
        try {
          a.getNetworkCameraQualityRetentionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qualityRetentionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCameraQualityRetentionProfile - errors', () => {
      it('should have a updateNetworkCameraQualityRetentionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCameraQualityRetentionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCameraQualityRetentionProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qualityRetentionProfileId', (done) => {
        try {
          a.updateNetworkCameraQualityRetentionProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qualityRetentionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkCameraQualityRetentionProfile - errors', () => {
      it('should have a deleteNetworkCameraQualityRetentionProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkCameraQualityRetentionProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkCameraQualityRetentionProfile(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qualityRetentionProfileId', (done) => {
        try {
          a.deleteNetworkCameraQualityRetentionProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qualityRetentionProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkCameraQualityRetentionProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations - errors', () => {
      it('should have a getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCellularGatewaySettingsConnectivityMonitoringDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations - errors', () => {
      it('should have a updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCellularGatewaySettingsConnectivityMonitoringDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsDhcp - errors', () => {
      it('should have a getNetworkCellularGatewaySettingsDhcp function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCellularGatewaySettingsDhcp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsDhcp(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCellularGatewaySettingsDhcp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularGatewaySettingsDhcp - errors', () => {
      it('should have a updateNetworkCellularGatewaySettingsDhcp function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCellularGatewaySettingsDhcp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsDhcp(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCellularGatewaySettingsDhcp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCellularGatewaySettings - errors', () => {
      it('should have a getDeviceCellularGatewaySettings function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCellularGatewaySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCellularGatewaySettings(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCellularGatewaySettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceCellularGatewaySettings - errors', () => {
      it('should have a updateDeviceCellularGatewaySettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceCellularGatewaySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateDeviceCellularGatewaySettings(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateDeviceCellularGatewaySettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDeviceCellularGatewaySettingsPortForwardingRules - errors', () => {
      it('should have a getDeviceCellularGatewaySettingsPortForwardingRules function', (done) => {
        try {
          assert.equal(true, typeof a.getDeviceCellularGatewaySettingsPortForwardingRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.getDeviceCellularGatewaySettingsPortForwardingRules(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getDeviceCellularGatewaySettingsPortForwardingRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDeviceCellularGatewaySettingsPortForwardingRules - errors', () => {
      it('should have a updateDeviceCellularGatewaySettingsPortForwardingRules function', (done) => {
        try {
          assert.equal(true, typeof a.updateDeviceCellularGatewaySettingsPortForwardingRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateDeviceCellularGatewaySettingsPortForwardingRules(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateDeviceCellularGatewaySettingsPortForwardingRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsSubnetPool - errors', () => {
      it('should have a getNetworkCellularGatewaySettingsSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCellularGatewaySettingsSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsSubnetPool(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCellularGatewaySettingsSubnetPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularGatewaySettingsSubnetPool - errors', () => {
      it('should have a updateNetworkCellularGatewaySettingsSubnetPool function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCellularGatewaySettingsSubnetPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsSubnetPool(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCellularGatewaySettingsSubnetPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkCellularGatewaySettingsUplink - errors', () => {
      it('should have a getNetworkCellularGatewaySettingsUplink function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkCellularGatewaySettingsUplink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkCellularGatewaySettingsUplink(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkCellularGatewaySettingsUplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkCellularGatewaySettingsUplink - errors', () => {
      it('should have a updateNetworkCellularGatewaySettingsUplink function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkCellularGatewaySettingsUplink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkCellularGatewaySettingsUplink(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkCellularGatewaySettingsUplink', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchAccessControlLists - errors', () => {
      it('should have a getNetworkSwitchAccessControlLists function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchAccessControlLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchAccessControlLists(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchAccessControlLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchAccessControlLists - errors', () => {
      it('should have a updateNetworkSwitchAccessControlLists function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchAccessControlLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchAccessControlLists('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchAccessControlLists', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchPortSchedules - errors', () => {
      it('should have a getNetworkSwitchPortSchedules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchPortSchedules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchPortSchedules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchPortSchedules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSwitchPortSchedule - errors', () => {
      it('should have a createNetworkSwitchPortSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSwitchPortSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSwitchPortSchedule(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSwitchPortSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchPortSchedule - errors', () => {
      it('should have a updateNetworkSwitchPortSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchPortSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchPortSchedule(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchPortSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portScheduleId', (done) => {
        try {
          a.updateNetworkSwitchPortSchedule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'portScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchPortSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchPortSchedule - errors', () => {
      it('should have a deleteNetworkSwitchPortSchedule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSwitchPortSchedule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSwitchPortSchedule(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchPortSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portScheduleId', (done) => {
        try {
          a.deleteNetworkSwitchPortSchedule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portScheduleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchPortSchedule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAppliancePorts - errors', () => {
      it('should have a getNetworkAppliancePorts function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAppliancePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkAppliancePorts(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAppliancePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkAppliancePort - errors', () => {
      it('should have a getNetworkAppliancePort function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkAppliancePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkAppliancePort(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAppliancePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appliancePortId', (done) => {
        try {
          a.getNetworkAppliancePort('fakeparam', null, (data, error) => {
            try {
              const displayE = 'appliancePortId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkAppliancePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkAppliancePort - errors', () => {
      it('should have a updateNetworkAppliancePort function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkAppliancePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkAppliancePort('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkAppliancePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appliancePortId', (done) => {
        try {
          a.updateNetworkAppliancePort('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'appliancePortId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkAppliancePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#swapNetworkWarmspare - errors', () => {
      it('should have a swapNetworkWarmspare function', (done) => {
        try {
          assert.equal(true, typeof a.swapNetworkWarmspare === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.swapNetworkWarmspare(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-swapNetworkWarmspare', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWarmSpareSettings - errors', () => {
      it('should have a getNetworkWarmSpareSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkWarmSpareSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkWarmSpareSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkWarmSpareSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkWarmSpareSettings - errors', () => {
      it('should have a updateNetworkWarmSpareSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkWarmSpareSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkWarmSpareSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkWarmSpareSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicenses - errors', () => {
      it('should have a getOrganizationLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationLicenses('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationLicenses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationLicense - errors', () => {
      it('should have a getOrganizationLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationLicense(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseId', (done) => {
        try {
          a.getOrganizationLicense('fakeparam', null, (data, error) => {
            try {
              const displayE = 'licenseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationLicense - errors', () => {
      it('should have a updateOrganizationLicense function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationLicense('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseId', (done) => {
        try {
          a.updateOrganizationLicense('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'licenseId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationLicense', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignOrganizationLicensesSeats - errors', () => {
      it('should have a assignOrganizationLicensesSeats function', (done) => {
        try {
          assert.equal(true, typeof a.assignOrganizationLicensesSeats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.assignOrganizationLicensesSeats(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-assignOrganizationLicensesSeats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveOrganizationLicenses - errors', () => {
      it('should have a moveOrganizationLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.moveOrganizationLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.moveOrganizationLicenses(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-moveOrganizationLicenses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveOrganizationLicensesSeats - errors', () => {
      it('should have a moveOrganizationLicensesSeats function', (done) => {
        try {
          assert.equal(true, typeof a.moveOrganizationLicensesSeats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.moveOrganizationLicensesSeats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-moveOrganizationLicensesSeats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#renewOrganizationLicensesSeats - errors', () => {
      it('should have a renewOrganizationLicensesSeats function', (done) => {
        try {
          assert.equal(true, typeof a.renewOrganizationLicensesSeats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.renewOrganizationLicensesSeats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-renewOrganizationLicensesSeats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsDhcpServerPolicy - errors', () => {
      it('should have a getNetworkSwitchSettingsDhcpServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsDhcpServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsDhcpServerPolicy(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsDhcpServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsDhcpServerPolicy - errors', () => {
      it('should have a updateNetworkSwitchSettingsDhcpServerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsDhcpServerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsDhcpServerPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsDhcpServerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsDscpToCosMappings - errors', () => {
      it('should have a getNetworkSwitchSettingsDscpToCosMappings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsDscpToCosMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsDscpToCosMappings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsDscpToCosMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsDscpToCosMappings - errors', () => {
      it('should have a updateNetworkSwitchSettingsDscpToCosMappings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsDscpToCosMappings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsDscpToCosMappings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsDscpToCosMappings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsMtu - errors', () => {
      it('should have a getNetworkSwitchSettingsMtu function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsMtu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsMtu(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsMtu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsMtu - errors', () => {
      it('should have a updateNetworkSwitchSettingsMtu function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsMtu === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsMtu('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsMtu', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRules - errors', () => {
      it('should have a getNetworkSwitchSettingsQosRules function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsQosRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRules(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsQosRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSwitchSettingsQosRule - errors', () => {
      it('should have a createNetworkSwitchSettingsQosRule function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSwitchSettingsQosRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSwitchSettingsQosRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRulesOrder - errors', () => {
      it('should have a getNetworkSwitchSettingsQosRulesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsQosRulesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRulesOrder(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsQosRulesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsQosRulesOrder - errors', () => {
      it('should have a updateNetworkSwitchSettingsQosRulesOrder function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsQosRulesOrder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsQosRulesOrder('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsQosRulesOrder', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsQosRule - errors', () => {
      it('should have a getNetworkSwitchSettingsQosRule function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsQosRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRule(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosRuleId', (done) => {
        try {
          a.getNetworkSwitchSettingsQosRule('fakedata', null, (data, error) => {
            try {
              const displayE = 'qosRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchSettingsQosRule - errors', () => {
      it('should have a deleteNetworkSwitchSettingsQosRule function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSwitchSettingsQosRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSwitchSettingsQosRule(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosRuleId', (done) => {
        try {
          a.deleteNetworkSwitchSettingsQosRule('fakeparam', null, (data, error) => {
            try {
              const displayE = 'qosRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsQosRule - errors', () => {
      it('should have a updateNetworkSwitchSettingsQosRule function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsQosRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsQosRule(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qosRuleId', (done) => {
        try {
          a.updateNetworkSwitchSettingsQosRule('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qosRuleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsQosRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsStormControl - errors', () => {
      it('should have a getNetworkSwitchSettingsStormControl function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsStormControl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsStormControl(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsStormControl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsStormControl - errors', () => {
      it('should have a updateNetworkSwitchSettingsStormControl function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsStormControl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsStormControl(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsStormControl', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchSettingsStp - errors', () => {
      it('should have a getNetworkSwitchSettingsStp function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchSettingsStp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchSettingsStp(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchSettingsStp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchSettingsStp - errors', () => {
      it('should have a updateNetworkSwitchSettingsStp function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchSettingsStp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchSettingsStp('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchSettingsStp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkConnectivityMonitoringDestinations - errors', () => {
      it('should have a getNetworkConnectivityMonitoringDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkConnectivityMonitoringDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkConnectivityMonitoringDestinations(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkConnectivityMonitoringDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkConnectivityMonitoringDestinations - errors', () => {
      it('should have a updateNetworkConnectivityMonitoringDestinations function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkConnectivityMonitoringDestinations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkConnectivityMonitoringDestinations(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkConnectivityMonitoringDestinations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPolicies - errors', () => {
      it('should have a getOrganizationBrandingPolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationBrandingPolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationBrandingPolicies(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationBrandingPolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrganizationBrandingPolicy - errors', () => {
      it('should have a createOrganizationBrandingPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.createOrganizationBrandingPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createOrganizationBrandingPolicy(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPoliciesPriorities - errors', () => {
      it('should have a getOrganizationBrandingPoliciesPriorities function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationBrandingPoliciesPriorities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationBrandingPoliciesPriorities(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationBrandingPoliciesPriorities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationBrandingPoliciesPriorities - errors', () => {
      it('should have a updateOrganizationBrandingPoliciesPriorities function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationBrandingPoliciesPriorities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationBrandingPoliciesPriorities(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationBrandingPoliciesPriorities', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationBrandingPolicy - errors', () => {
      it('should have a getOrganizationBrandingPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationBrandingPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationBrandingPolicy(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateOrganizationBrandingPolicy - errors', () => {
      it('should have a updateOrganizationBrandingPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateOrganizationBrandingPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateOrganizationBrandingPolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brandingPolicyId', (done) => {
        try {
          a.updateOrganizationBrandingPolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'brandingPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOrganizationBrandingPolicy - errors', () => {
      it('should have a deleteOrganizationBrandingPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOrganizationBrandingPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteOrganizationBrandingPolicy(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brandingPolicyId', (done) => {
        try {
          a.deleteOrganizationBrandingPolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'brandingPolicyId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteOrganizationBrandingPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEvents - errors', () => {
      it('should have a getNetworkEvents function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEvents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkEvents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkEvents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkEventsEventTypes - errors', () => {
      it('should have a getNetworkEventsEventTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkEventsEventTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkEventsEventTypes(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkEventsEventTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFloorPlans - errors', () => {
      it('should have a getNetworkFloorPlans function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFloorPlans === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkFloorPlans(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFloorPlans', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkFloorPlan - errors', () => {
      it('should have a createNetworkFloorPlan function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkFloorPlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkFloorPlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkFloorPlan - errors', () => {
      it('should have a getNetworkFloorPlan function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkFloorPlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkFloorPlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorPlanId', (done) => {
        try {
          a.getNetworkFloorPlan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floorPlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkFloorPlan - errors', () => {
      it('should have a updateNetworkFloorPlan function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkFloorPlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkFloorPlan(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorPlanId', (done) => {
        try {
          a.updateNetworkFloorPlan('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'floorPlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkFloorPlan - errors', () => {
      it('should have a deleteNetworkFloorPlan function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkFloorPlan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkFloorPlan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing floorPlanId', (done) => {
        try {
          a.deleteNetworkFloorPlan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'floorPlanId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkFloorPlan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkSwitchLinkAggregations - errors', () => {
      it('should have a getNetworkSwitchLinkAggregations function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkSwitchLinkAggregations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkSwitchLinkAggregations(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkSwitchLinkAggregations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkSwitchLinkAggregation - errors', () => {
      it('should have a createNetworkSwitchLinkAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkSwitchLinkAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createNetworkSwitchLinkAggregation(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createNetworkSwitchLinkAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkSwitchLinkAggregation - errors', () => {
      it('should have a updateNetworkSwitchLinkAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkSwitchLinkAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkSwitchLinkAggregation(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchLinkAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkAggregationId', (done) => {
        try {
          a.updateNetworkSwitchLinkAggregation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'linkAggregationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkSwitchLinkAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkSwitchLinkAggregation - errors', () => {
      it('should have a deleteNetworkSwitchLinkAggregation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkSwitchLinkAggregation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteNetworkSwitchLinkAggregation(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchLinkAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkAggregationId', (done) => {
        try {
          a.deleteNetworkSwitchLinkAggregation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'linkAggregationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteNetworkSwitchLinkAggregation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkNetflowSettings - errors', () => {
      it('should have a getNetworkNetflowSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkNetflowSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkNetflowSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkNetflowSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkNetflowSettings - errors', () => {
      it('should have a updateNetworkNetflowSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkNetflowSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkNetflowSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkNetflowSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkTrafficAnalysisSettings - errors', () => {
      it('should have a getNetworkTrafficAnalysisSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkTrafficAnalysisSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkTrafficAnalysisSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkTrafficAnalysisSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkTrafficAnalysisSettings - errors', () => {
      it('should have a updateNetworkTrafficAnalysisSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkTrafficAnalysisSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkTrafficAnalysisSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkTrafficAnalysisSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOrganizationWebhookLogs - errors', () => {
      it('should have a getOrganizationWebhookLogs function', (done) => {
        try {
          assert.equal(true, typeof a.getOrganizationWebhookLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getOrganizationWebhookLogs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getOrganizationWebhookLogs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkWirelessSettings - errors', () => {
      it('should have a getNetworkWirelessSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkWirelessSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getNetworkWirelessSettings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getNetworkWirelessSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateNetworkWirelessSettings - errors', () => {
      it('should have a updateNetworkWirelessSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateNetworkWirelessSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateNetworkWirelessSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateNetworkWirelessSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnstheidentityofthecurrentuser - errors', () => {
      it('should have a returnstheidentityofthecurrentuser function', (done) => {
        try {
          assert.equal(true, typeof a.returnstheidentityofthecurrentuser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheDHCPsubnetinformationforanappliance - errors', () => {
      it('should have a returntheDHCPsubnetinformationforanappliance function', (done) => {
        try {
          assert.equal(true, typeof a.returntheDHCPsubnetinformationforanappliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returntheDHCPsubnetinformationforanappliance(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returntheDHCPsubnetinformationforanappliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getthesentandreceivedbytesforeachuplinkofanetwork - errors', () => {
      it('should have a getthesentandreceivedbytesforeachuplinkofanetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getthesentandreceivedbytesforeachuplinkofanetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getthesentandreceivedbytesforeachuplinkofanetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getthesentandreceivedbytesforeachuplinkofanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPusageovertimeforadeviceornetworkclient - errors', () => {
      it('should have a returnAPusageovertimeforadeviceornetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnAPusageovertimeforadeviceornetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAPusageovertimeforadeviceornetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPusageovertimeforadeviceornetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization - errors', () => {
      it('should have a listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheuplinkstatusofeveryMerakiMXandZseriesappliancesintheorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchOnboardingStatusOfCameras - errors', () => {
      it('should have a fetchOnboardingStatusOfCameras function', (done) => {
        try {
          assert.equal(true, typeof a.fetchOnboardingStatusOfCameras === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.fetchOnboardingStatusOfCameras(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-fetchOnboardingStatusOfCameras', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notifyThatCredentialHandoffToCameraHasCompleted - errors', () => {
      it('should have a notifyThatCredentialHandoffToCameraHasCompleted function', (done) => {
        try {
          assert.equal(true, typeof a.notifyThatCredentialHandoffToCameraHasCompleted === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.notifyThatCredentialHandoffToCameraHasCompleted(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-notifyThatCredentialHandoffToCameraHasCompleted', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.notifyThatCredentialHandoffToCameraHasCompleted('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-notifyThatCredentialHandoffToCameraHasCompleted', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization - errors', () => {
      it('should have a listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheuplinkstatusofeveryMerakiMGcellulargatewayintheorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization - errors', () => {
      it('should have a listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheuplinkstatusofeveryMerakiMXMGandZseriesdevicesintheorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNstatusfornetworksinanorganization - errors', () => {
      it('should have a showVPNstatusfornetworksinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.showVPNstatusfornetworksinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.showVPNstatusfornetworksinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-showVPNstatusfornetworksinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheStatusOfEveryMerakiDeviceInTheOrganization - errors', () => {
      it('should have a listTheStatusOfEveryMerakiDeviceInTheOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listTheStatusOfEveryMerakiDeviceInTheOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listTheStatusOfEveryMerakiDeviceInTheOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheStatusOfEveryMerakiDeviceInTheOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showVPNhistorystatfornetworksinanorganization - errors', () => {
      it('should have a showVPNhistorystatfornetworksinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.showVPNhistorystatfornetworksinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.showVPNhistorystatfornetworksinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-showVPNhistorystatfornetworksinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheFirewallSettingsForThisNetwork - errors', () => {
      it('should have a returnTheFirewallSettingsForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheFirewallSettingsForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheFirewallSettingsForThisNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheFirewallSettingsForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheFirewallSettingsForThisNetwork - errors', () => {
      it('should have a updateTheFirewallSettingsForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheFirewallSettingsForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheFirewallSettingsForThisNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheFirewallSettingsForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheFirewallSettingsForThisNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheFirewallSettingsForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheApplianceSettingsForANetwork - errors', () => {
      it('should have a returnTheApplianceSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheApplianceSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheApplianceSettingsForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheApplianceSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheApplianceSettingsForANetwork - errors', () => {
      it('should have a updateTheApplianceSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheApplianceSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheApplianceSettingsForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheApplianceSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheApplianceSettingsForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheApplianceSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheEnabledStatusOfVLANsForTheNetwork - errors', () => {
      it('should have a returnsTheEnabledStatusOfVLANsForTheNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheEnabledStatusOfVLANsForTheNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnsTheEnabledStatusOfVLANsForTheNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheEnabledStatusOfVLANsForTheNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableDisableVLANsForTheGivenNetwork - errors', () => {
      it('should have a enableDisableVLANsForTheGivenNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.enableDisableVLANsForTheGivenNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.enableDisableVLANsForTheGivenNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enableDisableVLANsForTheGivenNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.enableDisableVLANsForTheGivenNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enableDisableVLANsForTheGivenNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSettingsForANetwork - errors', () => {
      it('should have a returnTheSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheSettingsForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheSettingsForANetwork - errors', () => {
      it('should have a updateTheSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheSettingsForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheSettingsForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsGlobalAdaptivePolicySettingsInAnOrganization - errors', () => {
      it('should have a returnsGlobalAdaptivePolicySettingsInAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnsGlobalAdaptivePolicySettingsInAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsGlobalAdaptivePolicySettingsInAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsGlobalAdaptivePolicySettingsInAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGlobalAdaptivePolicySettings - errors', () => {
      it('should have a updateGlobalAdaptivePolicySettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateGlobalAdaptivePolicySettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateGlobalAdaptivePolicySettings(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateGlobalAdaptivePolicySettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateGlobalAdaptivePolicySettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateGlobalAdaptivePolicySettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBluetoothSettingsForAWirelessDevice - errors', () => {
      it('should have a returnTheBluetoothSettingsForAWirelessDevice function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheBluetoothSettingsForAWirelessDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnTheBluetoothSettingsForAWirelessDevice(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheBluetoothSettingsForAWirelessDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheBluetoothSettingsForAWirelessDevice - errors', () => {
      it('should have a updateTheBluetoothSettingsForAWirelessDevice function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheBluetoothSettingsForAWirelessDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateTheBluetoothSettingsForAWirelessDevice(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBluetoothSettingsForAWirelessDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheBluetoothSettingsForAWirelessDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBluetoothSettingsForAWirelessDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork - errors', () => {
      it('should have a returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheBluetoothSettingsForANetworkAHrefHttpsDocumentationMerakiComMRBluetoothBluetoothLowEnergyBLEBluetoothSettingsAMustBeEnabledOnTheNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheBluetoothSettingsForANetwork - errors', () => {
      it('should have a updateTheBluetoothSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheBluetoothSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheBluetoothSettingsForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBluetoothSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheBluetoothSettingsForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBluetoothSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAllThePortsOfASwitchProfile - errors', () => {
      it('should have a returnAllThePortsOfASwitchProfile function', (done) => {
        try {
          assert.equal(true, typeof a.returnAllThePortsOfASwitchProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnAllThePortsOfASwitchProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAllThePortsOfASwitchProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.returnAllThePortsOfASwitchProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAllThePortsOfASwitchProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.returnAllThePortsOfASwitchProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAllThePortsOfASwitchProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASwitchProfilePort - errors', () => {
      it('should have a returnASwitchProfilePort function', (done) => {
        try {
          assert.equal(true, typeof a.returnASwitchProfilePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnASwitchProfilePort(null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.returnASwitchProfilePort('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.returnASwitchProfilePort('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.returnASwitchProfilePort('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateASwitchProfilePort - errors', () => {
      it('should have a updateASwitchProfilePort function', (done) => {
        try {
          assert.equal(true, typeof a.updateASwitchProfilePort === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateASwitchProfilePort(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.updateASwitchProfilePort('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileId', (done) => {
        try {
          a.updateASwitchProfilePort('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'profileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portId', (done) => {
        try {
          a.updateASwitchProfilePort('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'portId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateASwitchProfilePort('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASwitchProfilePort', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnSingleLANConfiguration - errors', () => {
      it('should have a returnSingleLANConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.returnSingleLANConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnSingleLANConfiguration(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnSingleLANConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSingleLANConfiguration - errors', () => {
      it('should have a updateSingleLANConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateSingleLANConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateSingleLANConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateSingleLANConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSingleLANConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateSingleLANConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3StaticRoutesForASwitch - errors', () => {
      it('should have a listLayer3StaticRoutesForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.listLayer3StaticRoutesForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.listLayer3StaticRoutesForASwitch(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3StaticRoutesForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createALayer3StaticRouteForASwitch - errors', () => {
      it('should have a createALayer3StaticRouteForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.createALayer3StaticRouteForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.createALayer3StaticRouteForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createALayer3StaticRouteForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3StaticRouteForASwitch - errors', () => {
      it('should have a returnALayer3StaticRouteForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3StaticRouteForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3StaticRouteForASwitch - errors', () => {
      it('should have a updateALayer3StaticRouteForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3StaticRouteForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3StaticRouteForASwitch - errors', () => {
      it('should have a deleteALayer3StaticRouteForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteALayer3StaticRouteForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3StaticRouteForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3StaticRoutesForASwitchStack - errors', () => {
      it('should have a listLayer3StaticRoutesForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.listLayer3StaticRoutesForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listLayer3StaticRoutesForASwitchStack(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3StaticRoutesForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.listLayer3StaticRoutesForASwitchStack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3StaticRoutesForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createALayer3StaticRouteForASwitchStack - errors', () => {
      it('should have a createALayer3StaticRouteForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.createALayer3StaticRouteForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createALayer3StaticRouteForASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.createALayer3StaticRouteForASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createALayer3StaticRouteForASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3StaticRouteForASwitchStack - errors', () => {
      it('should have a returnALayer3StaticRouteForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3StaticRouteForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.returnALayer3StaticRouteForASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3StaticRouteForASwitchStack - errors', () => {
      it('should have a updateALayer3StaticRouteForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3StaticRouteForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitchStack(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitchStack('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitchStack('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3StaticRouteForASwitchStack('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3StaticRouteForASwitchStack - errors', () => {
      it('should have a deleteALayer3StaticRouteForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteALayer3StaticRouteForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing staticRouteId', (done) => {
        try {
          a.deleteALayer3StaticRouteForASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'staticRouteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3StaticRouteForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllCustomPerformanceClassesForAnMXNetwork - errors', () => {
      it('should have a listAllCustomPerformanceClassesForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listAllCustomPerformanceClassesForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listAllCustomPerformanceClassesForAnMXNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAllCustomPerformanceClassesForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should have a addACustomPerformanceClassForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.addACustomPerformanceClassForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addACustomPerformanceClassForAnMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addACustomPerformanceClassForAnMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should have a returnACustomPerformanceClassForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnACustomPerformanceClassForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnACustomPerformanceClassForAnMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customPerformanceClassId', (done) => {
        try {
          a.returnACustomPerformanceClassForAnMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customPerformanceClassId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateACustomPerformanceClassForAnMXNetwork - errors', () => {
      it('should have a updateACustomPerformanceClassForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateACustomPerformanceClassForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateACustomPerformanceClassForAnMXNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customPerformanceClassId', (done) => {
        try {
          a.updateACustomPerformanceClassForAnMXNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'customPerformanceClassId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateACustomPerformanceClassForAnMXNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateACustomPerformanceClassForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteACustomPerformanceClassFromAnMXNetwork - errors', () => {
      it('should have a deleteACustomPerformanceClassFromAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteACustomPerformanceClassFromAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteACustomPerformanceClassFromAnMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteACustomPerformanceClassFromAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customPerformanceClassId', (done) => {
        try {
          a.deleteACustomPerformanceClassFromAnMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'customPerformanceClassId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteACustomPerformanceClassFromAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheTrafficShapingSettingsRulesForAnMXNetwork - errors', () => {
      it('should have a updateTheTrafficShapingSettingsRulesForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheTrafficShapingSettingsRulesForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheTrafficShapingSettingsRulesForAnMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheTrafficShapingSettingsRulesForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheTrafficShapingSettingsRulesForAnMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheTrafficShapingSettingsRulesForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#displayTheTrafficShapingSettingsRulesForAnMXNetwork - errors', () => {
      it('should have a displayTheTrafficShapingSettingsRulesForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.displayTheTrafficShapingSettingsRulesForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.displayTheTrafficShapingSettingsRulesForAnMXNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-displayTheTrafficShapingSettingsRulesForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showUplinkSelectionSettingsForAnMXNetwork - errors', () => {
      it('should have a showUplinkSelectionSettingsForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.showUplinkSelectionSettingsForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.showUplinkSelectionSettingsForAnMXNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-showUplinkSelectionSettingsForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateUplinkSelectionSettingsForAnMXNetwork - errors', () => {
      it('should have a updateUplinkSelectionSettingsForAnMXNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateUplinkSelectionSettingsForAnMXNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateUplinkSelectionSettingsForAnMXNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateUplinkSelectionSettingsForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateUplinkSelectionSettingsForAnMXNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateUplinkSelectionSettingsForAnMXNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAHubBGPConfiguration - errors', () => {
      it('should have a returnAHubBGPConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.returnAHubBGPConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAHubBGPConfiguration(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAHubBGPConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAHubBGPConfiguration - errors', () => {
      it('should have a updateAHubBGPConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateAHubBGPConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAHubBGPConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAHubBGPConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAHubBGPConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAHubBGPConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnWarmSpareConfigurationForASwitch - errors', () => {
      it('should have a returnWarmSpareConfigurationForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnWarmSpareConfigurationForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnWarmSpareConfigurationForASwitch(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnWarmSpareConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateWarmSpareConfigurationForASwitch - errors', () => {
      it('should have a updateWarmSpareConfigurationForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.updateWarmSpareConfigurationForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateWarmSpareConfigurationForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateWarmSpareConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateWarmSpareConfigurationForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateWarmSpareConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnoverviewstatisticsfornetworkclients - errors', () => {
      it('should have a returnoverviewstatisticsfornetworkclients function', (done) => {
        try {
          assert.equal(true, typeof a.returnoverviewstatisticsfornetworkclients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnoverviewstatisticsfornetworkclients(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnoverviewstatisticsfornetworkclients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization - errors', () => {
      it('should have a returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsummaryinformationaroundclientdatausageInmbAcrossthegivenorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnanoverviewofcurrentdevicestatuses - errors', () => {
      it('should have a returnanoverviewofcurrentdevicestatuses function', (done) => {
        try {
          assert.equal(true, typeof a.returnanoverviewofcurrentdevicestatuses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnanoverviewofcurrentdevicestatuses(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnanoverviewofcurrentdevicestatuses', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsadaptivepolicyaggregatestatisticsforanorganization - errors', () => {
      it('should have a returnsadaptivepolicyaggregatestatisticsforanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnsadaptivepolicyaggregatestatisticsforanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsadaptivepolicyaggregatestatisticsforanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsadaptivepolicyaggregatestatisticsforanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnanoverviewofthelicensestateforanorganization - errors', () => {
      it('should have a returnanoverviewofthelicensestateforanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnanoverviewofthelicensestateforanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnanoverviewofthelicensestateforanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnanoverviewofthelicensestateforanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp - errors', () => {
      it('should have a returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp function', (done) => {
        try {
          assert.equal(true, typeof a.returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnallreportedreadingsfromsensorsinagiventimespanSortedbytimestamp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listCustomAnalyticsArtifacts - errors', () => {
      it('should have a listCustomAnalyticsArtifacts function', (done) => {
        try {
          assert.equal(true, typeof a.listCustomAnalyticsArtifacts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listCustomAnalyticsArtifacts(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listCustomAnalyticsArtifacts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomAnalyticsArtifact - errors', () => {
      it('should have a createCustomAnalyticsArtifact function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomAnalyticsArtifact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createCustomAnalyticsArtifact(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCustomAnalyticsArtifact('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCustomAnalyticsArtifact - errors', () => {
      it('should have a getCustomAnalyticsArtifact function', (done) => {
        try {
          assert.equal(true, typeof a.getCustomAnalyticsArtifact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getCustomAnalyticsArtifact(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing artifactId', (done) => {
        try {
          a.getCustomAnalyticsArtifact('fakeparam', null, (data, error) => {
            try {
              const displayE = 'artifactId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomAnalyticsArtifact - errors', () => {
      it('should have a deleteCustomAnalyticsArtifact function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomAnalyticsArtifact === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteCustomAnalyticsArtifact(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing artifactId', (done) => {
        try {
          a.deleteCustomAnalyticsArtifact('fakeparam', null, (data, error) => {
            try {
              const displayE = 'artifactId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteCustomAnalyticsArtifact', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnCustomAnalyticsSettingsForACamera - errors', () => {
      it('should have a returnCustomAnalyticsSettingsForACamera function', (done) => {
        try {
          assert.equal(true, typeof a.returnCustomAnalyticsSettingsForACamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnCustomAnalyticsSettingsForACamera(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnCustomAnalyticsSettingsForACamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCustomAnalyticsSettingsForACamera - errors', () => {
      it('should have a updateCustomAnalyticsSettingsForACamera function', (done) => {
        try {
          assert.equal(true, typeof a.updateCustomAnalyticsSettingsForACamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateCustomAnalyticsSettingsForACamera(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateCustomAnalyticsSettingsForACamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCustomAnalyticsSettingsForACamera('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateCustomAnalyticsSettingsForACamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnstheMVSenseobjectdetectionmodellistforthegivencamera - errors', () => {
      it('should have a returnstheMVSenseobjectdetectionmodellistforthegivencamera function', (done) => {
        try {
          assert.equal(true, typeof a.returnstheMVSenseobjectdetectionmodellistforthegivencamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnstheMVSenseobjectdetectionmodellistforthegivencamera(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnstheMVSenseobjectdetectionmodellistforthegivencamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsSenseSettingsForAGivenCamera - errors', () => {
      it('should have a returnsSenseSettingsForAGivenCamera function', (done) => {
        try {
          assert.equal(true, typeof a.returnsSenseSettingsForAGivenCamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnsSenseSettingsForAGivenCamera(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsSenseSettingsForAGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSenseSettingsForTheGivenCamera - errors', () => {
      it('should have a updateSenseSettingsForTheGivenCamera function', (done) => {
        try {
          assert.equal(true, typeof a.updateSenseSettingsForTheGivenCamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateSenseSettingsForTheGivenCamera(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateSenseSettingsForTheGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateSenseSettingsForTheGivenCamera('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateSenseSettingsForTheGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsWirelessProfileAssignedToTheGivenCamera - errors', () => {
      it('should have a returnsWirelessProfileAssignedToTheGivenCamera function', (done) => {
        try {
          assert.equal(true, typeof a.returnsWirelessProfileAssignedToTheGivenCamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnsWirelessProfileAssignedToTheGivenCamera(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsWirelessProfileAssignedToTheGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignWirelessProfilesToTheGivenCamera - errors', () => {
      it('should have a assignWirelessProfilesToTheGivenCamera function', (done) => {
        try {
          assert.equal(true, typeof a.assignWirelessProfilesToTheGivenCamera === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.assignWirelessProfilesToTheGivenCamera(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-assignWirelessProfilesToTheGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.assignWirelessProfilesToTheGivenCamera('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-assignWirelessProfilesToTheGivenCamera', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createsANewCameraWirelessProfileForThisNetwork - errors', () => {
      it('should have a createsANewCameraWirelessProfileForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createsANewCameraWirelessProfileForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createsANewCameraWirelessProfileForThisNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsANewCameraWirelessProfileForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createsANewCameraWirelessProfileForThisNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsANewCameraWirelessProfileForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheCameraWirelessProfilesForThisNetwork - errors', () => {
      it('should have a listTheCameraWirelessProfilesForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listTheCameraWirelessProfilesForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheCameraWirelessProfilesForThisNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheCameraWirelessProfilesForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrieveASingleCameraWirelessProfile - errors', () => {
      it('should have a retrieveASingleCameraWirelessProfile function', (done) => {
        try {
          assert.equal(true, typeof a.retrieveASingleCameraWirelessProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.retrieveASingleCameraWirelessProfile(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-retrieveASingleCameraWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wirelessProfileId', (done) => {
        try {
          a.retrieveASingleCameraWirelessProfile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wirelessProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-retrieveASingleCameraWirelessProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnExistingCameraWirelessProfileInThisNetwork - errors', () => {
      it('should have a updateAnExistingCameraWirelessProfileInThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnExistingCameraWirelessProfileInThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAnExistingCameraWirelessProfileInThisNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnExistingCameraWirelessProfileInThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wirelessProfileId', (done) => {
        try {
          a.updateAnExistingCameraWirelessProfileInThisNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'wirelessProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnExistingCameraWirelessProfileInThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnExistingCameraWirelessProfileInThisNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnExistingCameraWirelessProfileInThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnExistingCameraWirelessProfileForThisNetwork - errors', () => {
      it('should have a deleteAnExistingCameraWirelessProfileForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnExistingCameraWirelessProfileForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAnExistingCameraWirelessProfileForThisNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnExistingCameraWirelessProfileForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wirelessProfileId', (done) => {
        try {
          a.deleteAnExistingCameraWirelessProfileForThisNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'wirelessProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnExistingCameraWirelessProfileForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheOutageScheduleForTheSSID - errors', () => {
      it('should have a listTheOutageScheduleForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.listTheOutageScheduleForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheOutageScheduleForTheSSID(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheOutageScheduleForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.listTheOutageScheduleForTheSSID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheOutageScheduleForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheOutageScheduleForTheSSID - errors', () => {
      it('should have a updateTheOutageScheduleForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheOutageScheduleForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheOutageScheduleForTheSSID(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheOutageScheduleForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateTheOutageScheduleForTheSSID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheOutageScheduleForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheOutageScheduleForTheSSID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheOutageScheduleForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceDHCPConfigurationForASwitch - errors', () => {
      it('should have a returnALayer3InterfaceDHCPConfigurationForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3InterfaceDHCPConfigurationForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceDHCPConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceDHCPConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3InterfaceDHCPConfigurationForASwitch - errors', () => {
      it('should have a updateALayer3InterfaceDHCPConfigurationForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3InterfaceDHCPConfigurationForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceDHCPConfigurationForASwitchStack - errors', () => {
      it('should have a returnALayer3InterfaceDHCPConfigurationForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3InterfaceDHCPConfigurationForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.returnALayer3InterfaceDHCPConfigurationForASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3InterfaceDHCPConfigurationForASwitchStack - errors', () => {
      it('should have a updateALayer3InterfaceDHCPConfigurationForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3InterfaceDHCPConfigurationForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitchStack(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitchStack('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitchStack('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3InterfaceDHCPConfigurationForASwitchStack('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceDHCPConfigurationForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheSwitchPortProfilesinanetwork - errors', () => {
      it('should have a listtheSwitchPortProfilesinanetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listtheSwitchPortProfilesinanetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listtheSwitchPortProfilesinanetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheSwitchPortProfilesinanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallprofilesinanetwork - errors', () => {
      it('should have a listallprofilesinanetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listallprofilesinanetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listallprofilesinanetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listallprofilesinanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnOrganizationWideAlertConfiguration - errors', () => {
      it('should have a createAnOrganizationWideAlertConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createAnOrganizationWideAlertConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createAnOrganizationWideAlertConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnOrganizationWideAlertConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAnOrganizationWideAlertConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnOrganizationWideAlertConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllOrganizationWideAlertConfigurations - errors', () => {
      it('should have a listAllOrganizationWideAlertConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.listAllOrganizationWideAlertConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listAllOrganizationWideAlertConfigurations(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAllOrganizationWideAlertConfigurations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removesAnOrganizationWideAlertConfig - errors', () => {
      it('should have a removesAnOrganizationWideAlertConfig function', (done) => {
        try {
          assert.equal(true, typeof a.removesAnOrganizationWideAlertConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.removesAnOrganizationWideAlertConfig(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removesAnOrganizationWideAlertConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alertConfigId', (done) => {
        try {
          a.removesAnOrganizationWideAlertConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alertConfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removesAnOrganizationWideAlertConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnOrganizationWideAlertConfig - errors', () => {
      it('should have a updateAnOrganizationWideAlertConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnOrganizationWideAlertConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateAnOrganizationWideAlertConfig(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnOrganizationWideAlertConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alertConfigId', (done) => {
        try {
          a.updateAnOrganizationWideAlertConfig('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'alertConfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnOrganizationWideAlertConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnOrganizationWideAlertConfig('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnOrganizationWideAlertConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheswitchportsinanorganizationbyswitch - errors', () => {
      it('should have a listtheswitchportsinanorganizationbyswitch function', (done) => {
        try {
          assert.equal(true, typeof a.listtheswitchportsinanorganizationbyswitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listtheswitchportsinanorganizationbyswitch(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheswitchportsinanorganizationbyswitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3InterfacesForASwitch - errors', () => {
      it('should have a listLayer3InterfacesForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.listLayer3InterfacesForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.listLayer3InterfacesForASwitch(null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3InterfacesForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createALayer3InterfaceForASwitch - errors', () => {
      it('should have a createALayer3InterfaceForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.createALayer3InterfaceForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.createALayer3InterfaceForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createALayer3InterfaceForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceForASwitch - errors', () => {
      it('should have a returnALayer3InterfaceForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3InterfaceForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnALayer3InterfaceForASwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.returnALayer3InterfaceForASwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3InterfaceForASwitch - errors', () => {
      it('should have a updateALayer3InterfaceForASwitch function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3InterfaceForASwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.updateALayer3InterfaceForASwitch(null, null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.updateALayer3InterfaceForASwitch('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3InterfaceForASwitch('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3InterfaceFromTheSwitch - errors', () => {
      it('should have a deleteALayer3InterfaceFromTheSwitch function', (done) => {
        try {
          assert.equal(true, typeof a.deleteALayer3InterfaceFromTheSwitch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.deleteALayer3InterfaceFromTheSwitch(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3InterfaceFromTheSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.deleteALayer3InterfaceFromTheSwitch('fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3InterfaceFromTheSwitch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listLayer3InterfacesForASwitchStack - errors', () => {
      it('should have a listLayer3InterfacesForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.listLayer3InterfacesForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listLayer3InterfacesForASwitchStack(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3InterfacesForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.listLayer3InterfacesForASwitchStack('fakeparam', null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listLayer3InterfacesForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createALayer3InterfaceForASwitchStack - errors', () => {
      it('should have a createALayer3InterfaceForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.createALayer3InterfaceForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createALayer3InterfaceForASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.createALayer3InterfaceForASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createALayer3InterfaceForASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnALayer3InterfaceFromASwitchStack - errors', () => {
      it('should have a returnALayer3InterfaceFromASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.returnALayer3InterfaceFromASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnALayer3InterfaceFromASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.returnALayer3InterfaceFromASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.returnALayer3InterfaceFromASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateALayer3InterfaceForASwitchStack - errors', () => {
      it('should have a updateALayer3InterfaceForASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.updateALayer3InterfaceForASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateALayer3InterfaceForASwitchStack(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.updateALayer3InterfaceForASwitchStack('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.updateALayer3InterfaceForASwitchStack('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateALayer3InterfaceForASwitchStack('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateALayer3InterfaceForASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteALayer3InterfaceFromASwitchStack - errors', () => {
      it('should have a deleteALayer3InterfaceFromASwitchStack function', (done) => {
        try {
          assert.equal(true, typeof a.deleteALayer3InterfaceFromASwitchStack === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteALayer3InterfaceFromASwitchStack(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing switchStackId', (done) => {
        try {
          a.deleteALayer3InterfaceFromASwitchStack('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'switchStackId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceId', (done) => {
        try {
          a.deleteALayer3InterfaceFromASwitchStack('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteALayer3InterfaceFromASwitchStack', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listMulticastRendezvousPoints - errors', () => {
      it('should have a listMulticastRendezvousPoints function', (done) => {
        try {
          assert.equal(true, typeof a.listMulticastRendezvousPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listMulticastRendezvousPoints(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listMulticastRendezvousPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAMulticastRendezvousPoint - errors', () => {
      it('should have a createAMulticastRendezvousPoint function', (done) => {
        try {
          assert.equal(true, typeof a.createAMulticastRendezvousPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createAMulticastRendezvousPoint(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAMulticastRendezvousPoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAMulticastRendezvousPoint - errors', () => {
      it('should have a returnAMulticastRendezvousPoint function', (done) => {
        try {
          assert.equal(true, typeof a.returnAMulticastRendezvousPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAMulticastRendezvousPoint(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rendezvousPointId', (done) => {
        try {
          a.returnAMulticastRendezvousPoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'rendezvousPointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAMulticastRendezvousPoint - errors', () => {
      it('should have a deleteAMulticastRendezvousPoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAMulticastRendezvousPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAMulticastRendezvousPoint(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rendezvousPointId', (done) => {
        try {
          a.deleteAMulticastRendezvousPoint('fakeparam', null, (data, error) => {
            try {
              const displayE = 'rendezvousPointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAMulticastRendezvousPoint - errors', () => {
      it('should have a updateAMulticastRendezvousPoint function', (done) => {
        try {
          assert.equal(true, typeof a.updateAMulticastRendezvousPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAMulticastRendezvousPoint(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rendezvousPointId', (done) => {
        try {
          a.updateAMulticastRendezvousPoint('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rendezvousPointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAMulticastRendezvousPoint('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAMulticastRendezvousPoint', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnMulticastSettingsForANetwork - errors', () => {
      it('should have a returnMulticastSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnMulticastSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnMulticastSettingsForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnMulticastSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMulticastSettingsForANetwork - errors', () => {
      it('should have a updateMulticastSettingsForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateMulticastSettingsForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateMulticastSettingsForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateMulticastSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateMulticastSettingsForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateMulticastSettingsForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnLayer3OSPFRoutingConfiguration - errors', () => {
      it('should have a returnLayer3OSPFRoutingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.returnLayer3OSPFRoutingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnLayer3OSPFRoutingConfiguration(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnLayer3OSPFRoutingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateLayer3OSPFRoutingConfiguration - errors', () => {
      it('should have a updateLayer3OSPFRoutingConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.updateLayer3OSPFRoutingConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateLayer3OSPFRoutingConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateLayer3OSPFRoutingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateLayer3OSPFRoutingConfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateLayer3OSPFRoutingConfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheSwitchAlternateManagementInterfaceForTheNetwork - errors', () => {
      it('should have a returnTheSwitchAlternateManagementInterfaceForTheNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheSwitchAlternateManagementInterfaceForTheNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheSwitchAlternateManagementInterfaceForTheNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheSwitchAlternateManagementInterfaceForTheNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheSwitchAlternateManagementInterfaceForTheNetwork - errors', () => {
      it('should have a updateTheSwitchAlternateManagementInterfaceForTheNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheSwitchAlternateManagementInterfaceForTheNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheSwitchAlternateManagementInterfaceForTheNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSwitchAlternateManagementInterfaceForTheNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheSwitchAlternateManagementInterfaceForTheNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheSwitchAlternateManagementInterfaceForTheNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAlternateManagementInterfaceAndDevicesWithIPAssigned - errors', () => {
      it('should have a returnAlternateManagementInterfaceAndDevicesWithIPAssigned function', (done) => {
        try {
          assert.equal(true, typeof a.returnAlternateManagementInterfaceAndDevicesWithIPAssigned === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAlternateManagementInterfaceAndDevicesWithIPAssigned(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAlternateManagementInterfaceAndDevicesWithIPAssigned', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAlternateManagementInterfaceAndDeviceStaticIP - errors', () => {
      it('should have a updateAlternateManagementInterfaceAndDeviceStaticIP function', (done) => {
        try {
          assert.equal(true, typeof a.updateAlternateManagementInterfaceAndDeviceStaticIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAlternateManagementInterfaceAndDeviceStaticIP(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAlternateManagementInterfaceAndDeviceStaticIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAlternateManagementInterfaceAndDeviceStaticIP('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAlternateManagementInterfaceAndDeviceStaticIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day - errors', () => {
      it('should have a returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day function', (done) => {
        try {
          assert.equal(true, typeof a.returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnthenetworkSDHCPv4serversseenwithintheselectedtimeframeDefault1day', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should have a returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheListOfServersTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should have a addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAServerToBeTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should have a updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedServerId', (done) => {
        try {
          a.updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'trustedServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAServerThatIsTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork - errors', () => {
      it('should have a removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trustedServerId', (done) => {
        try {
          a.removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'trustedServerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeAServerFromBeingTrustedByDynamicARPInspectionOnThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings - errors', () => {
      it('should have a returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings function', (done) => {
        try {
          assert.equal(true, typeof a.returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnthedevicesthathaveaDynamicARPInspectionwarningandtheirwarnings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthepowerstatusinformationfordevicesinanorganization - errors', () => {
      it('should have a listthepowerstatusinformationfordevicesinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listthepowerstatusinformationfordevicesinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listthepowerstatusinformationfordevicesinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listthepowerstatusinformationfordevicesinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthecurrentuplinkaddressesfordevicesinanorganization - errors', () => {
      it('should have a listthecurrentuplinkaddressesfordevicesinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listthecurrentuplinkaddressesfordevicesinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listthecurrentuplinkaddressesfordevicesinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listthecurrentuplinkaddressesfordevicesinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches - errors', () => {
      it('should have a cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches function', (done) => {
        try {
          assert.equal(true, typeof a.cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-cloneportLevelandsomeswitchLevelconfigurationsettingsfromasourceswitchtooneormoretargetswitches', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASingleDeviceFromTheInventoryOfAnOrganization - errors', () => {
      it('should have a returnASingleDeviceFromTheInventoryOfAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnASingleDeviceFromTheInventoryOfAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnASingleDeviceFromTheInventoryOfAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASingleDeviceFromTheInventoryOfAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnASingleDeviceFromTheInventoryOfAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASingleDeviceFromTheInventoryOfAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnTheBillingSettingsOfThisNetwork - errors', () => {
      it('should have a returnTheBillingSettingsOfThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnTheBillingSettingsOfThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnTheBillingSettingsOfThisNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnTheBillingSettingsOfThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheBillingSettings - errors', () => {
      it('should have a updateTheBillingSettings function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheBillingSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheBillingSettings(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBillingSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheBillingSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBillingSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheBonjourForwardingSettingAndRulesForTheSSID - errors', () => {
      it('should have a listTheBonjourForwardingSettingAndRulesForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.listTheBonjourForwardingSettingAndRulesForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheBonjourForwardingSettingAndRulesForTheSSID(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheBonjourForwardingSettingAndRulesForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.listTheBonjourForwardingSettingAndRulesForTheSSID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheBonjourForwardingSettingAndRulesForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheBonjourForwardingSettingAndRulesForTheSSID - errors', () => {
      it('should have a updateTheBonjourForwardingSettingAndRulesForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheBonjourForwardingSettingAndRulesForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheBonjourForwardingSettingAndRulesForTheSSID(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBonjourForwardingSettingAndRulesForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateTheBonjourForwardingSettingAndRulesForTheSSID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBonjourForwardingSettingAndRulesForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheBonjourForwardingSettingAndRulesForTheSSID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheBonjourForwardingSettingAndRulesForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAllIdentityPSKsInAWirelessNetwork - errors', () => {
      it('should have a listAllIdentityPSKsInAWirelessNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listAllIdentityPSKsInAWirelessNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listAllIdentityPSKsInAWirelessNetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAllIdentityPSKsInAWirelessNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.listAllIdentityPSKsInAWirelessNetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAllIdentityPSKsInAWirelessNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAnIdentityPSK - errors', () => {
      it('should have a createAnIdentityPSK function', (done) => {
        try {
          assert.equal(true, typeof a.createAnIdentityPSK === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createAnIdentityPSK(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.createAnIdentityPSK('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAnIdentityPSK('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnIdentityPSK - errors', () => {
      it('should have a returnAnIdentityPSK function', (done) => {
        try {
          assert.equal(true, typeof a.returnAnIdentityPSK === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAnIdentityPSK(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.returnAnIdentityPSK('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityPskId', (done) => {
        try {
          a.returnAnIdentityPSK('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'identityPskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnIdentityPSK - errors', () => {
      it('should have a updateAnIdentityPSK function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnIdentityPSK === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAnIdentityPSK(null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateAnIdentityPSK('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityPskId', (done) => {
        try {
          a.updateAnIdentityPSK('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'identityPskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnIdentityPSK('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnIdentityPSK - errors', () => {
      it('should have a deleteAnIdentityPSK function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnIdentityPSK === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAnIdentityPSK(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.deleteAnIdentityPSK('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing identityPskId', (done) => {
        try {
          a.deleteAnIdentityPSK('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'identityPskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnIdentityPSK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheVPNSettingsForTheSSID - errors', () => {
      it('should have a listTheVPNSettingsForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.listTheVPNSettingsForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheVPNSettingsForTheSSID(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheVPNSettingsForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.listTheVPNSettingsForTheSSID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheVPNSettingsForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheVPNSettingsForTheSSID - errors', () => {
      it('should have a updateTheVPNSettingsForTheSSID function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheVPNSettingsForTheSSID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateTheVPNSettingsForTheSSID(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheVPNSettingsForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing number', (done) => {
        try {
          a.updateTheVPNSettingsForTheSSID('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'number is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheVPNSettingsForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheVPNSettingsForTheSSID('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheVPNSettingsForTheSSID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPchannelutilizationovertimeforadeviceornetworkclient - errors', () => {
      it('should have a returnAPchannelutilizationovertimeforadeviceornetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnAPchannelutilizationovertimeforadeviceornetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAPchannelutilizationovertimeforadeviceornetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPchannelutilizationovertimeforadeviceornetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should have a returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnwirelessclientcountsovertimeforanetworkDeviceOrnetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan - errors', () => {
      it('should have a listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan function', (done) => {
        try {
          assert.equal(true, typeof a.listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientId', (done) => {
        try {
          a.listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan('fakeparam', null, (data, error) => {
            try {
              const displayE = 'clientId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listthewirelessconnectivityeventsforaclientwithinanetworkinthetimespan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should have a returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnaveragewirelesslatencyovertimeforanetworkDeviceOrnetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient - errors', () => {
      it('should have a returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnPHYdataratesovertimeforanetworkDeviceOrnetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listwirelessmeshstatusesforrepeaters - errors', () => {
      it('should have a listwirelessmeshstatusesforrepeaters function', (done) => {
        try {
          assert.equal(true, typeof a.listwirelessmeshstatusesforrepeaters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listwirelessmeshstatusesforrepeaters(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listwirelessmeshstatusesforrepeaters', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient - errors', () => {
      it('should have a returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient function', (done) => {
        try {
          assert.equal(true, typeof a.returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsignalqualitySNRRSSIOvertimeforadeviceornetworkclient', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getapplicationhealthbytime - errors', () => {
      it('should have a getapplicationhealthbytime function', (done) => {
        try {
          assert.equal(true, typeof a.getapplicationhealthbytime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getapplicationhealthbytime(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getapplicationhealthbytime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applicationId', (done) => {
        try {
          a.getapplicationhealthbytime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applicationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getapplicationhealthbytime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listallInsighttrackedapplications - errors', () => {
      it('should have a listallInsighttrackedapplications function', (done) => {
        try {
          assert.equal(true, typeof a.listallInsighttrackedapplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listallInsighttrackedapplications(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listallInsighttrackedapplications', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial - errors', () => {
      it('should have a returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial function', (done) => {
        try {
          assert.equal(true, typeof a.returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnthelatestavailablereadingforeachmetricfromeachsensorSortedbysensorserial', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTrustedAccessConfigs - errors', () => {
      it('should have a listTrustedAccessConfigs function', (done) => {
        try {
          assert.equal(true, typeof a.listTrustedAccessConfigs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTrustedAccessConfigs(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTrustedAccessConfigs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUserAccessDevicesAndItsTrustedAccessConnections - errors', () => {
      it('should have a listUserAccessDevicesAndItsTrustedAccessConnections function', (done) => {
        try {
          assert.equal(true, typeof a.listUserAccessDevicesAndItsTrustedAccessConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listUserAccessDevicesAndItsTrustedAccessConnections(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listUserAccessDevicesAndItsTrustedAccessConnections', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAUserAccessDevice - errors', () => {
      it('should have a deleteAUserAccessDevice function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAUserAccessDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAUserAccessDevice(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAUserAccessDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userAccessDeviceId', (done) => {
        try {
          a.deleteAUserAccessDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userAccessDeviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAUserAccessDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gettheorganizationSAPNScertificate - errors', () => {
      it('should have a gettheorganizationSAPNScertificate function', (done) => {
        try {
          assert.equal(true, typeof a.gettheorganizationSAPNScertificate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.gettheorganizationSAPNScertificate(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-gettheorganizationSAPNScertificate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheVPPAccountsInTheOrganization - errors', () => {
      it('should have a listTheVPPAccountsInTheOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listTheVPPAccountsInTheOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listTheVPPAccountsInTheOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheVPPAccountsInTheOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID - errors', () => {
      it('should have a getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID function', (done) => {
        try {
          assert.equal(true, typeof a.getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vppAccountId', (done) => {
        try {
          a.getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vppAccountId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getAHashContainingTheUnparsedTokenOfTheVPPAccountWithTheGivenID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enqueueAJobToPingATargetHostFromTheDevice - errors', () => {
      it('should have a enqueueAJobToPingATargetHostFromTheDevice function', (done) => {
        try {
          assert.equal(true, typeof a.enqueueAJobToPingATargetHostFromTheDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.enqueueAJobToPingATargetHostFromTheDevice(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enqueueAJobToPingATargetHostFromTheDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.enqueueAJobToPingATargetHostFromTheDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enqueueAJobToPingATargetHostFromTheDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPingJob - errors', () => {
      it('should have a returnAPingJob function', (done) => {
        try {
          assert.equal(true, typeof a.returnAPingJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnAPingJob(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPingJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnAPingJob('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPingJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enqueueAJobToCheckConnectivityStatusToTheDevice - errors', () => {
      it('should have a enqueueAJobToCheckConnectivityStatusToTheDevice function', (done) => {
        try {
          assert.equal(true, typeof a.enqueueAJobToCheckConnectivityStatusToTheDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.enqueueAJobToCheckConnectivityStatusToTheDevice(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enqueueAJobToCheckConnectivityStatusToTheDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.enqueueAJobToCheckConnectivityStatusToTheDevice('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-enqueueAJobToCheckConnectivityStatusToTheDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAPingDeviceJob - errors', () => {
      it('should have a returnAPingDeviceJob function', (done) => {
        try {
          assert.equal(true, typeof a.returnAPingDeviceJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serial', (done) => {
        try {
          a.returnAPingDeviceJob(null, null, (data, error) => {
            try {
              const displayE = 'serial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPingDeviceJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnAPingDeviceJob('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAPingDeviceJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimavMXintoanetwork - errors', () => {
      it('should have a claimavMXintoanetwork function', (done) => {
        try {
          assert.equal(true, typeof a.claimavMXintoanetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.claimavMXintoanetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimavMXintoanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.claimavMXintoanetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimavMXintoanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rollbackaFirmwareUpgradeForANetwork - errors', () => {
      it('should have a rollbackaFirmwareUpgradeForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.rollbackaFirmwareUpgradeForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.rollbackaFirmwareUpgradeForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-rollbackaFirmwareUpgradeForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.rollbackaFirmwareUpgradeForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-rollbackaFirmwareUpgradeForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFirmwareUpgradeInformationForANetwork - errors', () => {
      it('should have a getFirmwareUpgradeInformationForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getFirmwareUpgradeInformationForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getFirmwareUpgradeInformationForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getFirmwareUpgradeInformationForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateFirmwareUpgradeInformationForANetwork - errors', () => {
      it('should have a updateFirmwareUpgradeInformationForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateFirmwareUpgradeInformationForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateFirmwareUpgradeInformationForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateFirmwareUpgradeInformationForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateFirmwareUpgradeInformationForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateFirmwareUpgradeInformationForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnallglobalalertsonthisnetwork - errors', () => {
      it('should have a returnallglobalalertsonthisnetwork function', (done) => {
        try {
          assert.equal(true, typeof a.returnallglobalalertsonthisnetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnallglobalalertsonthisnetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnallglobalalertsonthisnetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap - errors', () => {
      it('should have a authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-authorizeAUserConfiguredWithMerakiAuthenticationForANetworkCurrentlySupports8021XSplashGuestAndClientVPNUsersAndCurrentlyOrganizationsHaveA50000UserCap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deauthorizeAUser - errors', () => {
      it('should have a deauthorizeAUser function', (done) => {
        try {
          assert.equal(true, typeof a.deauthorizeAUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deauthorizeAUser(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deauthorizeAUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing merakiAuthUserId', (done) => {
        try {
          a.deauthorizeAUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'merakiAuthUserId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deauthorizeAUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated - errors', () => {
      it('should have a updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated function', (done) => {
        try {
          assert.equal(true, typeof a.updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing merakiAuthUserId', (done) => {
        try {
          a.updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'merakiAuthUserId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAUserConfiguredWithMerakiAuthenticationCurrently8021XRADIUSSplashGuestAndClientVPNUsersCanBeUpdated', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAnMQTTBroker - errors', () => {
      it('should have a addAnMQTTBroker function', (done) => {
        try {
          assert.equal(true, typeof a.addAnMQTTBroker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.addAnMQTTBroker(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAnMQTTBroker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheMQTTBrokersForThisNetwork - errors', () => {
      it('should have a listTheMQTTBrokersForThisNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listTheMQTTBrokersForThisNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheMQTTBrokersForThisNetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheMQTTBrokersForThisNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnMQTTBroker - errors', () => {
      it('should have a deleteAnMQTTBroker function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnMQTTBroker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.deleteAnMQTTBroker(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mqttBrokerId', (done) => {
        try {
          a.deleteAnMQTTBroker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mqttBrokerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnMQTTBroker - errors', () => {
      it('should have a returnAnMQTTBroker function', (done) => {
        try {
          assert.equal(true, typeof a.returnAnMQTTBroker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnAnMQTTBroker(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mqttBrokerId', (done) => {
        try {
          a.returnAnMQTTBroker('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mqttBrokerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnMQTTBroker - errors', () => {
      it('should have a updateAnMQTTBroker function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnMQTTBroker === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAnMQTTBroker(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mqttBrokerId', (done) => {
        try {
          a.updateAnMQTTBroker('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mqttBrokerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnMQTTBroker('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnMQTTBroker', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getpoliciesforallclientswithpolicies - errors', () => {
      it('should have a getpoliciesforallclientswithpolicies function', (done) => {
        try {
          assert.equal(true, typeof a.getpoliciesforallclientswithpolicies === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getpoliciesforallclientswithpolicies(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getpoliciesforallclientswithpolicies', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listofdevicesandconnectionsamongthemwithinthenetwork - errors', () => {
      it('should have a listofdevicesandconnectionsamongthemwithinthenetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listofdevicesandconnectionsamongthemwithinthenetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listofdevicesandconnectionsamongthemwithinthenetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listofdevicesandconnectionsamongthemwithinthenetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should have a createAWebhookPayloadTemplateForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createAWebhookPayloadTemplateForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.createAWebhookPayloadTemplateForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAWebhookPayloadTemplateForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheWebhookPayloadTemplatesForANetwork - errors', () => {
      it('should have a listTheWebhookPayloadTemplatesForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.listTheWebhookPayloadTemplatesForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.listTheWebhookPayloadTemplatesForANetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheWebhookPayloadTemplatesForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#destroyAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should have a destroyAWebhookPayloadTemplateForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.destroyAWebhookPayloadTemplateForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.destroyAWebhookPayloadTemplateForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-destroyAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payloadTemplateId', (done) => {
        try {
          a.destroyAWebhookPayloadTemplateForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payloadTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-destroyAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTheWebhookPayloadTemplateForANetwork - errors', () => {
      it('should have a getTheWebhookPayloadTemplateForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getTheWebhookPayloadTemplateForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getTheWebhookPayloadTemplateForANetwork(null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getTheWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payloadTemplateId', (done) => {
        try {
          a.getTheWebhookPayloadTemplateForANetwork('fakeparam', null, (data, error) => {
            try {
              const displayE = 'payloadTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getTheWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAWebhookPayloadTemplateForANetwork - errors', () => {
      it('should have a updateAWebhookPayloadTemplateForANetwork function', (done) => {
        try {
          assert.equal(true, typeof a.updateAWebhookPayloadTemplateForANetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.updateAWebhookPayloadTemplateForANetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing payloadTemplateId', (done) => {
        try {
          a.updateAWebhookPayloadTemplateForANetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'payloadTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAWebhookPayloadTemplateForANetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAWebhookPayloadTemplateForANetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheapplicationusagedataforclients - errors', () => {
      it('should have a returntheapplicationusagedataforclients function', (done) => {
        try {
          assert.equal(true, typeof a.returntheapplicationusagedataforclients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returntheapplicationusagedataforclients(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returntheapplicationusagedataforclients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond - errors', () => {
      it('should have a returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond function', (done) => {
        try {
          assert.equal(true, typeof a.returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsatimeseriesoftotaltrafficconsumptionratesforallclientsonanetworkwithinagiventimespanInmegabitspersecond', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange - errors', () => {
      it('should have a returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returndatausageInmegabitspersecondOvertimeforallclientsinthegivenorganizationwithinagiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheusagehistoriesforclients - errors', () => {
      it('should have a returntheusagehistoriesforclients function', (done) => {
        try {
          assert.equal(true, typeof a.returntheusagehistoriesforclients === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.returntheusagehistoriesforclients(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returntheusagehistoriesforclients', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getthechannelutilizationovereachradioforallAPsinanetwork - errors', () => {
      it('should have a getthechannelutilizationovereachradioforallAPsinanetwork function', (done) => {
        try {
          assert.equal(true, typeof a.getthechannelutilizationovereachradioforallAPsinanetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkId', (done) => {
        try {
          a.getthechannelutilizationovereachradioforallAPsinanetwork(null, (data, error) => {
            try {
              const displayE = 'networkId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getthechannelutilizationovereachradioforallAPsinanetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createsNewAdaptivePolicyACL - errors', () => {
      it('should have a createsNewAdaptivePolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.createsNewAdaptivePolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createsNewAdaptivePolicyACL(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsNewAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createsNewAdaptivePolicyACL('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsNewAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePolicyACLsInAOrganization - errors', () => {
      it('should have a listAdaptivePolicyACLsInAOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listAdaptivePolicyACLsInAOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listAdaptivePolicyACLsInAOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAdaptivePolicyACLsInAOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesTheSpecifiedAdaptivePolicyACL - errors', () => {
      it('should have a deletesTheSpecifiedAdaptivePolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.deletesTheSpecifiedAdaptivePolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyACL(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deletesTheSpecifiedAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclId', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyACL('fakeparam', null, (data, error) => {
            try {
              const displayE = 'aclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deletesTheSpecifiedAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheAdaptivePolicyACLInformation - errors', () => {
      it('should have a returnsTheAdaptivePolicyACLInformation function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheAdaptivePolicyACLInformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsTheAdaptivePolicyACLInformation(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheAdaptivePolicyACLInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclId', (done) => {
        try {
          a.returnsTheAdaptivePolicyACLInformation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'aclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheAdaptivePolicyACLInformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatesAnAdaptivePolicyACL - errors', () => {
      it('should have a updatesAnAdaptivePolicyACL function', (done) => {
        try {
          assert.equal(true, typeof a.updatesAnAdaptivePolicyACL === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updatesAnAdaptivePolicyACL(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing aclId', (done) => {
        try {
          a.updatesAnAdaptivePolicyACL('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'aclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatesAnAdaptivePolicyACL('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyACL', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createsANewAdaptivePolicyGroup - errors', () => {
      it('should have a createsANewAdaptivePolicyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createsANewAdaptivePolicyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createsANewAdaptivePolicyGroup(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsANewAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createsANewAdaptivePolicyGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createsANewAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePolicyGroupsInAOrganization - errors', () => {
      it('should have a listAdaptivePolicyGroupsInAOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listAdaptivePolicyGroupsInAOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listAdaptivePolicyGroupsInAOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAdaptivePolicyGroupsInAOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences - errors', () => {
      it('should have a deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences function', (done) => {
        try {
          assert.equal(true, typeof a.deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deletesTheSpecifiedAdaptivePolicyGroupAndAnyAssociatedPoliciesAndReferences', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsAnAdaptivePolicyGroup - errors', () => {
      it('should have a returnsAnAdaptivePolicyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.returnsAnAdaptivePolicyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsAnAdaptivePolicyGroup(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsAnAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnsAnAdaptivePolicyGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsAnAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatesAnAdaptivePolicyGroup - errors', () => {
      it('should have a updatesAnAdaptivePolicyGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updatesAnAdaptivePolicyGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updatesAnAdaptivePolicyGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updatesAnAdaptivePolicyGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatesAnAdaptivePolicyGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesAnAdaptivePolicyGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addAnAdaptivePolicy - errors', () => {
      it('should have a addAnAdaptivePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addAnAdaptivePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.addAnAdaptivePolicy(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addAnAdaptivePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-addAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAdaptivePoliciesInAnOrganization - errors', () => {
      it('should have a listAdaptivePoliciesInAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listAdaptivePoliciesInAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listAdaptivePoliciesInAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listAdaptivePoliciesInAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnAdaptivePolicy - errors', () => {
      it('should have a deleteAnAdaptivePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnAdaptivePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteAnAdaptivePolicy(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteAnAdaptivePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnAnAdaptivePolicy - errors', () => {
      it('should have a returnAnAdaptivePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.returnAnAdaptivePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnAnAdaptivePolicy(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.returnAnAdaptivePolicy('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnAdaptivePolicy - errors', () => {
      it('should have a updateAnAdaptivePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnAdaptivePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateAnAdaptivePolicy(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateAnAdaptivePolicy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnAdaptivePolicy('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnAdaptivePolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returntheclientdetailsinanorganization - errors', () => {
      it('should have a returntheclientdetailsinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.returntheclientdetailsinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returntheclientdetailsinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returntheclientdetailsinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createANewConfigurationTemplate - errors', () => {
      it('should have a createANewConfigurationTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createANewConfigurationTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createANewConfigurationTemplate(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createANewConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createANewConfigurationTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createANewConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnASingleConfigurationTemplate - errors', () => {
      it('should have a returnASingleConfigurationTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.returnASingleConfigurationTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnASingleConfigurationTemplate(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASingleConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.returnASingleConfigurationTemplate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnASingleConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAConfigurationTemplate - errors', () => {
      it('should have a updateAConfigurationTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.updateAConfigurationTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateAConfigurationTemplate(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configTemplateId', (done) => {
        try {
          a.updateAConfigurationTemplate('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'configTemplateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAConfigurationTemplate('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAConfigurationTemplate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createANewEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should have a createANewEarlyAccessFeatureOptInForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.createANewEarlyAccessFeatureOptInForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createANewEarlyAccessFeatureOptInForAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createANewEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createANewEarlyAccessFeatureOptInForAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createANewEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheEarlyAccessFeatureOptInsForAnOrganization - errors', () => {
      it('should have a listTheEarlyAccessFeatureOptInsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listTheEarlyAccessFeatureOptInsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listTheEarlyAccessFeatureOptInsForAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheEarlyAccessFeatureOptInsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAnEarlyAccessFeatureOptIn - errors', () => {
      it('should have a deleteAnEarlyAccessFeatureOptIn function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAnEarlyAccessFeatureOptIn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.deleteAnEarlyAccessFeatureOptIn(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnEarlyAccessFeatureOptIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optInId', (done) => {
        try {
          a.deleteAnEarlyAccessFeatureOptIn('fakeparam', null, (data, error) => {
            try {
              const displayE = 'optInId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-deleteAnEarlyAccessFeatureOptIn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#showAnEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should have a showAnEarlyAccessFeatureOptInForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.showAnEarlyAccessFeatureOptInForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.showAnEarlyAccessFeatureOptInForAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-showAnEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optInId', (done) => {
        try {
          a.showAnEarlyAccessFeatureOptInForAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'optInId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-showAnEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateAnEarlyAccessFeatureOptInForAnOrganization - errors', () => {
      it('should have a updateAnEarlyAccessFeatureOptInForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateAnEarlyAccessFeatureOptInForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateAnEarlyAccessFeatureOptInForAnOrganization(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optInId', (done) => {
        try {
          a.updateAnEarlyAccessFeatureOptInForAnOrganization('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'optInId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateAnEarlyAccessFeatureOptInForAnOrganization('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateAnEarlyAccessFeatureOptInForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheAvailableEarlyAccessFeaturesForOrganization - errors', () => {
      it('should have a listTheAvailableEarlyAccessFeaturesForOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listTheAvailableEarlyAccessFeaturesForOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listTheAvailableEarlyAccessFeaturesForOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheAvailableEarlyAccessFeaturesForOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory - errors', () => {
      it('should have a claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory function', (done) => {
        try {
          assert.equal(true, typeof a.claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-claimAListOfDevicesLicensesAndOrOrdersIntoAnOrganizationInventory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseAListOfClaimedDevicesFromAnOrganization - errors', () => {
      it('should have a releaseAListOfClaimedDevicesFromAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.releaseAListOfClaimedDevicesFromAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.releaseAListOfClaimedDevicesFromAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-releaseAListOfClaimedDevicesFromAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.releaseAListOfClaimedDevicesFromAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-releaseAListOfClaimedDevicesFromAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheLoginSecuritySettingsForAnOrganization - errors', () => {
      it('should have a returnsTheLoginSecuritySettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheLoginSecuritySettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsTheLoginSecuritySettingsForAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheLoginSecuritySettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTheLoginSecuritySettingsForAnOrganization - errors', () => {
      it('should have a updateTheLoginSecuritySettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateTheLoginSecuritySettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateTheLoginSecuritySettingsForAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheLoginSecuritySettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateTheLoginSecuritySettingsForAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateTheLoginSecuritySettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createASAMLIdPForYourOrganization - errors', () => {
      it('should have a createASAMLIdPForYourOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.createASAMLIdPForYourOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.createASAMLIdPForYourOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createASAMLIdPForYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createASAMLIdPForYourOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-createASAMLIdPForYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listTheSAMLIdPsInYourOrganization - errors', () => {
      it('should have a listTheSAMLIdPsInYourOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.listTheSAMLIdPsInYourOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listTheSAMLIdPsInYourOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listTheSAMLIdPsInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getASAMLIdPFromYourOrganization - errors', () => {
      it('should have a getASAMLIdPFromYourOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.getASAMLIdPFromYourOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.getASAMLIdPFromYourOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getASAMLIdPFromYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.getASAMLIdPFromYourOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-getASAMLIdPFromYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeASAMLIdPInYourOrganization - errors', () => {
      it('should have a removeASAMLIdPInYourOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.removeASAMLIdPInYourOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.removeASAMLIdPInYourOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeASAMLIdPInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.removeASAMLIdPInYourOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-removeASAMLIdPInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateASAMLIdPInYourOrganization - errors', () => {
      it('should have a updateASAMLIdPInYourOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updateASAMLIdPInYourOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updateASAMLIdPInYourOrganization(null, null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASAMLIdPInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing idpId', (done) => {
        try {
          a.updateASAMLIdPInYourOrganization('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'idpId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASAMLIdPInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateASAMLIdPInYourOrganization('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updateASAMLIdPInYourOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnsTheSAMLSSOEnabledSettingsForAnOrganization - errors', () => {
      it('should have a returnsTheSAMLSSOEnabledSettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.returnsTheSAMLSSOEnabledSettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnsTheSAMLSSOEnabledSettingsForAnOrganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnsTheSAMLSSOEnabledSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatesTheSAMLSSOEnabledSettingsForAnOrganization - errors', () => {
      it('should have a updatesTheSAMLSSOEnabledSettingsForAnOrganization function', (done) => {
        try {
          assert.equal(true, typeof a.updatesTheSAMLSSOEnabledSettingsForAnOrganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.updatesTheSAMLSSOEnabledSettingsForAnOrganization(null, null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesTheSAMLSSOEnabledSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatesTheSAMLSSOEnabledSettingsForAnOrganization('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-updatesTheSAMLSSOEnabledSettingsForAnOrganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listtheavailabilityinformationfordevicesinanorganization - errors', () => {
      it('should have a listtheavailabilityinformationfordevicesinanorganization function', (done) => {
        try {
          assert.equal(true, typeof a.listtheavailabilityinformationfordevicesinanorganization === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.listtheavailabilityinformationfordevicesinanorganization(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-listtheavailabilityinformationfordevicesinanorganization', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnthetop10appliancessortedbyutilizationovergiventimerange - errors', () => {
      it('should have a returnthetop10appliancessortedbyutilizationovergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnthetop10appliancessortedbyutilizationovergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnthetop10appliancessortedbyutilizationovergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnthetop10appliancessortedbyutilizationovergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange - errors', () => {
      it('should have a returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStop10clientsbydatausageInmbOvergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange - errors', () => {
      it('should have a returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStop10devicessortedbydatausageovergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer - errors', () => {
      it('should have a returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStopclientsbydatausageInmbOvergiventimerangeGroupedbymanufacturer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange - errors', () => {
      it('should have a returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStop10devicemodelssortedbydatausageovergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange - errors', () => {
      it('should have a returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStop10ssidsbydatausageovergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange - errors', () => {
      it('should have a returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange function', (done) => {
        try {
          assert.equal(true, typeof a.returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnmetricsfororganizationStop10switchesbyenergyusageovergiventimerange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#returnalistofalerttypestobeusedwithmanagingwebhookalerts - errors', () => {
      it('should have a returnalistofalerttypestobeusedwithmanagingwebhookalerts function', (done) => {
        try {
          assert.equal(true, typeof a.returnalistofalerttypestobeusedwithmanagingwebhookalerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing organizationId', (done) => {
        try {
          a.returnalistofalerttypestobeusedwithmanagingwebhookalerts(null, (data, error) => {
            try {
              const displayE = 'organizationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-meraki-adapter-returnalistofalerttypestobeusedwithmanagingwebhookalerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
