
## 1.5.4 [10-15-2024]

* Changes made at 2024.10.14_21:29PM

See merge request itentialopensource/adapters/adapter-meraki!48

---

## 1.5.3 [09-01-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-meraki!46

---

## 1.5.2 [08-15-2024]

* Changes made at 2024.08.14_19:48PM

See merge request itentialopensource/adapters/adapter-meraki!45

---

## 1.5.1 [08-07-2024]

* Changes made at 2024.08.06_21:53PM

See merge request itentialopensource/adapters/adapter-meraki!44

---

## 1.5.0 [05-09-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!43

---

## 1.4.8 [03-26-2024]

* Changes made at 2024.03.26_14:10PM

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!42

---

## 1.4.7 [03-13-2024]

* Changes made at 2024.03.13_11:14AM

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!41

---

## 1.4.6 [03-11-2024]

* Changes made at 2024.03.11_10:44AM

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!40

---

## 1.4.5 [02-26-2024]

* Changes made at 2024.02.26_13:06PM

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!39

---

## 1.4.4 [02-13-2024]

* Add stub data for device broker calls

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!38

---

## 1.4.3 [02-09-2024]

* updates for broker

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!37

---

## 1.4.2 [12-24-2023]

* update metadata

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!36

---

## 1.4.1 [12-20-2023]

* add keys to broker config, update axios

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!35

---

## 1.4.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!34

---

## 1.3.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!34

---

## 1.2.4 [10-19-2023]

* meta use case and tag changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!33

---

## 1.2.3 [10-02-2023]

* meta use case and tag changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!33

---

## 1.2.2 [09-11-2023]

* remove example

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!32

---

## 1.2.1 [09-11-2023]

* Additional migration and metadata changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!31

---

## 1.2.0 [08-16-2023]

* Minor/2023 migration

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!30

---

## 1.1.0 [08-11-2023]

* Minor/2023 migration

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!30

---

## 1.0.4 [08-01-2023]

* Update adapter base and import order

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!27

---

## 1.0.3 [07-10-2023]

* Migrate adapter base to have broker changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!25

---

## 1.0.2 [05-10-2023]

* Migrate adapter base to have broker changes

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!25

---

## 1.0.1 [04-04-2023]

* Utils version has been updated in package.json, and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!23

---

## 1.0.0 [11-15-2022]

* Major/adapt 2285

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!22

---

## 0.8.4 [05-14-2022]

* Migrate the adapter to the latest foundation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!19

---

## 0.8.3 [05-08-2022] & 0.8.2 [05-02-2022] & 0.8.1 [04-13-2022] & 0.8.0 [01-21-2022]

- Multiple versions and releases due to this being one of the adapters heavily involved in the new addapter/broker integration effort.
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!18
See merge request itentialopensource/adapters/sd-wan/adapter-meraki!17
See merge request itentialopensource/adapters/sd-wan/adapter-meraki!16
See merge request itentialopensource/adapters/sd-wan/adapter-meraki!15

---

## 0.7.3 [06-08-2021]

- Update pronghorn.json and adpater.js for sending api call body directly

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!14

---

## 0.7.2 [03-10-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!13

---

## 0.7.1 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!12

---

## 0.7.0 [03-02-2020]

- Add new version calls based on additional query parameters that Meraki has made available.

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!11

---

## 0.6.0 [02-03-2020]

- Add new calls to the adapter that were in the new Postman Collection for Meraki (built swagger and put in report)

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!10

---

## 0.5.2 [01-13-2020]

- Bring the adapter up to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!8

---

## 0.5.1 [11-19-2019]

- Update the healthcheck url to one that was used in the lab

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!7

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!6

---

## 0.4.0 [09-17-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!5

---

## 0.3.1 [08-30-2019]

- Added the slash into the entity paths. Migrator does not currently update action files. This was fixed in the adapter builder a while ago but needed to be manually fixed in this adapter.

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!4

---

## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]
- Migrate to the latest adapter foundation, categorize and prepare for app artifact

See merge request itentialopensource/adapters/sd-wan/adapter-meraki!3

---

## 0.1.1 [06-18-2019]
- Initial Commit

See merge request itentialopensource/adapters/adapter-meraki!2

---
